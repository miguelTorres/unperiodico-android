export default {
  news: {
    offlineLoaded: false,
    news: {},
    onlineLoaded: {},
  },
  categories: {
      categories: [],
      offlineLoaded: false,
    },
  items: {
    onlineList: [],
    offlineList: []
  },
  newsItems: {
    offlineLoaded: false,
    news: {},
    online: {},
  },
  savedNews: {
    offlineLoaded: false,
    news: {}
  },
  connection: {
    connectionChecked: false
  },
  configuration: {
    offlineLoaded: false,
    configuration: {
      local: {
        subscribed: null
      }
    },
    loading: true
  },
  layout: {
    isFullScreen: false
  }
}