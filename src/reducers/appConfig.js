import {
  CONFIGURATION_LOADING,
  CONFIGURATION_LOAD_SUCCESS,
  CONFIGURATION_RESET,
  CONFIGURATION_LOAD_ERROR,
  CONFIGURATION_OFFLINE_LOAD_SUCCESS,
  CONFIGURATION_OFFLINE_LOAD_ERROR,
  CONFIGURATION_LOCAL,
} from '../actions/appConfig'
import moment from 'moment'
require('moment/locale/es');

import initialState from './initialState'


export default function reducer(state = initialState.configuration, action) {
  let list, local, newObject, uid, newsItem

  switch (action.type) {

    case CONFIGURATION_LOADING:
      return {
        ...state,
        loading: true
      }
    case CONFIGURATION_RESET:
      return {
        ...state,
        configuration: {}
      }
    case CONFIGURATION_LOAD_ERROR:
  
      return {
        ...state,
        error: action.error,
        loading: false
      }
    case CONFIGURATION_LOCAL:
    local = {...state.configuration.local, ...action.configuration}
    return {
      ...state,
      configuration: {...state.configuration, local: local}
    }

    case CONFIGURATION_LOAD_SUCCESS:
      local = {...state.configuration.local}

  
      return {
        ...state,
        loading: false,
        error: null,
        configuration: {...action.configuration, local: local}
      }

    case CONFIGURATION_OFFLINE_LOAD_ERROR:
      return {
        ...state,
        error: action.error
      }

    case CONFIGURATION_OFFLINE_LOAD_SUCCESS:

      return {
        ...state,
        offlineLoaded: true,
        configuration: action.configuration
      }

    default:
      return state
    }
}
