import React, { Component } from 'react'
import { View, Image, Dimensions, StyleSheet, Linking, WebView, Platform } from "react-native";
import { Text } from "native-base";


import HTML from 'react-native-render-html';
import unperiodicoVariables from '../../native-base-theme/variables/unperiodico-material'
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
import appConfig from '../models/appConfig';
import config from '../../config'
import UNText from './UNText';
import { normalizeUnits } from 'moment';
import { calculateResponsiveValue } from '../helpers/Responsive';


export default class UNPHTML extends Component {
  constructor(props) {
    super(props)
    this.alterData = this.alterData.bind(this)

  

}
alterData = (node) => {
  let { parent, data } = node;
  if (parent && parent.name === 'figcaption') {
      // Texts elements are always children of wrappers, this is why we check the tag
      // with "parent.name" and not "name"
      return data.trim();
  }
  // Don't return anything (eg a falsy value) for anything else than the <h1> tag so nothing is altered
}

homeConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      category: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(27), marginBottom: 10, marginTop: 10 },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25) },
      p: { fontSize: calculateResponsiveValue(16), marginBottom: 10, color: '#000' },
      date: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 8, color: '#4d4d4d', marginLeft: 0 },
      category: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 5, marginTop: 10, color: unperiodicoVariables.titleFontColor, paddingLeft: 0, marginLeft: 0 },
      figure: {marginBottom: 40},
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 40},
    }
  }
}
carouselConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      category: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: config.colors.config2.background, fontSize: calculateResponsiveValue(20, 27), lineHeight: calculateResponsiveValue(26, 32), marginBottom: calculateResponsiveValue(5, 10), fontWeight: 'bold', marginTop:5,  },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25) },
      p: { fontSize: calculateResponsiveValue(12, 19), lineHeight: calculateResponsiveValue(17, 26), paddingBottom: 30, color: config.colors.config1.color, paddingLeft: 20, paddingRight: 20, },
      category: { fontSize: calculateResponsiveValue(12), fontWeight: 'bold', marginTop: 10, color: config.colors.config1.color, paddingLeft: 0, marginLeft: 0 },
      figure: {marginBottom: 40},
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(3), },
      'date': { fontSize: calculateResponsiveValue(11, 19),  marginBottom: 0, color: config.colors.config1.color, marginLeft: 0,   },
      'category': { fontSize: calculateResponsiveValue(10, 17), fontWeight: 'bold',  color: config.colors.config1.color, marginLeft: 0, letterSpacing: 1,  },
      'image-caption': {marginBottom: 40},
      'wraper-category': {marginBottom: 5,},
      'wraper-date': {marginBottom: calculateResponsiveValue(10,13)},
    }
  }
}
recientesConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      category: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(16, 28), lineHeight: calculateResponsiveValue(23, 33), marginBottom: calculateResponsiveValue(5, 10),  marginTop: calculateResponsiveValue(5, 10) },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25) },
      p: { fontSize: calculateResponsiveValue(12, 19), marginBottom: 10, color: '#000' },
      date: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 8, color: '#4d4d4d', marginLeft: 0 },
      category: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 5, marginTop: 10, color: unperiodicoVariables.titleFontColor, paddingLeft: 0, marginLeft: 0 },
      figure: {marginBottom: 40},
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 40},
      'date': { fontSize: calculateResponsiveValue(10, 17),  marginBottom: 0,  marginLeft: 0, fontWeight: 'bold', color: '#4d4d4d'  },
      'category': {marginBottom: 5,  fontWeight: 'bold', letterSpacing: 0.5,fontSize: calculateResponsiveValue(10, 17), color: '#6E1E38'},
      'wraper-category': {marginBottom: 5,},
    }
  }
}
relatedConfiguration(){
  return {
    renderers: {
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      p: { fontSize: calculateResponsiveValue(13, 18), marginBottom: 0, color: '#6E1E38', fontWeight: 'bold',  paddingLeft: 0 },
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
    }
  }
}
listConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      category: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(17,24), lineHeight: calculateResponsiveValue(24,33), marginBottom: 10 },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25) },
      p: { fontSize: calculateResponsiveValue(13), marginBottom: 10, color: '#000' },
      category: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 10, color: unperiodicoVariables.titleFontColor },
      figure: {marginBottom: 40},
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 40},
      'date' : {fontWeight: 'bold', fontSize: calculateResponsiveValue(11), color: '#4A4A4A', fontWeight: 'bold',  },
      'date-seat' : {marginBottom: 5},
      'category': { fontWeight: 'bold', fontSize: calculateResponsiveValue(11), letterSpacing: 0.5, color: '#6E1E38' },
      'main-category': {marginBottom: 5,  }
    }
  }
}
listAuthorConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      category: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      author: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(17), marginBottom: 10 },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25) },
      p: {fontSize: calculateResponsiveValue(13), marginBottom: 10, color: '#000' },
      date: { fontSize: calculateResponsiveValue(12), fontWeight: 'bold', marginBottom: 10, color: '#4d4d4d' },
      category: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 30, color: unperiodicoVariables.titleFontColor },
      author: { fontSize: calculateResponsiveValue(14), fontWeight: 'bold', marginBottom: 10, color: unperiodicoVariables.titleFontColor },
      figure: {marginBottom: 40},
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 40},
      'date': {fontWeight: 'bold', fontSize: calculateResponsiveValue(11), color: '#4A4A4A', fontWeight: 'bold',},
      'category': {fontWeight: 'bold', fontSize: calculateResponsiveValue(11), letterSpacing: 0.5, color: '#6E1E38' },
      'author': {fontWeight: 'bold', fontSize: calculateResponsiveValue(12), color: '#6E1E38',   },
      'main-author': {marginBottom: 5,  },
      'date-seat': {marginBottom: 5},
      
      
    }
  }
}



authorBioConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      category: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      author: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      p: {fontSize: calculateResponsiveValue(13), marginBottom: 10, color: '#fff' },
    },
    classesStyles: {
      
      
    }
  }
}

detailConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      iframe: (attrs, children, convertedCSSStyles, passProps) => this.renderIframe(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderAllText(attrs, children, convertedCSSStyles, passProps),
      teaser: { renderer: this.renderAllText, wrapper: 'Text' },
      p: { renderer: this.renderAllText, wrapper: 'Text' },
      li: (attrs, children, convertedCSSStyles, passProps) => this.renderAllText(attrs, children, convertedCSSStyles, passProps),
      //h1: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25),  marginBottom: calculateResponsiveValue(5, 20), fontWeight: 'normal', marginTop:calculateResponsiveValue(0, 5), },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(20), fontWeight: 'normal', marginTop: 20, marginBottom: 15 },
      h4: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(16), fontWeight: 'normal', marginTop: 20, marginBottom: 15 },
      p: { fontSize: calculateResponsiveValue(15), marginBottom: 10, color: '#1A1A1A' },
      li: { fontSize: calculateResponsiveValue(15), marginBottom: 1, color: '#1A1A1A', padding: 0, marginLeft:0, marginRight: 0  },
      ul: {margin: 0, paddingLeft: -10      },
      date: { fontSize: calculateResponsiveValue(15), fontWeight: 'bold', marginBottom: 30, color: '#4d4d4d' },
      figure: {marginBottom: 40},
      img: {marginBottom:20},
      teaser: {fontSize:calculateResponsiveValue(50)},
      blockquote:{margin: 10, paddingLeft:15, marginBottom: 20,  borderLeftColor: '#F2D069',borderLeftWidth: 3, },
      a: {color: '#801836', fontWeight: 'bold'}
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 20, color: '#6a6a6a', marginTop: 20, fontSize: calculateResponsiveValue(13), },
      'teaser': {marginBottom: 10, },
      'date': {fontWeight: 'bold', fontSize: calculateResponsiveValue(12), color: '#6a6a6a', fontWeight: 'bold',},
      'category': {fontWeight: 'bold', fontSize: calculateResponsiveValue(12, 16), letterSpacing: 0.5, color: '#6E1E38' },
      'wraper-category': {marginBottom: 0},
      'wraper-date': {marginBottom:5},
      'author': {color: '#6a6a6a',fontSize: calculateResponsiveValue(13), fontWeight:'normal' },
      'main-author': {marginBottom: 20}
    }
  }
}
playerTextConfiguration(){
  return {
    renderers: {
       //h1: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      
    },
    classesStyles: {
      'title': { color: config.colors.config1.background },
    }
  }
}
detailTeaserConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderAllText(attrs, children, convertedCSSStyles, passProps),
      teaser: { renderer: this.renderAllText, wrapper: 'Text' },
      p: (attrs, children, convertedCSSStyles, passProps) => this.renderAllText(attrs, children, convertedCSSStyles, passProps),
      //h1: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(27), marginBottom: 15, fontWeight: 'normal' },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(25) },
      p: { fontSize: calculateResponsiveValue(17), marginBottom: 30, color: '#333333', fontStyle: 'italic' },
      date: { fontSize: calculateResponsiveValue(15), fontWeight: 'bold', marginBottom: 30, color: '#4d4d4d' },
      figure: {marginBottom: 40},
      teaser: {fontSize:calculateResponsiveValue(50)},
      a: {color: '#801836', textDecorationLine: 'none', fontWeight: 'bold' }
    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 40, color: 'green'},
      'teaser': {marginBottom: 10, },
      'date': {fontWeight: 'bold', fontSize: calculateResponsiveValue(12), color: '#4A4A4A', fontWeight: 'bold',},
      'category': {fontWeight: 'bold', fontSize: calculateResponsiveValue(12), letterSpacing: 0.5, color: '#6E1E38' },
      'wraper-category': {marginBottom: 10},
      'wraper-date': {marginBottom:15},
      'teaser-wraper': {marginBottom:10,},
    }
  }
}
ajaxConfiguration(){
  return {
    renderers: {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
      date: (attrs, children, convertedCSSStyles, passProps) => this.renderAllText(attrs, children, convertedCSSStyles, passProps),
  
      //p: (attrs, children, convertedCSSStyles, passProps) => this.renderText(attrs, children, convertedCSSStyles, passProps),
    },
    tagsStyles: {
      h1: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(23),  marginBottom: 20, fontWeight: 'bold', marginTop:0, },
      h3: { color: unperiodicoVariables.titleFontColor, fontSize: calculateResponsiveValue(15), fontWeight: 'normal', marginBottom: 15 },
      p: { fontSize: calculateResponsiveValue(15), marginBottom: 10, color: '#1A1A1A' },
      date: { fontSize: calculateResponsiveValue(15), fontWeight: 'bold', marginBottom: 30, color: '#4d4d4d' },
      figure: {marginBottom: 40},
      ul: {margin: 0, paddingLeft: -5},
      a: {color: '#801836', fontWeight: 'bold'}



    },
    classesStyles: {
      'header': { color: unperiodicoVariables.titleFontColor },
      'image-caption': {marginBottom: 40},
    }
  }
}
renderImg(attrs, children, convertedCSSStyles, passProps){
  const url = appConfig.config.app.SITE_URL + attrs.src
  return(
    <View key={passProps.key}>
    <Image
      style={styles.htmlImage}
      source={{uri: url}}
    />
    </View>
  )
}
renderIframe(attrs, children, convertedCSSStyles, passProps){
  if(attrs['data-unal-source']=='facebook'){
    return this.renderFB2Iframe(attrs, children, convertedCSSStyles, passProps);
  }
  if(attrs['data-unal-source']=='youtube'){
    return this.renderYBIframe(attrs, children, convertedCSSStyles, passProps);
  }
  if(attrs['data-unal-source']=='url'){
    return this.renderURLIframe(attrs, children, convertedCSSStyles, passProps);
  }
  
  return <Text key={passProps.key}>Fuente no definida</Text>;
  
  //return this.renderFBIframe(attrs, children, convertedCSSStyles, passProps);
  
}
calculateDimensions(targetWidth, targetHeight, width, height){
  if(targetWidth == 0 || width == 0){
    return {width: targetWidth, height: targetHeight}
  }
  if(targetHeight/targetWidth > height/width){
    return {
      width: targetWidth,
      height: height * targetWidth / width
    }
  }else{
    return {
      width: width * targetHeight / height,
      height: targetHeight
    }
  }
}
renderURLIframe(attrs, children, convertedCSSStyles, passProps){
  const targetWidth = typeof this.props.width !== 'undefined'? this.props.width : Dimensions.get('window').width
  const targetHeight = typeof this.props.height !== 'undefined'? this.props.height : Dimensions.get('window').height
  //const targetHeight = (attrs.height>0 && attrs.width > 0)? attrs.height * width / attrs.width : width * 3 / 4

  const {width, height} = this.calculateDimensions(targetWidth, targetHeight, attrs['width'], attrs['height'])


  let newAttrs = Object.assign({}, attrs)
  
  delete newAttrs['width']
  delete newAttrs['height']
  let stringAttrs = Object.keys(newAttrs).map((key)=>{
    return key+'="'+newAttrs[key]+'"'
  })

  //const html = '<iframe '+stringAttrs.join(' ')+' ></iframe>'
  
  return(
    <View key={passProps.key} style={{height: height, width: width}}>
      <WebView 
      originWhitelist={['*']} 
      source={{ uri: Platform.OS === "android"?attrs['data-unal-url-android']:attrs['data-unal-url-ios'] }}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      allowsInlineMediaPlayback={true}
      useWebKit={true}
      />
    </View>
  )
}
renderFB2Iframe(attrs, children, convertedCSSStyles, passProps){
  const targetWidth = typeof this.props.width !== 'undefined'? this.props.width : Dimensions.get('window').width
  const targetHeight = typeof this.props.height !== 'undefined'? this.props.height : Dimensions.get('window').height
  //const targetHeight = (attrs.height>0 && attrs.width > 0)? attrs.height * width / attrs.width : width * 3 / 4

  const {width, height} = this.calculateDimensions(targetWidth, targetHeight, attrs['width'], attrs['height'])


  let newAttrs = Object.assign({}, attrs)
  
  delete newAttrs['width']
  delete newAttrs['height']
  let stringAttrs = Object.keys(newAttrs).map((key)=>{
    return key+'="'+newAttrs[key]+'"'
  })

  //const html = '<iframe '+stringAttrs.join(' ')+' ></iframe>'
  const html = '<html><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" /><iframe src="' + newAttrs.src + '" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe></html>'
  return(
    <View key={passProps.key} style={{height: height, width: width}}>
      <WebView 
      originWhitelist={['*']} 
      source={{ html: html }}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      allowsInlineMediaPlayback={true}
      useWebKit={true}
      />
    </View>
  )
}
renderFB3Iframe(attrs, children, convertedCSSStyles, passProps){
  const targetWidth = typeof this.props.width !== 'undefined'? this.props.width : Dimensions.get('window').width
  const targetHeight = typeof this.props.height !== 'undefined'? this.props.height : Dimensions.get('window').height
  //const targetHeight = (attrs.height>0 && attrs.width > 0)? attrs.height * width / attrs.width : width * 3 / 4

  const {width, height} = this.calculateDimensions(targetWidth, targetHeight, attrs['width'], attrs['height'])


  let newAttrs = Object.assign({}, attrs)
  
  delete newAttrs['width']
  delete newAttrs['height']
  let stringAttrs = Object.keys(newAttrs).map((key)=>{
    return key+'="'+newAttrs[key]+'"'
  })
  const html2 = `
  <div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2&appId=301440247028261&autoLogAppEvents=1"></script>
<div class="fb-video" data-href="https://www.facebook.com/facebook/videos/716550658739782/" data-width="500" data-show-text="false"><blockquote cite="https://es-la.facebook.com/mvradiooficial/videos/716550658739782/" class="fb-xfbml-parse-ignore"><a href="https://es-la.facebook.com/mvradiooficial/videos/716550658739782/">Directo al corazón</a><p>Bienvenidos al programa... SOlo románticos..!</p>Publicado por <a href="https://www.facebook.com/mvradiooficial/">MV RADIO</a> en Jueves, 21 de febrero de 2019</blockquote></div>
  `
  //const html = '<iframe '+stringAttrs.join(' ')+' ></iframe>'
  const html = `
  <html><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <iframe playsinline="1" webkit-inline="1" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fmvradiooficial%2Fvideos%2F716550658739782%2F&width=500&show_text=false&appId=301440247028261&height=375&playsinline=1&webkit-inline=1&inline=1" width="100%" height="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="false"></iframe>
  </html>
  `
  return(
    <View key={passProps.key} style={{height: height, width: width}}>
      <WebView 
      originWhitelist={['*']} 
      source={{ html: html }}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      allowsInlineMediaPlayback={true}
      useWebKit={true}
      />
    </View>
  )
}
renderYBIframe(attrs, children, convertedCSSStyles, passProps){
  const width = typeof this.props.width !== 'undefined'? this.props.width : Dimensions.get('window').width
  const height = typeof this.props.height !== 'undefined'? this.props.height : Dimensions.get('window').height
  //const height = (attrs.height>0 && attrs.width > 0)? attrs.height * width / attrs.width : width * 3 / 4
  let newAttrs = Object.assign({}, attrs)
  delete newAttrs['width']
  delete newAttrs['height']
  let stringAttrs = Object.keys(newAttrs).map((key)=>{
    return key+'="'+newAttrs[key]+'"'
  })

  //const html = '<iframe '+stringAttrs.join(' ')+' ></iframe>'
  const html = '<html><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" /><iframe src="' + newAttrs.src + '" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe></html>'
  return(
    <View key={passProps.key} style={{height: height, width: width}}>
      <WebView 
      originWhitelist={['*']} 
      source={{ html: html }}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      allowsInlineMediaPlayback={true}
      />
    </View>
  )
}

renderFBIframe(attrs, children, convertedCSSStyles, passProps){
  const width = typeof this.props.width !== 'undefined'? this.props.width : Dimensions.get('window').width
  const height = typeof this.props.height !== 'undefined'? this.props.height : Dimensions.get('window').height
  //const height = (attrs.height>0 && attrs.width > 0)? attrs.height * width / attrs.width : width * 3 / 4
  let newAttrs = Object.assign({}, attrs, {width, height})
  let stringAttrs = Object.keys(newAttrs).map((key)=>{
    return key+'="'+newAttrs[key]+'"'
  })

  const html = '<iframe '+stringAttrs.join(' ')+'></iframe>'

  return(
    <View key={passProps.key} style={{height: height, width: width}}>
      <WebView 
      originWhitelist={['*']} 
      source={{ html: html }}

      />
    </View>
  )
}
renderText(attrs, children, convertedCSSStyles, passProps){
  return (
    <Text textBreakStrategy='simple' numberOfLines={3} key={passProps.key} style={convertedCSSStyles}>{ children }</Text>
  );
}
renderAllText(attrs, children, convertedCSSStyles, passProps){
  return (
    <Text textBreakStrategy='simple' key={passProps.key} style={convertedCSSStyles}>{ children }</Text>
  );
}


  render() {
    if(!this.props.html){
      return null;
    }
    const stylesConfiguration = typeof this[this.props.stylesConfiguration] === 'function'? this[this.props.stylesConfiguration]() : {}



    return(
      <HTML 
      {...this.props}
      ignoredTags = {[ ...IGNORED_TAGS, 'tag1', 'tag2']}
      imagesMaxWidth={Dimensions.get('window').width}
      {...stylesConfiguration}
      alterData = {this.alterData}
      onLinkPress={(evt, href) => { Linking.openURL(href); }}
    />
    )
  }
}

const styles = StyleSheet.create({
  htmlImage: {
    resizeMode: "contain",
    width: '100%',
    height: calculateResponsiveValue(220, 400),
    flex: 1
  },
});


