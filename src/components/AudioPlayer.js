import React from 'react';
import { StyleSheet, Text, View, AppState } from 'react-native';
import {
  Icon,
  Button
} from "native-base";
import MusicControl from 'react-native-music-control';
import Video from 'react-native-af-video-player'
import Sound from 'react-native-sound';
import PlayerHelper from '../helpers/PlayerHelper'
import ButtonIcon from './ButtonIcon';
import Assets from '../../assets'
import config from '../../config'
import appConfig from '../models/appConfig';

const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'
export default class AudioPlayer extends React.Component {
  constructor(props){
    super(props)
    this.state = { play: true, appState: AppState.currentState }
    this.displayInfo = this.displayInfo.bind(this)
    this.play = this.play.bind(this)
    this.pause = this.pause.bind(this)
    this._handleAppStateChange = this._handleAppStateChange.bind(this)
    
  }
  componentDidMount(){
    this.isComponentMount = true
    MusicControl.enableBackgroundMode(true);
    /*
    this.whoosh = new Sound(this.props.url, null, (error) => {
      if (error) {
      } else { // loaded successfully
        this.play()
      }
    });
    */

   this.play()

    MusicControl.on('play', this.play)
    MusicControl.on('pause', this.pause)
    //AppState.addEventListener('change', this._handleAppStateChange);

  }
  componentWillUnmount() 
  {
    this.isComponentMount = false
    PlayerHelper.unregister(this.pause)
    MusicControl.off('play')
    MusicControl.off('pause')
    //this.whoosh.release();
    MusicControl.stopControl()
    //AppState.removeEventListener('change', this._handleAppStateChange);
  }  
  _handleAppStateChange = (nextAppState) => {
    if (
      nextAppState === 'active'
    ) {
      this.play()
    }else{
      this.pause()
    }
    this.setState({appState: nextAppState});
  };
  play(){
    /*
    this.whoosh.play((success) => {
      if (success) {
      } else {
      }
    });*/
    if(this.video){
      this.video.play()
    }


    MusicControl.setNowPlaying({
      title: this.props.title,
      artwork: appConfig.config.app.AUDIO_ARTWORK,
      //artist: 'Michael Jackson',
      //album: 'Thriller',
      //genre: 'Post-disco, Rhythm and Blues, Funk, Dance-pop',
      //duration: this.whoosh.getDuration(),
      description: this.props.description,
      date: '1983-01-02T00:00:00Z',
      rating: 84
    })
    MusicControl.enableControl('play', false)
    MusicControl.enableControl('pause', true)

    MusicControl.updatePlayback({
        state: MusicControl.STATE_PLAYING
    });

    this.isComponentMount && this.setState({
      play: true
    })
    PlayerHelper.register(this.pause)

  }




  pause(){
    //this.whoosh.pause()
    if(this.video){
      this.video.pause()
    }
    
    MusicControl.enableControl('play', true)
    MusicControl.enableControl('pause', false)
    MusicControl.updatePlayback({
        state: MusicControl.STATE_PAUSED
    });


    this.isComponentMount && this.setState({
      play: false
    })
  }

  displayInfo(){
    if(!this.state.play){
      this.play()
    } else {
      this.pause()
    }
  }
  renderVideoComponent(){

    return (
      <View style={{height: 0}}>
      <Video 
      autoPlay={true}
      ref={(ref) => { this.video = ref }}
      url={this.props.url} 
      playInBackground={true}
      controls={false}
      fullScreenOnly={false}
      logo={logo}
      //width={width}
      //inlineOnly={true}
      />
      </View>
    )
  }
  render() {
    const label = this.state.play ? "PAUSE SONG" : "PLAY SONG"
    const label2 = "PLAY NO AUDIO"
    const label3 = "UPDATE INFO"



    return (
      <View style={styles.container}>
        { this.state.play &&
            <ButtonIcon headerButton={true} imageStyle = {{width: 25, height: 25, marginRight: 8, marginLeft: 7}} imageSource={Assets.pause} onPress={this.displayInfo}>
            </ButtonIcon>
        }
        {this.renderVideoComponent()}
        { !this.state.play &&
        
        <ButtonIcon headerButton={true} imageStyle = {{width: 40, height: 40}} imageSource={Assets.play} onPress={this.displayInfo}>
        </ButtonIcon>

        }
        <View style={{paddingRight: 50}}>
        <Text style={{color: config.colors.config1.background, marginLeft: 10, paddingRight: 45}}>{this.props.title}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#f2f2f2',
    //borderColor: 'red',
    //borderWidth: 3,
    flexDirection: 'row'
  },
  icon: {
    fontSize: 40,
  }
});
