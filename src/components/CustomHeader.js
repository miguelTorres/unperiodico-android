import React, { Component } from 'react'
import {StyleSheet, View} from 'react-native'
import {
  Header
} from "native-base";
import { calculateResponsiveValue } from '../helpers/Responsive';
import CustomStatusBar from './CustomStatusBar';
import unperiodicoVariables from '../../native-base-theme/variables/unperiodico-material'
import config from '../../config'





export default class CustomHeader extends Component {


  render() {

    return(
      <Header style={styles.header}>
        <CustomStatusBar />
        <View style={styles.innerHeader}>

        </View>
        {this.props.children}
      </Header>
    )


  }
}
const styles = StyleSheet.create({
  header: {
    //backgroundColor: config.colors.config1.background, 
    height: calculateResponsiveValue(54, 82),
    borderBottomColor: config.colors.config1.background,
    borderBottomWidth: 0.4,
  },
  innerHeader: {
    position: 'absolute',
    //top: unperiodicoVariables.isIphoneX? 0 : 18,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff'
  }

});
