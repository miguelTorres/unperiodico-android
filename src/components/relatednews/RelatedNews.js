import React from 'react';
import { StyleSheet, TouchableOpacity, TouchableHighlight, ScrollView, Image, Text, View, Alert, FlatList, Dimensions } from 'react-native';
import {
  Icon,
  Button,
  Card, CardItem,
  H1,
  H2,
  H3,
  H4,
} from "native-base";
import appConfig from '../../models/appConfig';
import _ from "lodash";
import UNCarousel from '../Carousel'

import CarouselView from '../CarouselView/CarouselView'
import RelatedPairItem from './CarouselView/RelatedPairItem'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import config from '../../../config'
import Assets from '../../../assets'
import { calculateResponsiveValue } from '../../helpers/Responsive';

export default class RelatedNews extends React.Component {
  constructor(props){
    super(props)
    this.renderRow = this.renderRow.bind(this)
    this.renderCarouselItem = this.renderCarouselItem.bind(this)
    this.state = {
      opened: (!!this.props.isDetail)? false : true
    }
    this.open = this.open.bind(this)
  }
  componentDidMount() {
    this.isComponentMount = true
  }
  componentWillUnmount() 
  {
    this.isComponentMount = false
  }  

  renderRow = (data) => {
    const item = data.item
    const img = appConfig.config.app.SITE_URL + item.image.src
     
    return (
      <Card style={styles.itemContainer} >


            <CardItem cardBody>
            <Image
                style={styles.image}
                source={{uri: img }}
              />

            </CardItem>

          </Card>
    )

    return(
      <View style={styles.itemContainer}><Text>{data.item.link.text}</Text></View>
    )
  }
  calculateRelatedInPairs(related){

    if(typeof this.relatedInPairs !== 'undefined'){
      return this.relatedInPairs
    }

    if(!related || related.length ===0){
      this.relatedInPairs = null
      return null
    }
    this.relatedInPairs = _(related).reduce(function(result, value, index, array) {
      if (index % 2 === 0)
        result.push(array.slice(index, index + 2));
      return result;
    }, []);
    return this.relatedInPairs
  }

  showRelatedInPairs(relatedInPairs){
    return _.map(relatedInPairs, function (item) {
      return Object.keys(item)
    })
  }
  renderCarousel(data, _renderItem){
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width
    //const indicatorAtBottom = this.props.layout.isFullScreen? false:true
    //const indicatorSize = this.props.layout.isFullScreen? 0:20

    //const dimensions = this.props.layout.isFullScreen? {width, height}:{}
    
    return (
      <View style={styles.carouselContainer}>
      <View>
        <UNCarousel
            delay={2000}
            animate={false}
            loop={false}
            //indicatorAtBottom={indicatorAtBottom}
            //indicatorSize={indicatorSize}
            onPageChange = {this.onPageChange}
            renderItem = {_renderItem}
            data = {data}
            isFullScreen = {false}
            //indicatorText="✽"
            //indicatorColor="red"
            >
          </UNCarousel>
      </View>
      </View>
      
    );
    
  }
  renderItem(item){
    const img = appConfig.config.app.SITE_URL + item.image.src
    return (
      <TouchableOpacity activeOpacity={0.8} onPress={() => {
        EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, {url, componentRender, initialFloatingState, block})
      }} >                  
        
      <View styles={styles.item}>
        <Image
          style={styles.image}
          source={{uri: img }}
        />
      </View>
      </TouchableOpacity>

    )
  }
  renderRelatedPairs(pair, index){

    if(typeof pair === 'undefined'){
      return null
    }



    
    const itemtal = pair[0]
    const img = appConfig.config.app.SITE_URL + itemtal.image.src
    return (
      
      <TouchableOpacity activeOpacity={0.8} onPress={() => {
        EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, {url, componentRender, initialFloatingState, block})
      }} >              
      <View>
      <View style={styles.aboveContainer} >
        </View>
        <View style={styles.belowContainer} >
        </View>
    </View>
    </TouchableOpacity>    
    )


    const item1 = this.renderItem(pair[0])
    const item2 = pair.length>0 ? this.renderItem(pair[1]) : null

    return (
      <View>
        {item1}
      </View>
    )

    const item = this.props.data.item
    const url = pair.media!==null?this.selector.getUrl(pair.media):null
    const preview = pair.preview
    const categories = item.additional.categories
    const configuration = configurationHelper.getConfiguration(this.props.configuration, categories)
    const tag = configuration? configuration.tag:null

    //const component = this.renderMediaComponentBySelector(pair, index)
    const componentRender = this.createRenderFunction(pair, index)
    const initialFloatingState = this.selector.getInitialFloatingState()
    const block = this.selector.getBlock()
    let imageStyle = styles.imageInCarousel
    if(typeof index === 'undefined'){
      imageStyle = styles.imageOutCarousel
    }

    const icon = this.selector.getIcon()
    
    
    //return null
    const view = (
      <View>
        <Image
                style={imageStyle}
            source={{uri: preview}}
          />
          {icon && 
            <View style={[styles.containerPreviewIcon]}>
              <Icon active type={icon.type} name={icon.name} style={styles.previewIcon} />
            </View>
          }
          {tag !== null && 
          <View style={styles.decoration}>
            <Text style={[styles.decorationText, {color: tag.color, backgroundColor:tag.bgcolor}]}>{tag.name}</Text>
          </View>
          }
      </View>
    )

    if(url === null && pair.media == null){
      return view
    }
    return view

    return (
        <TouchableOpacity activeOpacity={0.8} onPress={() => {
          EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, {url, componentRender, initialFloatingState, block})
        }} >                  
          {view}
        </TouchableOpacity>
    )    


  }

  renderCarouselItem(data, index){
    return (
      <RelatedPairItem
        navigation={this.props.navigation}
        data={data}
        onPressItem={this.props.onPressItem}
      />
    )
  }
  open(){
    this.isComponentMount && this.setState({
      opened: !this.state.opened
    })
  }
  render() {

    const related = this.props.related
    const relatedInPairs = this.calculateRelatedInPairs(related)
    const height = this.state.opened? config.carouselRelated.height:0
    const flecha = this.state.opened? Assets.flechaBlancaArriba:Assets.flechaBlancaAbajo

    return (
      <View style={styles.content}>
      {!this.props.isDetail && 
      <View style={styles.header}>
        <Text style={{fontSize: calculateResponsiveValue(15), fontWeight: 'bold', color: '#fff',}}>{this.props.title}</Text>
      </View>
      }
      {!!this.props.isDetail && 
      <TouchableOpacity onPress={this.open}>
      <View style={styles.header}>
        <View style={styles.flechaAbajoContainer}>
            <Image style={styles.flechaAbajo} source={flecha} />
        </View>
        <Text style={{fontSize: calculateResponsiveValue(14), fontWeight: 'bold', color: '#fff', }}>{this.props.title}</Text>
      </View> 
      </TouchableOpacity>
      }
      {this.state.opened? (
      <View style={{height: height}}>
      <CarouselView 
        hideIndicators={!this.state.opened}
        indicator={Assets.indicatorYellow}
        inactiveIndicator={Assets.indicatorWhite}
        data={relatedInPairs}
        isRefreshing={()=>false} 
        handleRefresh={()=>{}} 
        onEndReached={()=>{}}
        onRefreshControl={()=>{}}
        hasNextPage = {false}
        renderItem = {this.renderCarouselItem}
        height= {config.carouselRelated.height}
        indicatorOffset = {10}
       />
      </View>
      ):(
        <View></View>
      )}

       </View>
    )  


    return this.renderCarousel(relatedInPairs, this.renderRelatedPairs)

    return (
      <View><Text>{JSON.stringify(this.showRelatedInPairs(relatedInPairs))}</Text></View>
    )


    return (
      <View style={styles.container}>
      <ScrollView 
        contentContainerStyle={styles.contentContainer} 
        horizontal={true}
          showsHorizontalScrollIndicator={false}>
          <View style={{width: 2000}}>
          <FlatList
          style={styles.flatList}
              data={related}
              renderItem={this.renderRow}
              keyExtractor={item=>""+item.uid}
              ItemSeparatorComponent={()=>null}
              horizontal
              //refreshing={isRefreshing}
              //onRefresh={this.props.handleRefresh}
              //onEndReached={this.props.onEndReached}
              
                     
              />
          </View>

      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {backgroundColor: config.colors.config3.background,
    
     paddingBottom: 10, 
          bottom: 0,
     borderBottomLeftRadius: 0, 
     borderBottomRightRadius: 0, 
     marginBottom: 0},
  flechaAbajoContainer:{
    position: 'absolute',
    right: 15,
    top: 0,
    bottom: 0,
    justifyContent: 'center', alignItems: 'center'
  },
  flechaAbajo:{
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginTop: 10,
  },

  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  header: {
    paddingBottom: 0, 
    paddingLeft: 20,
    paddingTop:10,
    
    
    
  },

  contentContainer: {
    paddingVertical: 20
  },
  container: {
    //flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    borderColor: 'red',
    borderWidth: 3,
    height: Dimensions.get('window').height*1/4,
    marginTop: 10,
    marginBottom: 10,
    height: config.carouselRelated.height,
    //flexDirection: 'row'
  },
  carouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },

  icon: {
    fontSize: 40,
  },
  itemContainer:{
    width: Dimensions.get('window').width*4/5,
    height: Dimensions.get('window').width*1/4,
    margin: 20,
  },
  flatList:{
    borderColor: 'green',
    borderWidth: 3,

  },
  image: {
    resizeMode: "cover",
    width: '100%',
    height: 200,
    //flex: 1
  },
  item: {
    flex: 1
  },
  aboveContainer: {
    backgroundColor: 'green',
    position: 'absolute',
    top: 0,
    left: 0,
    height: 50,
   
  },
  belowContainer: {
    backgroundColor: 'red',
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 50,
   
  },
  
});
