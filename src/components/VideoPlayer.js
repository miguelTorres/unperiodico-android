import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  Icon,
  Button
} from "native-base";
import Video from 'react-native-af-video-player'
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from './FloatingViewConstants'
import PlayerHelper from '../helpers/PlayerHelper'
import ButtonIcon from './ButtonIcon';
import Assets from '../../assets'
import config from '../../config'
const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'
import HTML from './HTML'

export default class VideoPlayer extends React.Component {
  constructor(props){
    super(props)
    this.state = { maximized: false }
    this.onLayout = this.onLayout.bind(this)
  }
  componentDidMount(){
    this.isComponentMount = true
    this.listenerMaximized = EventRegister.addEventListener(FloatingViewConstants.layout.MAXIMIZED, () => {
      this.isComponentMount && this.setState({
        maximized: true
      })
    })
    this.listenerMinimized = EventRegister.addEventListener(FloatingViewConstants.layout.MINIMIZED, () => {
      this.isComponentMount && this.setState({
        maximized: false
      })
    })
    this.listenerMinimized = EventRegister.addEventListener(FloatingViewConstants.layout.MOVE, () => {
      if(!this.state.maximized){
        this.isComponentMount && this.setState({
          maximized: true
        })  
      }
    })

    
  }
  onLayout(event){
    this.setState({
      contentWidth: event.nativeEvent.layout.width,
      contentHeight: event.nativeEvent.layout.height
    })
  }
  componentWillUnmount() 
  {
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerMaximized)
    EventRegister.removeEventListener(this.listenerMinimized)
  }  
  renderVideoComponent(){
    const width = 200
    return (
      <Video 
      autoPlay={true}
      ref={this.props.videoRef} 
      url={this.props.url} 
      playInBackground={false}
      controls={false}
      fullScreenOnly={true}
      logo={logo}
      //width={width}
      //inlineOnly={true}
      />
    )
  }
  render() {
    const title = '<p class="title">' + this.props.title + '</p>'
    const styleMinimized = this.state.maximized? null : styles.containerMinimized;
    return (
      <View style={[styles.container, styleMinimized]}>
        <View style={styles.video} onLayout={this.onLayout}>
          {(!!this.state.contentWidth) && this.renderVideoComponent(this.state.contentWidth, this.state.contentHeight)}
        </View>
        {!this.state.maximized?
          <View style={styles.textContainer}>
          <View style={{marginLeft: 10, paddingRight: 45}}>
          <HTML stylesConfiguration='playerTextConfiguration' html={title} />
          </View>
          </View> : 
          <View />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    // borderColor: 'red',
    // borderWidth: 3,
    flexDirection: 'row'
  },
  containerMinimized: {
    backgroundColor: '#f2f2f2',
  },
  icon: {
    fontSize: 40,
  },
  video: {
    flex: 2
  },
  textContainer: {
    flex: 3
  }
});
