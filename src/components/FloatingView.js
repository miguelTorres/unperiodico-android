import React, { Component } from 'react'
import {
  PanResponder,
  View,
  Text,
  StyleSheet,
  Dimensions,
  BackHandler
} from "react-native";
import {
  Icon,
} from "native-base";
import Video from 'react-native-af-video-player'
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from './FloatingViewConstants'

import KeepAwake from 'react-native-keep-awake';
import Orientation from 'react-native-orientation';

import Assets from '../../assets'
import ButtonIcon from './ButtonIcon';

const componentWeight = 6


var {height, width} = Dimensions.get('window');

const floatingViewStyle = StyleSheet.create({
  component: {
    backgroundColor: 'black',
    //borderColor: '#aa8',
    overflow: 'hidden',
    //borderWidth: 2
  },
  hidden: {
    //position: 'absolute',
    flex: 0,
  },
  maximized: {
    //flex: 1
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,

  },
  minimized: {
    flex: 1
  },
  minimum: {
  },
});

const componentStyle = StyleSheet.create({
  component: {
    //backgroundColor: '#a0a',
    //borderColor: '#a8a',
    //borderWidth: 2
  },
  hidden: {
    flex: 0,
  },
  maximized: {
    flex: 1
  },
  minimized: {
    flex: componentWeight
  },
  minimum: {
    flex: componentWeight
  }
})

const styles = StyleSheet.create({
  container: {
    //position: 'absolute',
    flex: 1,
    /*position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,*/
    /*backgroundColor: '#dd0',
    borderColor: 'green',
    borderWidth: 3*/
  },
  controls: {
    position: 'absolute',
    flexDirection: 'row',
    top: 5,
    right: 5,
    //backgroundColor: 'rgba(52, 52, 52, 0.2)',
    //zIndex: 100,
  },
  controlsMin: {
    position: 'absolute',
    flexDirection: 'row',
    top: 0,
    right: 5,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',

    //backgroundColor: 'rgba(52, 52, 52, 0.2)',
    //zIndex: 100,
  },
  close:{
    width: 40,
    height: 40,
    opacity: 0.8
  },

});

const video = 'https://dev3.unal.edu.co/fileadmin/user_upload/SedeCaribe_720.mp4'
const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'

export default class FloatingView extends Component {
  constructor(props) {
    super(props)
    this.componentRenderDefault = ()=>{return null}
    this.onShouldCapture = this.onShouldCapture.bind(this)
    this.state = {
      componentRender: this.componentRenderDefault,
      layout: FloatingViewConstants.layout.HIDDEN,
      style: {
        //height,
        //width,
      },
      block: true,
      changes: 0
    }
    this.component = null
    this.updateComponentHeight = this.updateComponentHeight.bind(this)
    this.onBackPress = this.onBackPress.bind(this)


    this._panResponder = PanResponder.create({
      // Ask to be the responder:
      onStartShouldSetPanResponder: this.onShouldCapture,
      onStartShouldSetPanResponderCapture: this.onShouldCapture,
      onMoveShouldSetPanResponder: this.onShouldCapture,
      onMoveShouldSetPanResponderCapture: this.onShouldCapture,

      onPanResponderGrant: (evt, gestureState) => {
        // The gesture has started. Show visual feedback so the user knows
        // what is happening!
        if(this.state.block){
          return
        }

        if(this.state.layout === FloatingViewConstants.layout.MAXIMIZED){
          this.onPanResponderGrantMaximized(evt, gestureState)
        }else if(this.state.layout === FloatingViewConstants.layout.MINIMIZED){
          this.onPanResponderGrantMinimized(evt, gestureState)
        }
        this._highlight();
        // gestureState.d{x,y} will be set to zero now
      },
      onPanResponderMove: (evt, gestureState) => {
        if(this.state.block){
          return
        }
        // The most recent move distance is gestureState.move{X,Y}
        if(this.state.layout === FloatingViewConstants.layout.MAXIMIZED){
          this.onPanResponderMoveMaximized(evt, gestureState)
        }else if(this.state.layout === FloatingViewConstants.layout.MINIMIZED){
          this.onPanResponderMoveMinimized(evt, gestureState)
        }
        EventRegister.emit(FloatingViewConstants.layout.MOVE)
        // The accumulated gesture distance since becoming responder is
        // gestureState.d{x,y}
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
        if(this.state.block){
          return
        }
        // The user has released all touches while this view is the
        // responder. This typically means a gesture has succeeded
        if(this.state.layout === FloatingViewConstants.layout.MAXIMIZED){
          this.onPanResponderReleaseMaximized(evt, gestureState)
        }else if(this.state.layout === FloatingViewConstants.layout.MINIMIZED){
          this.onPanResponderReleaseMinimized(evt, gestureState)
        }
        this._unHighlight();
      },
      onPanResponderTerminate: (evt, gestureState) => {
        // Another component has become the responder, so this gesture
        // should be cancelled
      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        // Returns whether this component should block native components from becoming the JS
        // responder. Returns true by default. Is currently only supported on android.
        return false;
      },
    });
  }
  onShouldCapture(evt, gestureState){
    if(Math.abs(gestureState.dy)>10){
      return true;
    }
      return false;
  }
  _floatingStyles = {style: {}}
  _componentStyles = {style: {}}
  _highlight() {
    return
    this._floatingStyles.style.borderWidth = 3;
    this._floatingStyles.style.borderColor = 'blue';

    this._updateNativeStyles();
  }
  _unHighlight() {
    return
    this._floatingStyles.style.borderWidth = 0;
    this._floatingStyles.style.borderColor = 'transparent';
    this._updateNativeStyles();
  }
  _updateNativeStyles() {
    this.floatingView && this.floatingView.setNativeProps(this._floatingStyles);
    this.componentView && this.componentView.setNativeProps(this._componentStyles);
  }
  onPanResponderGrantMaximized(evt, gestureState){
    var {height, width} = Dimensions.get('window');
    this.initialHeight = height - gestureState.y0
  }
  onPanResponderMoveMaximized(evt, gestureState){

    if(gestureState.dy <=0){
      return
    }

    var percentage = (this.initialHeight - gestureState.dy)/this.initialHeight
    var normalFlex = componentWeight - componentWeight * percentage
    var acceleratedFlex = this.scaleMaximizedFlex(normalFlex)
    this._componentStyles.style.flex = acceleratedFlex



    this._updateNativeStyles();

  }
  onPanResponderReleaseMaximized(evt, gestureState){
    var percentage = gestureState.dy<0?1:(this.initialHeight - gestureState.dy)/this.initialHeight
    var normalFlex = componentWeight - componentWeight * percentage
    var acceleratedFlex = this.scaleMaximizedFlex(normalFlex)


    if(acceleratedFlex > 1.5){
      this.minimize()
    }else{
      this.maximize()
    }

  }

  scaleMaximizedFlex(normalFlex){
    var acceleratedFlex = normalFlex<=0 ? 0 : (componentWeight/Math.pow(componentWeight, 4/5))*(Math.pow(normalFlex, 4/5))
    this.isComponentMount && this.setState({
      ...this.state,
      gestureState: {
        normalFlex,
        acceleratedFlex: acceleratedFlex > componentWeight ? componentWeight:acceleratedFlex
      }
    })
    return acceleratedFlex > componentWeight ? componentWeight:acceleratedFlex
  }
  scaleMinimizedFlex(normalFlex){

    var acceleratedFlex = normalFlex<=0 ? 0 : componentWeight * (Math.pow(normalFlex, 4))/(Math.pow(componentWeight, 4))
    this.isComponentMount && this.setState({
      ...this.state,
      gestureState: {
        normalFlex,
        acceleratedFlex: acceleratedFlex > componentWeight ? componentWeight:acceleratedFlex
      }
    })

    return (acceleratedFlex > componentWeight ? componentWeight:acceleratedFlex)
  }

  onPanResponderGrantMinimized(evt, gestureState){
    var {height, width} = Dimensions.get('window');
    this.initialHeight = gestureState.y0
  }
  onPanResponderMoveMinimized(evt, gestureState){

    if(gestureState.dy >=0){
      return
    }

    var percentage = (this.initialHeight + gestureState.dy)/this.initialHeight
    var normalFlex = componentWeight * percentage
    var acceleratedFlex = this.scaleMinimizedFlex(normalFlex)

    this._componentStyles.style.flex = acceleratedFlex


    this._updateNativeStyles();

  }
  onPanResponderReleaseMinimized(evt, gestureState){
    var percentage = gestureState.dy>=0?1:(this.initialHeight + gestureState.dy)/this.initialHeight
    var normalFlex = componentWeight * percentage
    var acceleratedFlex = this.scaleMinimizedFlex(normalFlex)

    if(acceleratedFlex < 1.5){
      this.maximize()
    }else{
      this.minimize()
    }

  }


  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    this.isComponentMount = true
    this.listener = EventRegister.addEventListener(FloatingViewConstants.VIDEO_EVENT, (data) => {
      var {url, componentRender, initialFloatingState, block, lockToLandscapeOnMaximize} = data
        this.isComponentMount && this.setState({
            ...this.state,
            video: url,
            componentRender: this.componentRenderDefault,
        })
        setTimeout(()=>{
          this.isComponentMount && this.setState({
            ...this.state,
            video: url,
            componentRender: componentRender,
            layout: initialFloatingState,
            block,
            lockToLandscapeOnMaximize
          })
        }, 300)
    })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listener)
    Orientation.unlockAllOrientations();
  }  
  onBackPress = () => {
    const { navigation, dispatch } = this.props;
    if(this.state.layout === FloatingViewConstants.layout.MAXIMIZED){
      this.close();
      return true;
    }
    return false;
  };
  onVideoFullScreen(status){
    //this.props.toggleFullScreen(status)
    this.setFullScreen(status)
  }
  get componentStyle(){
    switch (this.state.layout) {
      case FloatingViewConstants.layout.MAXIMIZED:
      return componentStyle.hidden
      case FloatingViewConstants.layout.MINIMIZED:
      return componentStyle.minimized
      case FloatingViewConstants.layout.MINIMUM:
      return componentStyle.minimum
      default:
      return componentStyle.maximized
      break;
    }
  }
  get floatingViewStyle(){
    var {height, width} = Dimensions.get('window');
    if(this.state.layout === FloatingViewConstants.layout.MAXIMIZED){
      return floatingViewStyle.maximized
    }else if(this.state.layout === FloatingViewConstants.layout.MINIMIZED){
      return floatingViewStyle.minimized
    }else if(this.state.layout === FloatingViewConstants.layout.MINIMUM){
      return floatingViewStyle.minimum
    }else{
      return floatingViewStyle.hidden
    }
  }
  setFullScreen(status){
    if(status){
      this.isComponentMount && this.setState({
        ...this.state,
        layout: FloatingViewConstants.layout.MAXIMIZED,
      })
    }else{
      this.isComponentMount && this.setState({
        ...this.state,
        layout: FloatingViewConstants.layout.MINIMIZED,
      })
    }
  }
  close(){
    this.isComponentMount && this.setState({
      ...this.state,
      layout: FloatingViewConstants.layout.HIDDEN,
      componentRender: this.componentRenderDefault,
    }, ()=>{
      Orientation.unlockAllOrientations();
      EventRegister.emit(FloatingViewConstants.layout.HIDDEN)
      EventRegister.emit(FloatingViewConstants.layout.ONCHANGE, FloatingViewConstants.layout.HIDDEN)
      KeepAwake.deactivate();
    })
  }
  minimize(){
    this._componentStyles.style.flex = componentWeight
    this._updateNativeStyles();

    this.isComponentMount && this.setState({
      ...this.state,
      changes: this.state.changes+1,
      layout: FloatingViewConstants.layout.MINIMIZED,
    })
    Orientation.unlockAllOrientations();
    EventRegister.emit(FloatingViewConstants.layout.MINIMIZED)
    EventRegister.emit(FloatingViewConstants.layout.ONCHANGE, FloatingViewConstants.layout.MINIMIZED)
  }
  maximize(){
    this._componentStyles.style.flex = 0
    this._updateNativeStyles();
    this.isComponentMount && this.setState({
      ...this.state,
      changes: this.state.changes+1,
      layout: FloatingViewConstants.layout.MAXIMIZED,
    })
    if(this.state.lockToLandscapeOnMaximize){
      Orientation.lockToLandscape();
      setTimeout(() => {
        EventRegister.emit(FloatingViewConstants.layout.MAXIMIZED)
        EventRegister.emit(FloatingViewConstants.layout.ONCHANGE, FloatingViewConstants.layout.MAXIMIZED)
      }, 500);
    }else{
      EventRegister.emit(FloatingViewConstants.layout.MAXIMIZED)
      EventRegister.emit(FloatingViewConstants.layout.ONCHANGE, FloatingViewConstants.layout.MAXIMIZED)
    }
    
}
  renderVideo(){

    /*return (
      <View>
        <Text>Video {this.state.video}</Text>
      </View>
    )*/

    if(!this.state.video){
      return null
    }
    return(
      <View>
        <Video
                  ref={(ref) => {
                    this.video = ref 
                  }}
                  autoPlay={true}
                  playInBackground={true}
                  url={video}
                  //fullScreenOnly={true}
                  onLoad = {(data)=>{
                    
                    //this.video.toggleFS()
                  }}
                  logo={logo}
                  onFullScreen={status => this.onVideoFullScreen(status)}
                  />
      </View>
    )
  }
  updateComponentHeight(event){
    this.isComponentMount && this.setState({
      componentHeight: event.nativeEvent.layout.height
    })
  }

  render() {




    const currentFloatingViewStyle = this.floatingViewStyle
    const currentComponentStyle = this.componentStyle
    //const video = this.renderVideo()
    //const Component = this.state.component
    //const video = typeof Component !== 'undefined' ? <Component ref={ref => {      this.component=ref;    }} /> : null
    //const video = typeof Component !== 'undefined' ? React.cloneElement(Component, { ref: (ref)=>{this.component=ref} }): null
    //const video = typeof Component !== 'undefined' ? Component: null
    const stylesControls = this.state.layout === FloatingViewConstants.layout.MINIMIZED||FloatingViewConstants.layout.MINIMUM?
    styles.controlsMin:styles.controls

    const floatinViewDynamicStyle = (!!this.state.componentHeight) && this.state.layout === FloatingViewConstants.layout.MINIMUM?
       {height: this.state.componentHeight} : {}


    return(
      <View style={[styles.container]}>
        <View style={[componentStyle.component, currentComponentStyle]}
          ref={componentView => {
            this.componentView = componentView;
          }}
        
        >
          {this.props.component}
        </View>
        <View style={[floatingViewStyle.component, currentFloatingViewStyle, floatinViewDynamicStyle]}
        onLayout={this.updateComponentHeight}
          ref={floatingView => {
            this.floatingView = floatingView;
          }}
          {...this._panResponder.panHandlers}
          
        >
          {this.state.componentRender()}
          <View style={stylesControls}>
                      {false && this.state.layout !== FloatingViewConstants.layout.MAXIMIZED &&
                        <Icon type="Feather" name='maximize' style={{fontSize: 40, color: '#777'}} onPress={()=>{
                          this.maximize()
                        }} />
                      }
                      {false && this.state.layout !== FloatingViewConstants.layout.MINIMIZED &&
                        <Icon type="Feather" name='minimize' style={{fontSize: 40, color: '#777'}} onPress={()=>{
                          this.minimize()
                        }} />
                      }
                      <ButtonIcon headerButton={true} imageStyle = {styles.close} imageSource={Assets.equisRoja} onPress={()=>{
                        this.close()
                      }}></ButtonIcon>

                    </View>
        </View>
        {this.props.children}
      </View>
    )
  }
}

