import React, { Component } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions
} from "react-native";
import {
  Icon,
} from "native-base";
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from './FloatingViewConstants'



const styles = StyleSheet.create({
  container: {
    //position: 'absolute',
    //flex: 1,
    backgroundColor: '#fff',
    borderColor: 'red',
    borderWidth: 1
  },
  controlls: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#fff'
  },
});

const video = 'https://dev3.unal.edu.co/fileadmin/user_upload/SedeCaribe_720.mp4'
const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'

export default class FloatingView extends Component {
  constructor(props) {
    super(props)

  }

  render() {
    return(
      <View style={[styles.container]}>
      <TouchableOpacity onPress={() => {
        EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, video)
      }} >  
        <Text>Hola</Text>
      </TouchableOpacity>
      </View>
    )
  }
}

