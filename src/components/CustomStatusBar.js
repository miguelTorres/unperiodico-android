
import React, { Component } from 'react'

import { StyleSheet, View, StatusBar } from 'react-native';
import config from '../../config'

export default class CustomStatusBar extends Component {
  
  render() {

    return(
      <View style={styles.header}>
        <StatusBar barStyle="light-content" backgroundColor={config.colors.config1.background} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    height: 0,
    left: 0,
    right: 0,
    backgroundColor: config.colors.config1.background,
  }

});