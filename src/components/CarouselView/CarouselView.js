import React, { PureComponent } from 'react'
import {
  View,
  Dimensions,
  FlatList,
  RefreshControl,
  StyleSheet,
  TouchableOpacity
} from 'react-native'

import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Spinner,
} from "native-base";

import UnalCarousel from './../react-native-carousel-view/UnalCarousel';

import HTML from 'react-native-render-html';
import config from '../../../config'
import styleVariables from '../../../native-base-theme/variables/unperiodico-material'
import CarouselViewItem from './CarouselViewItem'
import Assets from './../../../assets'

export default class CarouselView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {activeSlide: 0};

  }

  _renderItem = (data, index) => {

    const component = (typeof this.props.renderItem === 'undefined') ? (
      <CarouselViewItem
        data={data}
      />
    ) : this.props.renderItem(data, index)

    
    return (
      <View key={index}>
        <TouchableOpacity activeOpacity={1} style={{flex: 1}}>
          {component}  
        </TouchableOpacity>
      </View>
    )
  };  
  renderSeparator = () => {
    return null
    return (
      <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            height: 1,
            width: "80%",
            backgroundColor: "#CED0CE",
          }}
          />
      </View>
    )
  }
  renderRow = (data) => {
    return (
      <Item data={data} />
    )
  }
  renderFooter = () => {
    const hasNextPage = this.props.hasNextPage()


    if(!hasNextPage || this.props.isRefreshing()) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE",
        }}
        >
          <Spinner />
        </View>
    )
  }

  get pagination () {
    const { activeSlide } = this.state;
    const entries = this.props.data
    return (
        <Pagination
          dotsLength={entries.length}
          activeDotIndex={activeSlide}
          containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.75)' }}
          dotStyle={{
              width: 10,
              height: 10,
              borderRadius: 5,
              marginHorizontal: 8,
              backgroundColor: 'rgba(255, 255, 255, 0.92)'
          }}
          inactiveDotStyle={{
              // Define styles for inactive dots here
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
          carouselRef={this._carousel}
          tappableDots={!!this._carousel}
        />
    );
}  
  render() {
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width
    const $this = this

    if(this.props.data.length == 0){
      return null
    }

    return (
      <View style={{
        height: this.props.height,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <View style={[stylesconst.container, {height: this.props.height}]}>
          <UnalCarousel
            width={width}
            contentContainerStyle={{height: this.props.height}}
            //height={height}
            delay={2000}
            animate={false}
            loop={false}
            indicatorAtBottom={true}
            indicatorSize={20}
            indicator={Assets.indicatorYellow}
            inactiveIndicator={Assets.indicatorWhite}
                //indicatorText="✽"
            //indicatorColor="red"
            {...this.props}
            indicatorSize = {20}
            >
            {this.props.data.map((data, index) => {
              return this._renderItem(data, index)
              return (
                <View key={''+index} style={stylesconst.contentContainer}>
                <Text>Page {index}</Text>
                </View>
              )
            })}
          </UnalCarousel>
        </View>
      </View>
    );

    
    
    

    
  }
}


const stylesconst = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    borderWidth: 2,
    borderColor: '#CCC',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
