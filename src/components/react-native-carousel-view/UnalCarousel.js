import React from 'react'
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native'

import Carousel from 'react-native-carousel-view';
import Assets from '../../../assets'
import { calculateResponsiveValue } from '../../helpers/Responsive';

var indicatorMarginRight = calculateResponsiveValue(6, 35);
var indicatorWidth = calculateResponsiveValue(10, 16);

export default class UnalCarousel extends Carousel {
  renderPageIndicator() {
    const {hideIndicators, indicatorOffset,
      indicatorAtBottom, 
      indicatorColor, inactiveIndicatorColor,
      indicatorSize, indicatorText, inactiveIndicatorText} = this.props;
    const {activePage} = this.state;
    if (hideIndicators === true) {
      return null;
    }
    let indicatorSpace = indicatorWidth + indicatorMarginRight;

    const indicators = [];
    const positionIndicatorStyle = indicatorAtBottom ?
      {bottom: indicatorOffset} :
      {top: indicatorOffset};
    const indicatorWidth = this.children.length * indicatorSpace - indicatorMarginRight;
    let style;
    let position;

    position = {
      minWidth: indicatorWidth,
      left: (this.getWidth() - indicatorWidth) / 2,
    };

    this.children.forEach((child, i) => {
      style = i === activePage ?
        {color: indicatorColor} :
        {color: inactiveIndicatorColor};
      indicators.push(
        <TouchableOpacity key={i.toString()} onPress={() => this.indicatorPressed(i)}>
        <Image 
          
          style={styles.indicator}
          source={i === activePage ? this.props.indicator:this.props.inactiveIndicator}
          
        />
        </TouchableOpacity>

      );
    });

    // only one item don't need indicators
    if (indicators.length === 1) {
      return null;
    }

    return (
      <View style={[styles.pageIndicator, position, positionIndicatorStyle]}>
        {indicators}
      </View>
    );
  }

}


const styles = StyleSheet.create({
  pageIndicator: {
    position: 'absolute',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: 'transparent',
  },
  indicator: {
    marginRight: indicatorMarginRight,
    width: indicatorWidth, height: indicatorWidth, resizeMode: 'contain'
  }
});
