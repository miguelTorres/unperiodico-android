import React from 'react';
import {View,TouchableOpacity,UIManager,findNodeHandle, StyleSheet} from 'react-native';
import {Icon, Button} from 'native-base';
import PropTypes from 'prop-types';

const ICON_SIZE = 24;

const styles = StyleSheet.create({
    headerButton: {
        borderWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderBottomWidth: 0,
      },
  });

class PopupMenu extends React.Component {
  handleShowPopupError = () => {
    // show error here
  };

  handleMenuPress = () => {
    const { actions, onPress } = this.props;

    UIManager.showPopupMenu(
      findNodeHandle(this.refs.menu),
      actions,
      this.handleShowPopupError,
      onPress,
    );
  };

  render() {
    return (
      <View>
        <Button bordered style={styles.headerButton} onPress={this.handleMenuPress}>
        <Icon
            name={this.props.icon.name}
            type={this.props.icon.type}
            //size={ICON_SIZE}
            //color='white'
            ref="menu"
          />
            </Button>
      </View>
    );
  }
}

PopupMenu.propTypes = {
  actions: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default PopupMenu;