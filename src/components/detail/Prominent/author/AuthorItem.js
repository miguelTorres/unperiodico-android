
import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
} from "native-base";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from '../../../FloatingViewConstants'
import Carousel from 'react-native-carousel-view';
import UNCarousel from '../../../Carousel'
import _ from 'underscore'
import moment from 'moment'
require('moment/locale/es');

import Video from 'react-native-af-video-player'
import MusicControl from 'react-native-music-control';

import AudioPlayer from '../../../../components/AudioPlayer'
import IframePlayer from '../../../../components/IframePlayer'


import HTML from '../../../HTML';
import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import Assets from '../../../../../assets'
const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'
import appConfig from '../../../../models/appConfig'
import configurationHelper from '../../../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../../../helpers/MediaSelectorHelper'
import avoidDoubleAction from '../../../../services/avoidDoubleAction';
import { calculateResponsiveValue } from '../../../../helpers/Responsive';

export default class AuthorItem extends PureComponent {

render() {
  const item = this.props.data.item
  
  if(!item.additional || !item.additional.author){
    Alert.alert('que pasa', JSON.stringify(item))
    return null;
  }
  const img = appConfig.config.app.SITE_URL + item.additional.author.image.src

  const categories = item.additional.categories
  const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
  const tagContentType = CONTENT_TYPE? CONTENT_TYPE.tag:null


  const title = '<h1>'+item.title+'</h1>'
  const teaser = item.additional.teaser
  const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
  const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
  const dateSeat = '<div class="date-seat"><p class="date">'+date + seat+'</p></div>'
  const mainCategory = (!!item.additional.mainCategory && false)? '<div class="main-category"><p class="category">'+item.additional.mainCategory.toUpperCase()+'</p></div>' : null
  const author = '<div class="main-author"><p class="author">'+item.additional.author.name+'</p></div>'


  return (
    <ListItem thumbnail style={styles.listItem} >
              <Left style={{alignSelf: 'flex-start', paddingTop: 15}}>
              <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive()){
                        return
                      }
                      this.props.navigation.navigate ('Author', {
                        item
                      });
                    }} >            
                <Thumbnail circular size={55} style={{height:calculateResponsiveValue(55), width:calculateResponsiveValue(55), borderRadius:  calculateResponsiveValue(55)/2}}  source={{uri:img}} />
                  {tagContentType !== null && tagContentType.name!="" &&
                    <Text style={[styles.decorationText, {fontWeight: 'bold', color: tagContentType.color, backgroundColor:tagContentType.bgcolor}]}>{tagContentType.name}</Text>
                  }
                </TouchableOpacity>
              </Left>
              <Body style={styles.listItemBody}>
              <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive()){
                        return
                      }
                      this.props.navigation.navigate ('Author', {
                        item
                      });
                    }} >                  
                
                <HTML stylesConfiguration='listAuthorConfiguration' html={title} />
                
                   
                </TouchableOpacity>
                
                <HTML stylesConfiguration='listAuthorConfiguration' html={author} />
                <HTML stylesConfiguration='listAuthorConfiguration' html={dateSeat} />
                <HTML 
                  stylesConfiguration='listAuthorConfiguration'
                  html={teaser} 
                />
                 <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive()){
                        return
                      }
                      this.props.navigation.navigate ('Author', {
                        item
                      });
                    }} >   
                     <Button small transparent onPress={() => {
                      if(!avoidDoubleAction.isActionActive()){
                        return
                      }
                      this.props.navigation.navigate ('Author', {
                        item
                      });
                    }}>
                      <Text style={{paddingRight: 5, paddingLeft: 0, fontSize: calculateResponsiveValue(12), fontWeight: 'bold'}}>Ver</Text>       
                      <Image
                        style={{width: calculateResponsiveValue(20), height: calculateResponsiveValue(20), resizeMode: 'contain'}}
                        source={Assets.mas}
                      />
                    </Button>
                    
                    </TouchableOpacity>
                    
              </Body>
            </ListItem>
  )

}
}
