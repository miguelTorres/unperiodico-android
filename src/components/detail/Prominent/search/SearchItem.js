

import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Icon,
  Body,
  H3,
} from "native-base";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";

import moment from 'moment'
require('moment/locale/es');

import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from '../../../FloatingViewConstants'
import Carousel from 'react-native-carousel-view';
import UNCarousel from '../../../Carousel'
import _ from 'underscore'

import Video from 'react-native-af-video-player'
import MusicControl from 'react-native-music-control';

import AudioPlayer from '../../../../components/AudioPlayer'
import IframePlayer from '../../../../components/IframePlayer'


import HTML from '../../../HTML';
import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import Assets from '../../../../../assets'
const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'
import appConfig from '../../../../models/appConfig'
import configurationHelper from '../../../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../../../helpers/MediaSelectorHelper'
import tabService from '../../../../services/tabService';
import avoidDoubleAction from '../../../../services/avoidDoubleAction';

export default class SearchItem extends PureComponent {

  constructor(props) {
    super(props)
    this.videoComponent = {}
    this.videosRef = {}
    this.audiosRef = {}
    this.renderMediaPreview = this.renderMediaPreview.bind(this)
    
    this.onPageChange = this.onPageChange.bind(this)
    this.state = {
      isFullScreen: true
    }
  }
  
  onFullScreen(status){
    this.props.toggleFullScreen(status)
  }
  createRenderFunction(mediaObject, index){
    var functionName = 'render' + this.selector.type + 'Component'
    if(typeof this[functionName]!=='undefined'){
      return ()=>{
        return this[functionName](mediaObject, index)
      }
    }
    return ()=>{
      return null
    }

  }



  renderMediaPreview(mediaObject, index){
    const item = this.props.data.item
    const url = mediaObject.media!==null?this.selector.getUrl(mediaObject.media):null
    const preview = mediaObject.preview
    const categories = item.additional.categories
    const configuration = configurationHelper.getConfiguration(this.props.configuration, categories)
    const tag = configuration? configuration.tag:null

    const componentRender = this.createRenderFunction(mediaObject, index)
    const initialFloatingState = this.selector.getInitialFloatingState()
    const block = this.selector.getBlock()
    let imageStyle = styles.imageInCarousel
    if(typeof index === 'undefined'){
      imageStyle = styles.imageOutCarousel
    }

    const icon = this.selector.getIcon()
    
    
    //return null
    const view = (
      <View>
        <Image
                style={imageStyle}
            source={{uri: preview}}
          />
          {icon && 
            <View style={[styles.containerPreviewIcon]}>
              <Icon active type={icon.type} name={icon.name} style={styles.previewIcon} />
            </View>
          }
          {tag !== null && 
          <View style={styles.decoration}>
            <Text style={[styles.decorationText, {color: tag.color, backgroundColor:tag.bgcolor}]}>{tag.name}</Text>
          </View>
          }
      </View>
    )

    if(url === null && mediaObject.media == null){
      return view
    }

    return (
        <TouchableOpacity activeOpacity={0.8} onPress={() => {
          EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, {url, componentRender, initialFloatingState, block})
        }} >                  
          {view}
        </TouchableOpacity>
    )    


  }
  


  onPageChange (index){
    _.each(this.videosRef, (video)=>{
      video.pause()
    })
  }
 
  renderMediaPreviewBySelector(selector){
    const mediaObjects = selector.getMediaObjects()
    const media = this.renderMediaPreview(mediaObjects[0])
    return media
  }
  isShowMainCategory(){

    return !(typeof this.props.showMainCategory === 'undefined' || this.props.showMainCategory*1==0)
  }

  render() {
    const item = this.props.data.item


    const mediaSelectorHelper = new MediaSelectorHelper(item, true)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(0)
    const mediaPreview = this.renderMediaPreviewBySelector(selector)
    const medias = selector.getMedias()

    const categories = item.additional.categories
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE? CONTENT_TYPE.tag:null
    const isAuthor = (tagContentType && (!!tagContentType.name) && tagContentType.name==='COLUMNA')
    const authorImage = (tagContentType && (!!tagContentType.name) && tagContentType.name==='COLUMNA' && (!!item.additional.imageAuthor))? 
    appConfig.config.app.SITE_URL + item.additional.imageAuthor.src : null;

    const title = '<h1>'+item.title+'</h1>'
    const teaser = item.additional.teaser
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<date>'+date + seat+'</date>'
    const mainCategory = (!!item.additional.mainCategory && this.isShowMainCategory())? '<category>'+item.additional.mainCategory.toUpperCase()+'</category>' : null
    return (
      <ListItem thumbnail style={styles.listItem} >
                <Left style={{alignSelf: 'flex-start', paddingTop: 15}}>
                <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      if(isAuthor){
                        this.props.navigation.navigate ('Author', {
                          item
                        });
                      }else{
                        this.props.navigation.navigate ('Detail', {
                          item
                        });
                      }

                    }} >                  
                  <Thumbnail circular size={55} source={{uri:(!!authorImage)?authorImage:img}} />
                  {tagContentType !== null && tagContentType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagContentType.color, backgroundColor:tagContentType.bgcolor}]}>{tagContentType.name}</Text>
                  }
                  
                  </TouchableOpacity>
                </Left>
                <Body style={styles.listItemBody}>
                  <TouchableOpacity onPress={() => {
                    if(!avoidDoubleAction.isActionActive('detail')){
                      return
                    }
                    if(isAuthor){
                      this.props.navigation.navigate ('Author', {
                        item
                      });
                    }else{
                      this.props.navigation.navigate ('Detail', {
                        item
                      });
                    }
                }} >   

                  {mainCategory && 
                    <HTML stylesConfiguration='listConfiguration' html={mainCategory} />
                  }
                  <HTML stylesConfiguration='listConfiguration' html={dateSeat} />
                  <HTML stylesConfiguration='listConfiguration' html={title} />
                  <HTML 
                    stylesConfiguration='listConfiguration'
                    html={teaser} 
                  />
                  </TouchableOpacity>
                  
                    <Button small transparent onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      if(isAuthor){
                        this.props.navigation.navigate ('Author', {
                          item
                        });
                      }else{
                        this.props.navigation.navigate ('Detail', {
                          item
                        });
                      }
                    }}>
                      <Text style={{paddingRight: 5, paddingLeft: 0}}>Ver</Text>       
                      <Image
                        style={{width: 20, height: 20, resizeMode: 'contain'}}
                        source={Assets.mas}
                      />
                    </Button>
                </Body>
              </ListItem>
    )
    
  }
}
