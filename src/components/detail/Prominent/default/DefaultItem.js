
import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Icon,
  Body,
  H3,
} from "native-base";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";

import moment from 'moment'
require('moment/locale/es');

import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from '../../../FloatingViewConstants'
import Carousel from 'react-native-carousel-view';
import UNCarousel from '../../../Carousel'
import _ from 'underscore'

import Video from 'react-native-af-video-player'
import MusicControl from 'react-native-music-control';

import AudioPlayer from '../../../../components/AudioPlayer'
import IframePlayer from '../../../../components/IframePlayer'


import HTML from '../../../HTML';
import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import Assets from '../../../../../assets'
const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'
import appConfig from '../../../../models/appConfig'
import configurationHelper from '../../../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../../../helpers/MediaSelectorHelper'
import tabService from '../../../../services/tabService';
import avoidDoubleAction from '../../../../services/avoidDoubleAction';
import { calculateResponsiveValue } from '../../../../helpers/Responsive';

export default class DefaultItem extends PureComponent {

  constructor(props) {
    super(props)
    this.videoComponent = {}
    this.videosRef = {}
    this.audiosRef = {}
    this.renderMediaPreview = this.renderMediaPreview.bind(this)
    
    this.onPageChange = this.onPageChange.bind(this)
    this.state = {
      isFullScreen: true
    }
  }
  
  onFullScreen(status){
    this.props.toggleFullScreen(status)
  }
  createRenderFunction(mediaObject, index){
    var functionName = 'render' + this.selector.type + 'Component'
    if(typeof this[functionName]!=='undefined'){
      return ()=>{
        return this[functionName](mediaObject, index)
      }
    }
    return ()=>{
      return null
    }

  }

  renderMediaComponentBySelector(mediaObject, index){
    const component = (
      <View>
        <Text>Media {this.selector.type} {this.selector.getUrl(mediaObject.media)}</Text>
      </View>
    )
    var functionName = 'render' + this.selector.type + 'Component'
    if(typeof this[functionName]!=='undefined'){
      return this[functionName](mediaObject, index)
    }
    return component
  }

  renderMediaPreview(mediaObject, index){
    const item = this.props.data.item
    const url = mediaObject.media!==null?this.selector.getUrl(mediaObject.media):null
    const preview = mediaObject.preview
    const categories = item.additional.categories
    const configuration = configurationHelper.getConfiguration(this.props.configuration, categories)
    const tag = configuration? configuration.tag:null

    //const component = this.renderMediaComponentBySelector(mediaObject, index)
    const componentRender = this.createRenderFunction(mediaObject, index)
    const initialFloatingState = this.selector.getInitialFloatingState()
    const block = this.selector.getBlock()
    let imageStyle = styles.imageInCarousel
    if(typeof index === 'undefined'){
      imageStyle = styles.imageOutCarousel
    }

    const icon = this.selector.getIcon()
    
    
    //return null
    const view = (
      <View>
        <Image
                style={imageStyle}
            source={{uri: preview}}
          />
          {icon && 
            <View style={[styles.containerPreviewIcon]}>
              <Icon active type={icon.type} name={icon.name} style={styles.previewIcon} />
            </View>
          }
          {tag !== null && 
          <View style={styles.decoration}>
            <Text style={[styles.decorationText, {color: tag.color, backgroundColor:tag.bgcolor}]}>{tag.name}</Text>
          </View>
          }
      </View>
    )

    if(url === null && mediaObject.media == null){
      return view
    }

    return (
        <TouchableOpacity activeOpacity={0.8} onPress={() => {
          EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, {url, componentRender, initialFloatingState, block})
        }} >                  
          {view}
        </TouchableOpacity>
    )    


  }
  


  onPageChange (index){
    _.each(this.videosRef, (video)=>{
      video.pause()
    })
  }
  renderCarousel(data, _renderItem){
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width
    const indicatorAtBottom = this.props.layout.isFullScreen? false:true
    const indicatorSize = this.props.layout.isFullScreen? 0:20

    const dimensions = this.props.layout.isFullScreen? {width, height}:{}
    
    return (
      <View style={styles.prominentCarouselContainer}>
      <View>
        <UNCarousel
            delay={2000}
            animate={false}
            loop={false}
            indicatorAtBottom={indicatorAtBottom}
            indicatorSize={indicatorSize}
            onPageChange = {this.onPageChange}
            renderItem = {_renderItem}
            data = {data}
            isFullScreen = {false}
            //indicatorText="✽"
            //indicatorColor="red"
            >
          </UNCarousel>
      </View>
      </View>
      
    );
    
  }
  renderAudioComponent(mediaObject, index){
    var audio = mediaObject.media
    const item = this.props.data.item
    const url = this.selector.getUrl(mediaObject.media)
    const description = 'Audio '+(index+1)
    const title = description + '. '+item.title

    return (
      <View>
        <AudioPlayer 
          //style={{height: 0}}
          url={url}
          title={title}
          description={description}
          logo={logo} 
          controls={false}
        />
      </View>
    )


  }
  renderVideo(video, index){
    video = typeof video.item === 'undefined'? video: video.item
    const item = this.props.data.item
    const url = appConfig.config.app.SITE_URL + '/' + video.sources[0].src
    //return null

    if(typeof this.videoComponent[index] === 'undefined'){
      this.videoComponent[index] = (
        <View key={''+index} style={styles.prominentItemContainer}>
          <Video 
          style={{height: 0}}
          autoPlay={true}
          //onFullScreen={status => this.onVideoFullScreen(status)} 
          ref={ref => {      this.videosRef[index]=ref;    }} 
          url={url} 
          logo={logo} 
          playInBackground={true}
          controls={false}
          />
        </View>
      )
    }
  
    return this.videoComponent[index]
  }  
  renderVideoComponent(mediaObject, index){
    var video = mediaObject.media
    const item = this.props.data.item
    const url = this.selector.getUrl(mediaObject.media)
    const description = 'Audio '+(index+1)
    const title = description + '. '+item.title

    return (
      <View key={''+index} style={styles.prominentItemContainer}>
        <Video 
        autoPlay={true}
        //onFullScreen={status => this.onVideoFullScreen(status)} 
        ref={ref => {      this.videosRef[index]=ref;    }} 
        url={url} 
        logo={logo} 
        playInBackground={true}
        controls={false}
        //fullScreenOnly
        //inlineOnly
        
        />
    </View>
)


  }
  renderIframeComponent(mediaObject, index){
    const html = mediaObject.media
    return <IframePlayer html={html} />
  }
  renderMediaPreviewBySelector(selector){
    const mediaObjects = selector.getMediaObjects()
    const media = mediaObjects.length === 1?
      this.renderMediaPreview(mediaObjects[0]):
      this.renderCarousel(mediaObjects, this.renderMediaPreview)
    return media
  }
  isShowMainCategory(){

    return !(typeof this.props.showMainCategory === 'undefined' || this.props.showMainCategory*1==0)
  }

  render() {
    const item = this.props.data.item


    const mediaSelectorHelper = new MediaSelectorHelper(item)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(0)

    const categories = item.additional.categories
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE? CONTENT_TYPE.tag:null

    const isAuthor = (tagContentType && (!!tagContentType.name) && tagContentType.name==='COLUMNA')
    const authorImage = (tagContentType && (!!tagContentType.name) && tagContentType.name==='COLUMNA' && (!!item.additional.imageAuthor))? 
    appConfig.config.app.SITE_URL + item.additional.imageAuthor.src : null;

    const title = '<h1>'+item.title+'</h1>'
    const teaser = item.additional.teaser
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<div class="date-seat"><p class="date">'+date + seat+'</p></div>'
    const mainCategory = (!!item.additional.mainCategory && this.isShowMainCategory())? '<div class="main-category"><p class="category">'+item.additional.mainCategory.toUpperCase()+'</p></div>' : null


    const author = item.additional.author? {
      name:  
      '<div class="main-author"><p class="author">'+item.additional.author.name+'</p></div>'
      ,
      title: item.additional.author.title,
      firstName: item.additional.author.firstName,
      lastName: item.additional.author.lastName,
    }:null
    const authorHtml = author? '<div class="main-author"><p class="author">'+author.firstName+'</p></div>':
    ''

    //cambio
    return (
      <ListItem thumbnail style={styles.listItem} >
                <Left style={{alignSelf: 'flex-start', paddingTop: 15}}>
                <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      if(isAuthor){
                        this.props.navigation.navigate ('Author', {
                          item
                        });
                      }else{
                        this.props.navigation.navigate ('Detail', {
                          item,
                          saveNews: this.props.saveNews
                        });
                      }
                      
                    }} >                  
                  <Thumbnail circular size={calculateResponsiveValue(55)} style={{height:calculateResponsiveValue(55), width:calculateResponsiveValue(55), borderRadius:  calculateResponsiveValue(55)/2}} source={{uri:(!!authorImage)?authorImage:img}} />
                  {tagContentType !== null && tagContentType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagContentType.color, backgroundColor:tagContentType.bgcolor, fontWeight: 'bold'}]}>{tagContentType.name}</Text>
                  }
                  
                  </TouchableOpacity>
                </Left>
                <Body style={styles.listItemBody}>
                {mainCategory && 
                    <HTML stylesConfiguration='listConfiguration' html={mainCategory} />
                  }
                  <HTML stylesConfiguration='listConfiguration' html={dateSeat} />
                  <TouchableOpacity onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      if(isAuthor){
                        this.props.navigation.navigate ('Author', {
                          item
                        });
                      }else{
                        this.props.navigation.navigate ('Detail', {
                          item,
                          saveNews: this.props.saveNews
                        });
                      }

                  }} >   
                  

                 
                  <HTML stylesConfiguration='listConfiguration' html={title} />
                  
                  </TouchableOpacity>
                  <HTML 
                    stylesConfiguration='listConfiguration'
                    html={teaser} 
                  />
                  
                    <Button small transparent onPress={() => {
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                      if(isAuthor){
                        this.props.navigation.navigate ('Author', {
                          item
                        });
                      }else{
                        this.props.navigation.navigate ('Detail', {
                          item,
                          saveNews: this.props.saveNews
                        });
                      }

                    }}>
                      <Text style={{paddingRight: 5, paddingLeft: 0, fontSize: calculateResponsiveValue(12), fontWeight: 'bold'}}>Ver</Text>       
                      <Image
                        style={{width: calculateResponsiveValue(20), height: calculateResponsiveValue(20), resizeMode: 'contain'}}
                        source={Assets.mas}
                      />
                    </Button>
                </Body>
              </ListItem>
    )
    
  }
}
