
import React, { PureComponent } from 'react'
import {
  View,
  Image,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Card,
  CardItem,
  Icon,

} from "native-base";
import HTML from '../../../HTML';
import moment from 'moment'
require('moment/locale/es');

import config from '../../../../../config'
import styleVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import appConfig from '../../../../models/appConfig';
import Assets from '../../../../../assets'
import configurationHelper from '../../../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../../../helpers/MediaSelectorHelper'
import RelatedNews from '../../../relatednews/RelatedNews';
import Multimedia from '../../../multimedia/Multimedia';
import avoidDoubleAction from '../../../../services/avoidDoubleAction';
import { EventRegister } from 'react-native-event-listeners';
import FloatingViewConstants from '../../../FloatingViewConstants';


export default class ProminentCarouselViewItem extends PureComponent {
  constructor(props){
    super(props)
    this.onPressItem = this.onPressItem.bind(this)

  }
  onPressItem(item){
    if(!avoidDoubleAction.isActionActive('detail')){
      return
    }

    const special = this.props.data
    this.props.navigation.navigate ('Detail', {
      item,
      related: special.additional.related,
      special
    });
  }

  renderRelated(item){
    if(item.additional.related && item.additional.related.length > 0){
      return (
        <RelatedNews title='Noticias en este especial' related={item.additional.related} navigation={this.props.navigation} onPressItem={this.onPressItem} />
      )
    }
    return null
  }
  getIconNewsType(icon){
    if(typeof icon === 'string'){
      return(
        <Image
        style={{width: 75, height: 75, opacity: 0.8, resizeMode: 'contain'}}
        source={{uri: icon}}
        />
      )
    }
    if(!!icon && !!icon.name){
      return (
        <Icon active type={icon.type} name={icon.name} style={[styles.previewIcon, icon.style]} />
      )  
    }
    return null
  }

  render() {
    const item = this.props.data

    const mediaSelectorHelper = new MediaSelectorHelper(item, true)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(1)




    const categories = item.additional.categories
    const NEWS_TYPE = configurationHelper.getConfiguration(this.props.NEWS_TYPE, categories)
    const tagNewsType = NEWS_TYPE && NEWS_TYPE.tag!=''? NEWS_TYPE.tag:null
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE && CONTENT_TYPE.tag!=''? CONTENT_TYPE.tag:null


    const title = '<h1>'+item.title+'</h1>'
    const teaser = item.additional.teaser
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<div class="wraper-date"><p class="date">'+date + seat+'</p></div>'
    const mainCategory = (!!item.additional.mainCategory)? '<div class="wraper-category"><p class="category">'+item.additional.mainCategory.toUpperCase()+'</p></div>' : null

    const author = item.additional.author? {
      name:  
      '<div class="main-author"><p class="author">'+item.additional.author.name+'</p></div>'
      ,
      title: item.additional.author.title,
      firstName: item.additional.author.firstName,
      lastName: item.additional.author.lastName,
    }:null
    const authorHtml = author? '<div class="main-author"><p class="author">'+author.firstName+'</p></div>':
    ''

    const icon = CONTENT_TYPE? CONTENT_TYPE.icon:null

    const iconNewsType = this.getIconNewsType(icon)


    return (
      <ListItem style={styles.listItem} >
                <View style={styles.listItemBody}>
                <Multimedia {...this.props} background={true} item={item} hideTags={true} hideCaptionOnPreview={true} hideIconOnpreview={true} />
                <View style={styles.content}>
                  <View style={styles.innerText}>
                  {mainCategory && 
                          <HTML stylesConfiguration='carouselConfiguration' html={mainCategory} />
                        }
                        <HTML stylesConfiguration='carouselConfiguration' html={dateSeat} />
                        <TouchableOpacity onPress={() => {
                          if(!avoidDoubleAction.isActionActive('detail')){
                            return
                          }
                          this.props.navigation.navigate ('Detail', {
                            item
                          });
                        }} >
                        <HTML stylesConfiguration='carouselConfiguration' html={title} />
                        </TouchableOpacity>
                        <HTML 
                          stylesConfiguration='carouselConfiguration'
                          html={teaser}
                        />
                        </View>
                  {this.renderRelated(item)}
                
                  </View>   
                  <View style={styles.decoration}>
                  {tagNewsType !== null && tagNewsType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagNewsType.color, backgroundColor:tagNewsType.bgcolor, fontWeight: 'bold',}]}>{tagNewsType.name}</Text>
                  }
                  {tagContentType !== null && tagContentType.name!="" &&
                    <Text style={[styles.decorationText, {color: tagContentType.color, backgroundColor:tagContentType.bgcolor, fontWeight: 'bold',}]}>{tagContentType.name}</Text>
                  }
                  </View>
                  {iconNewsType &&
                    <View style={[styles.containerPreviewIcon]} >
                    <TouchableOpacity activeOpacity={0.8} onPress={()=>{
                      EventRegister.emit(FloatingViewConstants.PREVIEW_FUNCTION, null)
                    }} >                  
                    {iconNewsType}
                    </TouchableOpacity>
                    </View>
                  }

                </View>
              </ListItem>
    )

    


  }
}
