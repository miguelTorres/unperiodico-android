import unperiodicoVariables from '../../../../../native-base-theme/variables/unperiodico-material'
import { responsiveFontSize, calculateResponsiveValue } from '../../../../helpers/Responsive';
export default {
  content: {
    
    
    position: 'absolute',
    //paddingTop: 400,
    
    left: 0,
    right: 0,
   
    bottom: 0,
    //backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    
  },
  innerText: { //texto del carrusel
    paddingLeft: calculateResponsiveValue(20, 60),
    paddingRight: calculateResponsiveValue(20, 60),
    paddingBottom: calculateResponsiveValue(20, 48),
    //justifyContent: 'flex-start',
    alignItems: 'flex-start',
    
  },
  container: {
    backgroundColor: "#FFF",
  },
  decoration: {
    position: 'absolute',
    top: calculateResponsiveValue(20, 45),
    left: calculateResponsiveValue(15, 35),
    flexDirection: 'row',
    flexWrap:'wrap'

  },
  decorationText: {
    padding: calculateResponsiveValue(2, 5),
    paddingLeft: calculateResponsiveValue(10, 12),
    paddingRight: calculateResponsiveValue(10, 12),
    marginLeft: calculateResponsiveValue(5),
    marginRight: calculateResponsiveValue(5),
    fontSize: calculateResponsiveValue(9, 18),
    marginRight: 0,
    opacity: 0.9
  },

  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  listItem: {
    /*flex: 1,*/
    //borderBottomColor: '#bdbdbd',
    paddingBottom: 0,
    paddingTop: 0,
    borderBottomWidth: 0,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
  /*alignItems: 'flex-end', 
    justifyContent: 'flex-end',*/
  },
  listItemBody: {
    //flex: 1,
    width: '100%',
    borderBottomWidth: 0,

    //borderColor: 'red'
    //borderColor: 'rgba(256, 256, 256, 0)'
  },
  mb: {
    marginBottom: 15,
    padding: 10
  },
  prominentItemContainer: {
    flex: 1,
    justifyContent: 'center',
    /*paddingBottom: 50,
    marginBottom: 50,*/
  },
  prominentCarouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },
  previewIcon: {
    color: unperiodicoVariables.titleFontColor,
    fontSize: 75,
    opacity: 0.6
  },
  containerPreviewIcon: {
    position: 'absolute',
    top: calculateResponsiveValue(10, 35),
    right: calculateResponsiveValue(10, 35),
    
    //backgroundColor: 'rgba(255, 255, 255, 0.4)',
  },
};
