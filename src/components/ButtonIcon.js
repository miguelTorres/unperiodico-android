
import React, { Component } from 'react'

import { Button, Icon } from 'native-base';
import { StyleSheet, Image } from 'react-native';
import { calculateResponsiveValue } from '../helpers/Responsive';


export default class ButtonIcon extends Component {
  
  render() {

    const buttonStyle = !!this.props.headerButton? styles.headerButton:null
    const isIcon = typeof this.props.icon !== 'undefined'
    if(isIcon){
      return(
        <Button bordered style={buttonStyle} onPress={this.props.onPress}>
          <Icon name={this.props.icon.name} style={styles.icon} type={this.props.icon.type} />
        </Button>
      )        
    }
    return(
      <Button bordered style={buttonStyle} onPress={this.props.onPress}>
        <Image style={[styles.image, this.props.imageStyle]} source={this.props.imageSource} />
      </Button>
    )
  }
}

const styles = StyleSheet.create({
  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 5,
    paddingBottom: 5,
    //height: 34,
  },
  image:{
    width: calculateResponsiveValue(25, 30),
    height: calculateResponsiveValue(25, 30),
    resizeMode: 'contain'
  },
  icon: {
    marginLeft: 5,
    marginRight: 5,
    fontSize: calculateResponsiveValue(25, 30),
  }

});
