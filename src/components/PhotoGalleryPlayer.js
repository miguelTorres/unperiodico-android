import React from 'react';

import { StyleSheet, Text, View, Alert, TouchableOpacity } from 'react-native';
import {
  Icon,
  Button
} from "native-base";
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from './FloatingViewConstants'
import Gallery from 'react-native-image-gallery';

const styles = StyleSheet.create({
  imageTextContainer: {
    backgroundColor: 'rgba(55, 55, 55, 0.5)',
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  imageText: {
    padding: 5,
    fontSize: 18,
    color: 'white'
  },
});


export default class PhotoGalleryPlayer extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      showText: true,
      index: 0
    }
  }
  componentDidMount() {
    this.isComponentMount = true
    this.listenerMaximized = EventRegister.addEventListener(FloatingViewConstants.layout.MAXIMIZED, () => {
      this.isComponentMount && this.setState({
        showText: true
      })
    })
    this.listenerMinimized = EventRegister.addEventListener(FloatingViewConstants.layout.MINIMIZED, () => {
      this.isComponentMount && this.setState({
        showText: false
      })
    })
  }

  componentWillUnmount() {
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerMaximized)
      EventRegister.removeEventListener(this.listenerMinimized)
  }  
  onPageSelected(index){
    this.props.onPageSelected(index)
    this.isComponentMount && this.setState({
      ...this.state,
      index
    })
  }

  render() {

    const figCaption = this.props.images[this.state.index].figCaption
    return (
      <View style={{ flex: 1}}>
      <Gallery
      {...this.props}
      onPageSelected={(x)=>{this.onPageSelected(x)}}
      />
      {this.state.showText && figCaption &&
      <View style={styles.imageTextContainer}>
      <TouchableOpacity activeOpacity={0.8} onPress={() => {
        Alert.alert('Pié de foto', figCaption)
      }} >
      <Text style={[styles.imageText]} numberOfLines={3}>{figCaption}</Text>
      </TouchableOpacity>
      </View>
      }

      </View>
    );
  }
}
