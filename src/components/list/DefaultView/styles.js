export default {
  container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  listItem: {
    /*flex: 1,*/
    borderBottomColor: '#bdbdbd',

    borderBottomWidth: 0.5,
    paddingLeft: 0,
    marginLeft: 0,
    paddingRight: 0,
    marginRight: 0,
    /*alignItems: 'flex-end', 
    justifyContent: 'flex-end',*/
  },
  listItemBody: {
    flex: 1,
    borderColor: 'rgba(256, 256, 256, 0)'
  },
  mb: {
    marginBottom: 15
  },
  prominentItemContainer: {
    flex: 1,
    justifyContent: 'center',
    /*paddingBottom: 50,
    marginBottom: 50,*/
  },
  prominentCarouselContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#dd0'
  },
};
