import React, { PureComponent } from 'react'
import {
  View,
  ScrollView,
  Dimensions,
  FlatList,
  RefreshControl,
  StyleSheet
} from 'react-native'

import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Spinner,
} from "native-base";

//import Carousel from 'react-native-carousel-view';
import UnalCarousel from '../../react-native-carousel-view/UnalCarousel'
import Assets from '../../../../assets'

import HTML from 'react-native-render-html';
import config from '../../../../config'
import styleVariables from '../../../../native-base-theme/variables/unperiodico-material'
import RecientesCarouselViewItem from '../../detail/CarouselView/Recientes/RecientesCarouselViewItem'
import ProminentCarouselViewItem from '../../detail/CarouselView/Prominent/ProminentCarouselViewItem'
import ImageBackgroundItem from '../../detail/CarouselView/ImageBackground/ImageBackgroundItem'
import CustomRefreshControl from '../../CustomRefreshControl';
import { calculateResponsiveValue } from '../../../helpers/Responsive';

export default class CarouselView extends PureComponent {
  constructor(props) {
    super(props);
    this.setContentWidth = this.setContentWidth.bind(this)
    this.state = {activeSlide: 0};

  }

  _renderItem = (data, index) => {
    switch (this.props.itemView) {
      case 'ProminentCarouselViewItem':
      
      return(
        <View key={index}>
          <ProminentCarouselViewItem
            {...this.props}
            data={data}
          />
        </View>
      )
      case 'RecientesCarouselViewItem':
      
      return(
        <View key={index}>
          <RecientesCarouselViewItem
            {...this.props}
            data={data}
          />
        </View>
      )
    
        break;
    
      default:
        return(
          <View key={index}>
          <ImageBackgroundItem
            {...this.props}
            data={data}
          />
        </View>
        )
      }

  };  
  renderSeparator = () => {
    return null
    return (
      <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            height: 1,
            width: "80%",
            backgroundColor: "#CED0CE",
          }}
          />
      </View>
    )
  }
  renderRow = (data) => {
    return (
      <Item data={data} />
    )
  }
  renderFooter = () => {
    const hasNextPage = this.props.hasNextPage()


    if(!hasNextPage || this.props.isRefreshing()) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE",
        }}
        >
          <Spinner />
        </View>
    )
  }

  setContentWidth(event){
    this.setState({
      contentWidth: event.nativeEvent.layout.width
    })
  }

  render() {
    const isRefreshing = this.props.isRefreshing()
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width
    const $this = this

    if(this.props.data.length == 0){
      return null
    }
    if(!this.props.itemView){
      return null
    }
    return (
      <View style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
      onLayout={this.setContentWidth}
      >
      {(!!this.state.contentWidth) && (!!this.props.contentHeight) &&
              <ScrollView contentContainerStyle={stylesconst.container}
              refreshControl={
                <CustomRefreshControl
                  refreshing={isRefreshing}
                  onRefresh={this.props.handleRefresh}
                />
              }
            >
              <UnalCarousel
                width={this.state.contentWidth}
                height={this.props.contentHeight}
                delay={2000}
                animate={false}
                loop={false}
                indicatorAtBottom={true}
                indicatorSize={20}
                indicatorColor={config.colors.config2.color}
                inactiveIndicatorColor={config.colors.config2.background}
                indicatorOffset = {calculateResponsiveValue(10, 40)}
                indicator={Assets.indicador}
                inactiveIndicator={Assets.indicadorInactivo}
                //indicatorText="✽"
                //indicatorColor="red"
                >
                {this.props.data.map((data, index) => {
                  return this._renderItem(data, index)
                  return (
                    <View key={''+index} style={stylesconst.contentContainer}>
                    <Text>Page {index}</Text>
                    </View>
                  )
                })}
              </UnalCarousel>
            </ScrollView>
      }

      </View>
    );

    
  }
}


const stylesconst = StyleSheet.create({
  container: {
    //flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    borderWidth: 2,
    borderColor: '#CCC',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
