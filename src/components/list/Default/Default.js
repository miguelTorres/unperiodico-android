import React, { PureComponent } from 'react'
import {
  View,
  Image,
  Dimensions,
  FlatList,
  RefreshControl
} from 'react-native'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H3,
  Spinner,
} from "native-base";
import Assets from '../../../../assets'
import styles from './styles'
import Video, { ScrollView, Container } from 'react-native-af-video-player'

import HTML from 'react-native-render-html';
import config from '../../../../config'
import styleVariables from '../../../../native-base-theme/variables/unperiodico-material'
import DefaultItem from '../../detail/Prominent/default/DefaultItem'
import AuthorItem from '../../detail/Prominent/author/AuthorItem'
import SondeoItem from '../../detail/Prominent/sondeo/SondeoItem'
import ProminentItem from '../../detail/Prominent/prominent/ProminentItem'
import SearchItem from '../../detail/Prominent/search/SearchItem';
import CustomRefreshControl from '../../CustomRefreshControl';


const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'


export default class Default extends PureComponent {
  constructor(props) {
    super(props);

    
  }
  _renderItem = (data) => (
    <View>
      <DefaultItem
        {...this.props}
        data={data}
      />
    </View>
  );  
  renderSeparator = () => {
    return null
    return (
      <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row'}}>
        <View
          style={{
            height: 1,
            width: "80%",
            backgroundColor: "#CED0CE",
          }}
          />
      </View>
    )
  }
  renderRow = (data) => {
    
    if(this.props.view == "Authors"){
      return (
        <AuthorItem {...this.props} data={data} showMainCategory={1} />
      )  
    }
    if(this.props.view == "Sondeos"){
      return (
        <SondeoItem {...this.props} data={data} />
      )  
    }
    if(this.props.view == "Prominent"){
      return (
        <ProminentItem {...this.props} data={data} showMainCategory={1} />
      )  
    }
    if(this.props.view == "Search"){
      return (
        <SearchItem {...this.props} data={data} showMainCategory={1} />
      )  
    }
    return (
      <DefaultItem {...this.props} data={data} />
    )
  }
  getStyleContainer = () => {

    const containerStyles = {
      "Authors": styles.authorContainer,
      "Sondeos": null,
      "Prominent": null,
      "Search": styles.authorContainer,
    }

    if(typeof containerStyles[this.props.view] !== 'undefined'){
      return containerStyles[this.props.view]
    }
    return styles.defaultContainer
    
  }  
  renderFooter = () => {
    const hasNextPage = this.props.hasNextPage()
    const footerByFiltroFecha = (
      <View style={{height: this.props.auxFooterHeight}}></View>
    )


    if(!hasNextPage || this.props.isRefreshing()) return footerByFiltroFecha;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE",
        }}
        >
          <Spinner />
        </View>
    )
  }

  onFullScreen(status){
    this.props.toggleFullScreen()
  }   
  
  render() {
    const isRefreshing = this.props.isRefreshing()
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width


    if(!this.props.data || this.props.data.length === 0)
    {
      return null
    }
    const styleContainer = this.getStyleContainer()
    return(
      <View style={styleContainer}>
      <FlatList
        data={this.props.data}
        renderItem={this.renderRow}
        keyExtractor={item=>""+item.uid}
        ItemSeparatorComponent={this.renderSeparator}
        ListFooterComponent={this.renderFooter}
        refreshing={isRefreshing}
        onRefresh={this.props.handleRefresh}
        onEndReached={this.props.onEndReached}
        
        refreshControl={
          <CustomRefreshControl
            colors={["red", "green", "blue"]}
            refreshing={true}
            onRefresh={this.props.onRefreshControl}
            />
        }          
        /> 
        </View> 
      
    )
  }
}
