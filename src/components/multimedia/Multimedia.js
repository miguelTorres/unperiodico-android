
import React, { PureComponent } from 'react'
import {
  Button,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Icon,
  Body,
  H3,
} from "native-base";
import {
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import { EventRegister } from 'react-native-event-listeners'
import FloatingViewConstants from '../FloatingViewConstants'
import Carousel from 'react-native-carousel-view';
import UNCarousel from '../Carousel'
import CarouselView from '../CarouselView/CarouselView'
import _ from 'underscore'

import Video from 'react-native-af-video-player'
import MusicControl from 'react-native-music-control';

import AudioPlayer from '../../components/AudioPlayer'
import VideoPlayer from '../../components/VideoPlayer'
import IframePlayer from '../../components/IframePlayer'



import HTML from 'react-native-render-html';
import config from '../../../config'
import styleVariables from '../../../native-base-theme/variables/unperiodico-material'
import styles from "./styles"
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const logo = 'https://unperiodico.unal.edu.co/fileadmin/user_upload/images/transparente.png'
import configurationHelper from '../../helpers/ConfigurationHelper'
import MediaSelectorHelper from '../../helpers/MediaSelectorHelper'
import PhotoGalleryPlayer from '../PhotoGalleryPlayer';
import appConfig from '../../models/appConfig';
import Assets from '../../../assets'
import { calculateResponsiveValue } from '../../helpers/Responsive';
export default class Multimedia extends PureComponent {

  constructor(props) {
    super(props)
    this.videoComponent = {}
    this.videosRef = {}
    this.audiosRef = {}
    this.renderMediaPreview = this.renderMediaPreview.bind(this)
    
    this.onPageChange = this.onPageChange.bind(this)
    this.state = {
      isFullScreen: true
    }
  }
  async componentDidMount() {

    this.listenerPreviewFunction = EventRegister.addEventListener(FloatingViewConstants.PREVIEW_FUNCTION, () => {
      if(!!this.previewFunction){
        this.previewFunction()
      }
    })

    
  }
  componentWillUnmount() 
  {
    EventRegister.removeEventListener(this.listenerPreviewFunction)
  }  
  
  onFullScreen(status){
    this.props.toggleFullScreen(status)
  }
  createRenderFunction(mediaObject, index){
    var functionName = 'render' + this.selector.type + 'Component'
    if(typeof this[functionName]!=='undefined'){
      return ()=>{
        return this[functionName](mediaObject, index)
      }
    }
    return ()=>{
      return null
    }

  }

  renderMediaComponentBySelector(mediaObject, index){
    const component = (
      <View>
        <Text>Media {this.selector.type} {this.selector.getUrl(mediaObject.media)}</Text>
      </View>
    )
    var functionName = 'render' + this.selector.type + 'Component'
    if(typeof this[functionName]!=='undefined'){
      return this[functionName](mediaObject, index)
    }
    return component
  }
  getIconNewsType(icon){
    if(typeof icon === 'string'){
      return(
        <Image
        style={{width: 75, height: 75, opacity: 0.8, resizeMode: 'contain'}}
        source={{uri: icon}}
        />
      )
    }
    if(!!icon && !!icon.name){
      return (
        <Icon active type={icon.type} name={icon.name} style={[styles.previewIcon, icon.style]} />
      )  
    }
    return null
  }

  renderMediaPreview(mediaObject, index){
    const item = this.props.item
    const url = mediaObject.media!==null?this.selector.getUrl(mediaObject.media):null
    const preview = mediaObject.preview
    const categories = item.additional.categories
    const NEWS_TYPE = configurationHelper.getConfiguration(this.props.NEWS_TYPE, categories)
    const tagNewsType = NEWS_TYPE && NEWS_TYPE.tag!=''? NEWS_TYPE.tag:null
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE && CONTENT_TYPE.tag!=''? CONTENT_TYPE.tag:null

    if(!preview){
      return null
    }

    //const component = this.renderMediaComponentBySelector(mediaObject, index)
    const componentRender = this.createRenderFunction(mediaObject, index)
    const initialFloatingState = this.selector.getInitialFloatingState()
    const lockToLandscapeOnMaximize = this.selector.getLockToLandscapeOnMaximize()
    const block = this.selector.getBlock()
    let imageStyle = styles.imageInCarousel
    let imageHeightStyle = {}
    let customCaptionStyle = styles.customCaptionStyle
    if(typeof index === 'undefined' && !this.props.background){
      imageStyle = styles.imageOutCarousel
      imageHeightStyle={height: calculateResponsiveValue(220, 400)}
      customCaptionStyle = null
    }

    //const icon = this.selector.getIcon()
    const icon = CONTENT_TYPE? CONTENT_TYPE.icon:null

    const iconNewsType = this.getIconNewsType(icon)

    const figCaption = mediaObject.media && (!!mediaObject.media.figCaption)? mediaObject.media.figCaption : null
    const containerStyle = (!!this.props.background)? {flex: 1} : {}
    const foreground = (!!this.props.background)? (<View style={styles.foreground}></View>) : null
    const showFigCaption = (!!this.props.hideCaptionOnPreview)? !this.props.hideCaptionOnPreview : true
    const hideIconOnpreview = (!!this.props.hideIconOnpreview)

    this.previewFunction = () => {
      EventRegister.emit(FloatingViewConstants.VIDEO_EVENT, {url, componentRender, initialFloatingState, block, lockToLandscapeOnMaximize})
    }
    
    //return null
    const view = (
      <View style={containerStyle}>
        <View style={{backgroundColor: 'black'}}>
        <Image
                style={[imageStyle, imageHeightStyle]}
                source={{uri: preview}}
          />
        </View>
        
          {iconNewsType && !hideIconOnpreview &&
            <View style={[styles.containerPreviewIcon]}>
              {iconNewsType}
            </View>
          }
          
          <View style={styles.decoration}>
          {!this.props.hideTags && tagNewsType !== null && tagNewsType.name!="" &&
            <Text style={[styles.decorationText, {color: tagNewsType.color, backgroundColor:tagNewsType.bgcolor, fontWeight:'bold'}]}>{tagNewsType.name}</Text>
          }
          {!this.props.hideTags && tagContentType !== null && tagContentType.name!="" &&
            <Text style={[styles.decorationText, {color: tagContentType.color, backgroundColor:tagContentType.bgcolor, fontWeight:'bold'}]}>{tagContentType.name}</Text>
          }
          </View>
          {figCaption && showFigCaption &&
            <View style={[styles.figCaption, customCaptionStyle]}>
              <TouchableOpacity activeOpacity={0.8} onPress={() => {
                Alert.alert('Pié de foto', figCaption)
              }} >
              <Text style={styles.figCaptionText} numberOfLines={3}>{figCaption}</Text>
              </TouchableOpacity>
            </View>
          }
          {foreground}
          
      </View>
    )

    if(url === null && mediaObject.media == null){
      return view
    }

    return (
        <TouchableOpacity style={containerStyle} activeOpacity={0.8} onPress={this.previewFunction} >                  
          {view}
        </TouchableOpacity>
    )    


  }
  


  onPageChange (index){
    return
    _.each(this.videosRef, (video)=>{
      video.pause()
    })
  }
  renderCarousel(data, _renderItem){
    var {height, width} = Dimensions.get('window');
    const sliderWidth = width
    const itemWidth = width
    const indicatorAtBottom = this.props.layout.isFullScreen? false:true
    const indicatorSize = this.props.layout.isFullScreen? 0:20

    const dimensions = this.props.layout.isFullScreen? {width, height}:{}

    return(
      <CarouselView 
        ref={(carousel) => {
          this.carousel = carousel;
        }}         
        delay={2000}
        animate={false}
        loop={false}
        indicatorAtBottom={indicatorAtBottom}
        indicatorSize={indicatorSize}
        indicatorColor={config.colors.config2.color}
        inactiveIndicatorColor={config.colors.config2.background}
        indicator={Assets.indicatorClaret}
        inactiveIndicator={Assets.indicatorYellow}
        indicatorOffset = {5}
        onPageChange = {this.onPageChange}
        renderItem = {_renderItem}
        data = {data}
        height={calculateResponsiveValue(220, 400)}
        //width={width}

        //indicatorOffset="50%"
       />
    )
    
    return (
      <View style={styles.prominentCarouselContainer}>
      <View>
        <UNCarousel
            ref={(carousel) => {
              this.carousel = carousel;
            }}        
            delay={2000}
            animate={false}
            loop={false}
            indicatorAtBottom={indicatorAtBottom}
            indicatorSize={indicatorSize}
            onPageChange = {this.onPageChange}
            renderItem = {_renderItem}
            data = {data}
            isFullScreen = {false}
            height={160}
            //indicatorText="✽"
            //indicatorColor="red"
            >
          </UNCarousel>
      </View>
      </View>
      
    );
    return (
      <View style={styles.prominentCarouselContainer}>
      <View>
        <Carousel
            delay={2000}
            animate={false}
            loop={false}
            indicatorAtBottom={indicatorAtBottom}
            indicatorSize={indicatorSize}
            onPageChange = {this.onPageChange}
            //indicatorText="✽"
            //indicatorColor="red"
            >
            {data.map((video, index) => {
              return _renderItem(video, index)
            })}
          </Carousel>
      </View>
      </View>
      
    );
  }
  renderVideoComponent(mediaObject, index){
    var video = mediaObject.media
    const item = this.props.item
    const url = this.selector.getUrl(mediaObject.media)
    const indexAux = typeof index === 'undefined'? 0:index
    const description = typeof index === 'undefined'? '' : 'Video '+(indexAux+1) + '. '
    const title = description + item.title

    return (
      <View style={{flex: 1}}>
        <VideoPlayer 
          videoRef={ref => {      this.videosRef[index]=ref;    }} 
          url={url}
          title={title}
        />
      </View>
    )
  }

  renderAudioComponent(mediaObject, index){
    var audio = mediaObject.media
    const item = this.props.item
    const url = this.selector.getUrl(mediaObject.media)
    const indexAux = typeof index === 'undefined'? 0:index
    const description = typeof index === 'undefined'? '' : 'Audio '+(indexAux+1) + '. '
    const title = description + item.title

    return (
      <View>
        <AudioPlayer 
          //style={{height: 0}}
          url={url}
          title={title}
          description={description}
          logo={logo} 
          controls={false}
        />
      </View>
    )


  }
  selectPage(index){
    if(this.carousel && this.carousel.carousel){
      this.carousel.carousel.pager.scrollToPage(index)
    }
  }
  renderPhotoGalleryComponent(mediaObject, index){
    const item = this.props.item
    const url = mediaObject.media!==null?this.selector.getUrl(mediaObject.media):null
    const preview = mediaObject.preview
    const categories = item.additional.categories

    const NEWS_TYPE = configurationHelper.getConfiguration(this.props.NEWS_TYPE, categories)
    const tagNewsType = NEWS_TYPE && NEWS_TYPE.tag!=''? NEWS_TYPE.tag:null
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE && CONTENT_TYPE.tag!=''? CONTENT_TYPE.tag:null


    //const component = this.renderMediaComponentBySelector(mediaObject, index)
    const componentRender = this.createRenderFunction(mediaObject, index)
    const initialFloatingState = this.selector.getInitialFloatingState()
    const block = this.selector.getBlock()
    let imageStyle = styles.imageInCarousel
    if(typeof index === 'undefined'){
      imageStyle = styles.imageOutCarousel
    }

    const icon = this.selector.getIcon()

    const images = this.selector.getImages()
    
    //return null
    const view = (
      <PhotoGalleryPlayer 
      images={images}
      initialPage={index}
      onPageSelected={(x)=>{this.selectPage(x)}}
      />
    )
    return view
  }
  renderVideo(video, index){
    video = typeof video.item === 'undefined'? video: video.item
    const item = this.props.item
    const url = appConfig.config.app.SITE_URL + '/' + video.sources[0].src
    //return null

    if(typeof this.videoComponent[index] === 'undefined'){
      this.videoComponent[index] = (
        <View key={''+index} style={styles.prominentItemContainer}>
          <Video 
          style={{height: 0}}
          autoPlay={true}
          //onFullScreen={status => this.onVideoFullScreen(status)} 
          ref={ref => {      this.videosRef[index]=ref;    }} 
          url={url} 
          logo={logo} 
          playInBackground={true}
          controls={false}
          />
        </View>
      )
    }
  
    return this.videoComponent[index]
  }  
  renderIframeComponent(mediaObject, index){
    const item = this.props.item
    const indexAux = typeof index === 'undefined'? 0:index
    const description = typeof index === 'undefined'? '' : 'Iframe '+(indexAux+1) + '. '
    const title = description + item.title
    const html = mediaObject.media
    return <IframePlayer html={html} title={title} />
  }
  renderMediaPreviewBySelector(selector){
    const mediaObjects = selector.getMediaObjects()
    const media = mediaObjects.length === 1 || !!this.props.background?
      this.renderMediaPreview(mediaObjects[0]):
      this.renderCarousel(mediaObjects, this.renderMediaPreview)
    return media
  }
  render() {
    const item = this.props.item
    /*return (
      <ListItem style={styles.listItem}>
        <Body style={styles.listItemBody}>
        <Text>Item {item.uid}</Text>
        
        </Body>
      </ListItem>
    )*/
    
    const title = '<p>' + item.title + '</p>'
    const description = '<p>' + item.title + '</p>'
    const date = item.datetime


    const mediaSelectorHelper = new MediaSelectorHelper(item)
    //const selector = new AudioSelectorHelper(item)
    const selector = mediaSelectorHelper.getSelector()
    this.selector = selector
    const img = selector.getImageUrl(0)
    const mediaPreview = this.renderMediaPreviewBySelector(selector)
    //const medias = selector.getMedias()

    const containerStyle = (!!this.props.background)? {width: '100%', height: '100%'} : {}
    // <ImageBackground source={{uri:img}} style={{width: '100%', height: '100%'}}>

    // </ImageBackground>             

    return (
      <View style={containerStyle}>
        {mediaPreview}
      </View>
    )

  }
}
