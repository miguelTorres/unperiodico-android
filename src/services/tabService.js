
class TabService{
  constructor(){
    this.defaultPage = 0
    this.page = {}
    this.tabReady = {}
    this.disable = false
  }
  disableService(time){
    return
    this.disable = true
    setTimeout(()=>{
      this.disable = false
    }, time)
  }
  isDisabledService(){
    return this.disable
  }
  setPage(category, page){
    if(this.disable){
      return
    }
    this.page[category] = page
  }
  getPage(category){
    return typeof this.page[category]!=='undefined'? this.page[category] : this.defaultPage
  }
  setTabReady(category, tabReady){
    if(typeof this.tabReady[category] === 'undefined'){
      this.tabReady[category] = {}
    }
    this.tabReady[category][tabReady] = true
  }
  getTabReady(category, tabReady){
    return typeof this.tabReady[category]!=='undefined' && typeof this.tabReady[category][tabReady] !== 'undefined'
  }
}
let tabService = new TabService();
export default tabService;