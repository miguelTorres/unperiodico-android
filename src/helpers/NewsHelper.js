import offline from 'react-native-simple-store'

class NewsHelper {
  construct() {
  }
  calculateSaveItem(item){
    
    var newsItem = {
      uid: item.uid,
      title: item.title,
      datetime: item.datetime,
      additional: {
        categories: item.additional.categories,
        falMediaPreviews: item.additional.falMediaPreviewsCopy,
        mainCategory: item.additional.mainCategory,
        seat: item.additional.seat,
        teaser: item.additional.teaser,
        url: item.additional.url,
        imageAuthor: item.additional.imageAuthor,
      }
    }
    return newsItem
  }
  async saveNews(item){
    var key = 'savedNews'+item.uid
    return offline.save(key, item)
  }
  async loadNews(uid){
    var key = 'savedNews'+uid
    return offline.get(key)
  }
  async removeNews(uid){
    var key = 'savedNews'+uid
    return offline.delete(key)
  }
}
let newsHelper = new NewsHelper();
export default newsHelper;