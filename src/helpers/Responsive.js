import { Dimensions } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

var { height, width } = Dimensions.get('window');
var fontSizeBase = calculateBaseSize();
var factor = fontSizeBase/10;
var unityIphoneX = 11/3;
var unityIpad = 7.5;
var factorDevice = unityIpad / unityIphoneX;
var wpunity = wp('1%');
var hpunity = hp('1%');

var currentUnity = wpunity < hpunity? wpunity : hpunity;
var unity = currentUnity < unityIphoneX ? unityIphoneX : currentUnity;
export function calculateBaseSize() {
    var domain = [10, 20]
    var range = [10, 13]

    var fontSizeTemp = wp('1.60%')


    if(Math.abs(fontSizeTemp - domain[0])<Math.abs(domain[1]-fontSizeTemp)){
        return range[0]
    }
    return range[1]
}
export function responsiveFontSize(size){
    return factor * size
}
// x es el valor en un iPhone X
// y el el valor en un iPad pro de 9.7 pulgadas
export function calculateResponsiveValue(x, y, islog) {
    if(typeof y === 'undefined'){
        y=x*1.34;
    }
    //Ajuste por el cambio de tipo de letra
    //y = 0.98 * y;
    var s = (y-x) / (unityIpad-unityIphoneX);
    
    return x + s * (unity - unityIphoneX);
}
