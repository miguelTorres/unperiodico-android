

export const LAYOUT_TOGGLE_FULLSCREEN = 'LAYOUT_TOGGLE_FULLSCREEN'



export function toggleFullScreen(isFullScreen) {
  return {
    type: LAYOUT_TOGGLE_FULLSCREEN,
    isFullScreen
  }
}

