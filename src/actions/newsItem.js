import offline from 'react-native-simple-store'
import jsonApi from '../api/jsonApi';

export const LOAD_NEWS_ITEM_SUCCESS = 'LOAD_NEWS_ITEM_SUCCESS'
export const LOADING_NEWS_ITEM = 'LOADING_NEWS_ITEM'
export const LOAD_NEWS_ITEM_ERROR = 'LOAD_NEWS_ITEM_ERROR'

export const NEWS_ITEM_OFFLINE_LOAD_SUCCESS = 'NEWS_ITEM_OFFLINE_LOAD_SUCCESS'
export const NEWS_ITEM_OFFLINE_LOAD_ERROR = 'NEWS_ITEM_OFFLINE_LOAD_ERROR'

export function loadOfflineNewsItem() {
  return dispatch => {
    offline.get('newsItem').then(news => {
      dispatch(loadOfflineNewsItemSuccess(news || {}))
    }).catch(error=>{
      dispatch(loadOfflineNewsItemError('Error cargando las noticias offline.'))
    })
  }
}

export function loadOfflineNewsItemSuccess(news) {
  return {
    type: NEWS_ITEM_OFFLINE_LOAD_SUCCESS,
    news
  };
}
export function loadOfflineNewsItemError(error) {
  return {
    type: NEWS_ITEM_OFFLINE_LOAD_ERROR,
    error
  };
}


export function loadNewsItem(uid, url) {
  return function(dispatch) {
    dispatch(loadingNewsItem(uid));
    return jsonApi.getAll(url).then(news => {
      dispatch(loadNewsItemSuccess(uid, news));    
    }).catch(error => {
      dispatch(loadNewsItemError(uid, 'La noticia no fue cargada. Por favor revise su conexión.'));
      //throw(error);
    });
  };
}

export function loadNewsItemSuccess(uid, newsItem) {
  return {
    type: LOAD_NEWS_ITEM_SUCCESS,
    uid,
    newsItem
  };
}
export function loadingNewsItem(uid) {
  return {
    type: LOADING_NEWS_ITEM,
    uid,
  };
}
export function loadNewsItemError(uid, error) {
  return {
    type: LOAD_NEWS_ITEM_ERROR,
    uid,
    error
  };
}
