import offline from 'react-native-simple-store'
export const LOAD_CATEGORIES_SUCCESS = 'LOAD_CATEGORIES_SUCCESS'
export const LOAD_CATEGORIES_ERROR = 'LOAD_CATEGORIES_ERROR'

export const LOAD_OFFLINE_CATEGORIES_SUCCESS = 'LOAD_OFFLINE_CATEGORIES_SUCCESS'
export const LOAD_OFFLINE_CATEGORIES_ERROR = 'LOAD_OFFLINE_CATEGORIES_ERROR'

//loadCategoriesSuccess y loadCategoriesError se utiliza desde el componente home

export function loadCategoriesSuccess(categories) {
  return {type: LOAD_CATEGORIES_SUCCESS, categories};
}
export function loadCategoriesError(error) {
  return {type: LOAD_CATEGORIES_ERROR, error};
}

export function loadOfflineCategories() {
  return dispatch => {
    offline.get('categories').then(categories => {
      
      dispatch(loadOfflineCategoriesSuccess(categories || {}))
    }).catch(error=>{
    dispatch(loadOfflineCategoriesError('Error cargando las categorías offline'))
    })
  }
}


export function loadOfflineCategoriesSuccess(categories) {
  return {
    type: LOAD_OFFLINE_CATEGORIES_SUCCESS,
    categories
  };
}
export function loadOfflineCategoriesError(error) {
  return {
    type: LOAD_OFFLINE_CATEGORIES_ERROR,
    error
  };
}
