import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AppContainer from '../components/AppContainer'
import News from '../screens/News/News'
import * as ItemsActions from '../actions/items'
import * as NewsActions from '../actions/news'
import * as CategoriesActions from '../actions/categories'
import * as ConnectionActions from '../actions/connection'
import * as LayoutActions from '../actions/layout'

function mapStateToProps(state) {
  return {
    onlineItems: state.items.onlineList,
    offlineItems: state.items.offlineList,
    news: state.news,
    categories: state.categories,
    connectionChecked: state.connection.connectionChecked,
    connected: state.connection.connected,
    layout: state.layout
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Object.assign({}, ItemsActions, NewsActions, CategoriesActions, ConnectionActions, LayoutActions), dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)
