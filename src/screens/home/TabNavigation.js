import React, { Component } from "react";
import { Platform } from "react-native";
import {
  Tabs,
  Tab,
  TabHeading,
  Text,
  ScrollableTab
} from "native-base";
import _ from "lodash";
import PropTypes from 'prop-types';
import TabThree from "../../screens2/tab/tabThree";
import VariableHeading from "../../components/VariableHeading";
import connectComponentService from "../../models/connectComponent";
import tabService from "../../services/tabService";
import HomeConstants from "./HomeConstants";
import { EventRegister } from "react-native-event-listeners";
import View from "../../../native-base-theme/components/View";
import variableHeadingService from "../../services/VariableHeadingService";
import avoidDoubleAction from "../../services/avoidDoubleAction";


const components = [
  {
    heading: "POLÍTICA",
    component: <TabThree />
  },
  {
    heading: "ECONOMÍA",
    component: <TabThree />
  },
  {
    heading: "INTERNACIONAL",
    component: <TabThree />
  },
  {
    heading: "CIENCIA Y TECNOLOGÍA",
    component: <TabThree />
  },
  {
    heading: "Tab5",
    component: <TabThree />
  },
  {
    heading: "Tab6",
    component: <TabThree />
  },
]


class TabNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabReady: {},
      nchanges: 0,
      refreshing: false
    }

    this.renderTabBar=this.renderTabBar.bind(this)
  }
  componentDidMount() {
    this.isComponentMount = true

    this.listenerGoToTab = EventRegister.addEventListener(HomeConstants.GO_TO_TAB, (data) => {
      var {category, comparation} = data
      var page = 0
      if(this.props.category != category){
        return
      }

      if(typeof comparation==='object'){
        page = _.findIndex(this.props.navigationItems, (o)=>{
          var match = false
          _(comparation).keys().forEach((key)=>{
            if(o.dataTab[key]==comparation[key]){
              match = true
              return false
            }
          })
          return match
          
        });
      }

      tabService.setPage(category, page)
      this.isComponentMount && this.setState({
          ...this.state,
      })
    })
    this.listenerRefreshTabBar = EventRegister.addEventListener(HomeConstants.REFRESH_TAB_BAR, (data) => {
      var {category,tag, title} = data
      if(this.props.category != category){
        return
      }
      
      //B10m4ntr4*
      this.isComponentMount && this.setState({
          ...this.state,
          refreshing: true,
          //nchanges: this.state.nchanges + 1
      }, ()=>{
        setTimeout(() => {
          variableHeadingService.setTitle(tag, title)
          this.isComponentMount && this.setState({
            ...this.state,
            refreshing: false,
          })         
        }, 300);
  
      })

    })

  }

  componentWillUnmount() {
      this.isComponentMount = false
      EventRegister.removeEventListener(this.listenerGoToTab)
      EventRegister.removeEventListener(this.listenerRefreshTabBar)
  }   
  onPress = (navigation, route) => {
    if(!avoidDoubleAction.isActionActive()){
      return
    }
    navigation.navigate (route.routeName)
  }
  renderTabBar(){
    return <ScrollableTab nchanges={this.state.nchanges}  style={this.props.tabStyle} tabsContainerStyle={this.props.tabStyle} />
  }

  isTabReady(index){
    return typeof this.state.tabReady[index]!=='undefined'
  }

  onChangeTab(data){
    if(tabService.isDisabledService()){
      return
    }
    tabService.setTabReady(this.props.category, data.id)
    tabService.setPage(this.props.category, data.i)
    EventRegister.emit(HomeConstants.ON_CHANGE_TAB, {category: this.props.category, index: data.i})

    return
    setTimeout(()=>{
      var tab = {}
      tab[data.i] = true

      this.isComponentMount && this.setState({
        ...this.state,
        tabReady: {
          ...this.state.tabReady,
          ...tab
        }
      })  
    }, 2000)
  }
  renderComponent(navigationItem, index){
    return React.cloneElement(navigationItem.component, { tabBar: {category: this.props.category, index} })
    return 
    if(!this.isTabReady(index)){
      return (
        null
      )
    }
    return navigationItem.component
  }
  renderContentTabs(){
    return this.props.navigationItems.map((navigationItem, index) => {
      if(typeof navigationItem.label === 'object'){
        const ConnectedVariableHeading = connectComponentService.createVariableHeaderComponent(VariableHeading, navigationItem.label.footerTab.category)
        return(
          <Tab
            key = {index}
            heading={<TabHeading style={this.props.tabStyle} activeTabStyle={this.props.activeTabStyle} textStyle={this.props.textStyle} activeTextStyle={this.props.activeTextStyle}><ConnectedVariableHeading textStyle={this.props.textStyle} category={this.props.category} navigationItem={navigationItem} /></TabHeading>}
          >
            {this.renderComponent(navigationItem, index)}
          </Tab>
        )
      }else{
        return(
          <Tab
          key = {index}
          heading={<TabHeading  style={this.props.tabStyle} activeTabStyle={this.props.activeTabStyle} textStyle={this.props.textStyle} activeTextStyle={this.props.activeTextStyle}><VariableHeading textStyle={this.props.textStyle} category={this.props.category} navigationItem={navigationItem} /></TabHeading>}
        >
            {this.renderComponent(navigationItem, index)}
        </Tab>

        )
      }
      
      
    })
  }
  render () {

    const page = tabService.getPage(this.props.category)

    const { navigation, routes } = this.props

    const renderTabBar = this.state.refreshing? false:this.renderTabBar



    if(this.props.isScrollTabBar){
      return (
        //prerenderingSiblingsNumber={Infinity} renderiza todos los tabs
        <Tabs onChangeTab={(data)=>{this.onChangeTab(data)}} {...this.props} page={page} tabBarPosition={this.props.tabBarPosition} renderTabBar={renderTabBar}
          locked={true}
        >
                {this.renderContentTabs()}
        </Tabs>
      )
  
    }else{
      return (
        //prerenderingSiblingsNumber={Infinity} renderiza todos los tabs
        <Tabs onChangeTab={(data)=>{this.onChangeTab(data)}} {...this.props} page={page} tabBarPosition={this.props.tabBarPosition} 
          locked={true}
        >
                {this.renderContentTabs()}
        </Tabs>
      )
  
    }

    
  }
}
/*
// Prop type warnings
TabNavigation.propTypes = {
  navigation: PropTypes.object.isRequired,
  routes: PropTypes.object.isRequired
}
*/
export default TabNavigation