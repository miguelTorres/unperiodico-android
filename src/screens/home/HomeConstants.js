export default {
    GO_TO_TAB: 'HOME_GO_TO_TAB',
    REFRESH_TAB_BAR: 'HOME_REFRESH_TAB_BAR',
    ON_CHANGE_TAB: 'HOME_ON_CHANGE_TAB'
};