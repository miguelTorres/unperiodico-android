import React, { Component } from "react";
import { NetInfo, Platform, Image, RefreshControl, StatusBar, WebView, View } from "react-native";
import {
  Container,
  StyleProvider,
  FooterTab,
  Segment,
  Content,
  Header,
  Footer,
  Title,
  Button,
  Icon,
  Tabs,
  Tab,
  Right,
  Left,
  Body,
  Text,
  ScrollableTab,
  Toast,
  Spinner
} from "native-base";
import _ from 'underscore'
import PropTypes from 'prop-types';

import FCM from '../../models/fcm'

import getTheme from '../../../native-base-theme/components'
import material from '../../../native-base-theme/variables/unperiodico-material'
import platform from '../../../native-base-theme/variables/platform'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import connectComponentService from '../../models/connectComponent'
import News from "../News/News";
import PopupMenu from '../../components/PopupMenu'
import CustomError from '../../components/error/CustomError'

import config from '../../../config'


import TabOne from "../../screens2/tab/tabOne";
import TabTwo from "../../screens2/tab/tabTwo";
import TabThree from "../../screens2/tab/tabThree";
import TabFour from "../../screens2/tab/tabFour";
import TabFive from "../../screens2/tab/tabFive";
import TabNavigation from "./TabNavigation";

import Assets from '../../../assets'
import CategoriesModel from '../../models/categories'

import appConfig from '../../models/appConfig'
import styles from "./styles";
import configurationHelper from "../../helpers/ConfigurationHelper";
import appService from "../../services/appService";
import ButtonIcon from "../../components/ButtonIcon";
import avoidDoubleAction from "../../services/avoidDoubleAction";

import firebaseDatabase from '../../models/firebaseDatabase'
import firebaseConstants from '../../models/firebaseConstants'
import { EventRegister } from "react-native-event-listeners";
import { responsiveFontSize, calculateResponsiveValue } from "../../helpers/Responsive";
import CustomStatusBar from "../../components/CustomStatusBar";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";




// Components




const footerTabs = [
  {
    title: 'DESTACADO',
    active: true
  },
  {
    title: 'NOTICIAS',
    active: false
  },
  {
    title: 'RECOMENDADOS',
    active: false
  }
]
const components = [
  {
    heading: "POLÍTICA",
    component: <TabThree />
  },
  {
    heading: "ECONOMÍA",
    component: <TabThree />
  },
  {
    heading: "INTERNACIONAL",
    component: <TabThree />
  },
  {
    heading: "CIENCIA Y TECNOLOGÍA",
    component: <TabThree />
  },
  {
    heading: "Tab5",
    component: <TabThree />
  },
  {
    heading: "Tab6",
    component: <TabThree />
  },
]
const TabNavigationConnected = connectComponentService.createNewsComponent(TabNavigation)
class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      indexCurrentTab: 0,
      loading: false,
      navigationItems: null,
      navigationFooterTabs: null,
    }

    this.componentFooterTabs = {}
    this.popupActionsNames = ['Reiniciar aplicación']
    this.popupActions = ['reload']

    this.defaultNavigationItems = Object.keys(appConfig.config.app.initialPages).map((key, index)=>{
      var val = appConfig.config.app.initialPages[key]
      const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
      return {
        key,
        label: val.title,
        dataTab: val,
        component: <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />  
      }
    })

    this.footerTabs = Object.keys(appConfig.config.app.footerTabs).map((key, index)=>{
      var val = appConfig.config.app.footerTabs[key]
      return {
        ...val,
      }
    })
    this.navigationItems=null

  }

  async componentDidMount() {
    this.isComponentMount = true
    this.loadCategories(this.props.categories.categories)
    FCM.manageNotification(this.props.navigation)
    EventRegister.emit(firebaseConstants.APP_LOADED, null)
    if(this.props.configuration.configuration.local.subscribed === null){
      this.manageSubscription()
    }
    firebaseDatabase.init()
    appService.setFirstInit(false)  


  }

  componentWillUnmount() {
    this.isComponentMount = false
  }    

  loadCategories(categories){
    const navigationItems = this.createNavigationItems(categories)
    this.navigationItems = navigationItems
    const navigationFooterTabs = this.createNavigationFooterTabs()
    this.isComponentMount && this.setState(previousState => {
      return { navigationItems: navigationItems, navigationFooterTabs, indexCurrentTab: 0 };
    });
  }

  



  onPress = (navigation, route) => {
    if(!avoidDoubleAction.isActionActive()){
      return
    }
    navigation.navigate (route.routeName)
  }

  createNavigationItems(categories){
    let items, readonlyMessage, navigationItems

    if (typeof categories !== 'undefined') {
      items = categories
    } else {
      items = []
    }


    navigationItems = items.map((category, index) => {
      const NewsConnected = connectComponentService.createNewsComponent(News, category.item.title)

      return {
        label: category.item.title,
        sorting: category.item.sorting,
        dataTab: category.item,
        //label: 'algo',
        component: <NewsConnected {...this.props} category = {category} />
      }
    })
    
    navigationItems = [].concat(this.defaultNavigationItems).concat(navigationItems);

    return navigationItems


  }

  createNavigationFooterTabs(){
    let items, readonlyMessage, navigationFooterTabs


    navigationFooterTabs = this.footerTabs.map((footerTab, index) => {
      const component = this.createComponent(footerTab)
      if(typeof footerTab.title==='object'){
        return {
          label: {
            footerTab: footerTab
          },
          sorting: index,
          dataTab: footerTab,
          //label: 'algo',
          component: component
        }
      }

      return {
        label: footerTab.title,
        sorting: index,
        dataTab: footerTab,
        //label: 'algo',
        component: component
      }
    })
    

    return navigationFooterTabs

  }  

  renderLoading(){
    return (
      <StyleProvider style={getTheme(unperiodicoVariables)}>
      <Container>
      <CustomHeader>
      <CustomStatusBar />
      <Left>
      <Image
              style={{height: calculateResponsiveValue(30, 45), width: calculateResponsiveValue(106, 159), }}
              source={Assets.logo}>
              </Image>
        </Left>
        <Body>
        </Body>
        <Right>
          </Right>
      </CustomHeader>
      <Content  refreshControl={
        <CustomRefreshControl
          refreshing={true}
        />
      }>

       
        </Content>
  
    </Container>  
    </StyleProvider>
    ); 
  }

  createComponent_EveryNews (val) {
    const component = (
      <TabNavigation 
      category="headerTab" 
      navigation={this.props.navigation} 
      routes={this.props.routes} 
      navigationItems={this.navigationItems} 
      tabStyle={styles.tabStyleHeaderTab} 
      activeTabStyle={styles.activeTabStyleHeaderTab} 
      textStyle={styles.textStyleHeaderTab} 
      activeTextStyle={styles.activeTextStyleHeaderTab}
      isScrollTabBar={true}
      tabBarUnderlineStyle={{backgroundColor: '#F2D069'}}
      />
    )
    return component
  }
  createComponent_DefaultView (val) {
    const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
    const component = <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />
    return component
  }
  createComponent_DetailNews (val) {
    const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
    const component = <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />
    return component
  }
  createComponent_Prominent (val) {
    const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
    const component = <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />
    return component
  }
  createComponent_Authors (val) {
    const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
    const component = <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />
    return component
  }
  createComponent_Sondeos (val) {
    const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
    const component = <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />
    return component
  }
  createComponent_CarouselView (val) {
    const NewsConnected = connectComponentService.createNewsComponent(News, val.category)
    const component = <NewsConnected {...this.props} category = {val.category} url={val.url} jsonkey={val.jsonkey} view={val.view} filtroFecha={val.filtroFecha} itemView={val.itemView} showMainCategory={val.showMainCategory} />
    return component
  }
  

  createComponent(currentTab){

    
    const createComponentFunc = 'createComponent_'+currentTab.view;
    if(typeof this[createComponentFunc] === 'undefined' ){
      return <Text>Componente no registrado en home</Text>
    }
    return this[createComponentFunc](currentTab)
  }
  handlePopup(e,i){
    if(typeof this.popupActions[i] === 'undefined'){
      return
    }
    var action = this.popupActions[i]+'Action';
    if(typeof this[action] !== 'undefined'){
      this[action]()
    }else{
      Toast.show({
        text: 'Acción '+action+' no implementada',
        buttonText: 'Aceptar',
        duration: 5000
      })
    }
  }
  reloadAction(){
    this.props.resetApplication()
    this.props.navigation.navigate ('Loading', {error: 'La aplicación fue reiniciada.'})
  }
  subscriptionIcon(){
    if(!!this.props.configuration.configuration.local.subscribed){
      return Assets.campana
    }else{
      return Assets.campanaSilencio
    }
  }
  manageSubscription(){
    FCM.manageSubscription(!!this.props.configuration.configuration.local.subscribed).then(()=>{
      var subscribed = !this.props.configuration.configuration.local.subscribed
      this.props.updateLocalConfiguration({
        subscribed: !this.props.configuration.configuration.local.subscribed
      })
      Toast.show({
        text: subscribed? 'Notificaciones activadas':'Notificaciones desactivadas',
        buttonText: 'Aceptar',
        duration: 5000
      })
    }).catch((error)=>{
      if(this.props.configuration.configuration.local.subscribed === null){
        this.props.updateLocalConfiguration({
          subscribed: false
        })  
      }
      Toast.show({
        text: 'La suscripción no fue actualizada. Revise su conexión a internet',
        buttonText: 'Aceptar',
        duration: 5000
      })
    })

  }

  render () {
    let items, readonlyMessage, navigationItems




   const { navigation, routes } = this.props
    if(!this.state.navigationItems || !this.state.navigationFooterTabs){
      return this.renderLoading()
    }

    return (
      <StyleProvider style={getTheme(unperiodicoVariables)}>
      <Container>
        {!this.props.layout.isFullScreen && 
          <CustomHeader>
          <Left>
            <Button
                bordered style={styles.headerButton}
                onPress={() => {
                  if(!avoidDoubleAction.isActionActive()){
                    return
                  }

                  this.props.navigation.openDrawer()
                }}
              >
              <Icon style={styles.headerIcon} name="ios-menu" />
              <Image
                style={{height: calculateResponsiveValue(30, 45), width: calculateResponsiveValue(106, 159), }}
                resizeMode={'contain'}
                source={Assets.logo}>
                </Image>

            </Button>
          </Left>
          <Body>
          </Body>
          <Right>
            <ButtonIcon headerButton={true} onPress={() => this.manageSubscription()} imageSource={this.subscriptionIcon()} />
            <ButtonIcon headerButton={true} onPress={(e,i) => {
              if(!avoidDoubleAction.isActionActive()){
                return
              }
              
              this.props.navigation.navigate ('Searchbar')
              }} icon={{name: 'search'}} />
            <PopupMenu
              icon={{name: 'more'}}
              actions={this.popupActionsNames}
              onPress={(e,i) => this.handlePopup(e, i)} 
            />

          </Right>
        </CustomHeader>
      }
      {this.state.loading &&
        <View
        >
          <Spinner />
        </View>
      }

      <TabNavigation 
      category="footerTab" 
      tabBarPosition="bottom" 
      navigation={this.props.navigation} 
      routes={this.props.routes} 
      navigationItems={this.state.navigationFooterTabs} 
      tabStyle={styles.tabStyleFooterTab} 
      activeTabStyle={styles.activeTabStyleFooterTab} 
      textStyle={styles.textStyleFooterTab} 
      activeTextStyle={styles.activeTextStyleFooterTab}
      isScrollTabBar={false}
      tabBarUnderlineStyle={{backgroundColor: '#801836'}}
      />
    </Container>  
    </StyleProvider>

    )
  }
}


export default connectComponentService.createGeneralComponent(Home)
