import React, { Component } from "react";
import { ScrollView, StyleSheet, ListView, ImageBackground, View, StatusBar, Image, UIManager, Alert, RefreshControl, Modal, TouchableHighlight, TouchableOpacity, Clipboard, ToastAndroid, AlertIOS, Platform, Dimensions } from "react-native";
import { Container, Content, Header, Body, Left, Right, Icon, H3, Text, List, ListItem, Thumbnail, Button, Card, CardItem, Title, Grid, Col,   Footer,
  FooterTab,
  StyleProvider,
  Fab,
  IconNB,
  Toast
 } from "native-base";
 
 import HTML from '../../components/HTML';
 import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils';
 import Share from 'react-native-share';

import Bar from '../bar/bar'
import CustomError from '../../components/error/CustomError'

import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import NewsItemModel from '../../models/newsItem'
import Message from "../../components/Message";
import Multimedia from "../../components/multimedia/Multimedia";

import RelatedNews from '../../components/relatednews/RelatedNews'
import AjaxModel from '../../models/ajax'
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";


class Detail extends Component<{}, State> {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      html: null,
    }

    this.renderers = {
      img: (attrs, children, convertedCSSStyles, passProps) => this.renderImg(attrs, children, convertedCSSStyles, passProps),
    }
    this.tagsStyles = {
      h3: { color: unperiodicoVariables.titleFontColor },
      p: { fontSize: 14 }
    }
    this.classesStyles = {
      'header': { color: unperiodicoVariables.titleFontColor },
    }
    this.isComponentMounted = false
    
  }
  renderImg(attrs, children, convertedCSSStyles, passProps){
    const url = appConfig.config.app.SITE_URL + attrs.src
    return(
      <View key={passProps.key}>
      <Image
        style={styles.htmlImage}
        source={{uri: url}}
      />
      </View>
    )
  }

  componentDidMount() {
    this.isComponentMount = true
    this.load()
  }
  componentWillMount() {
    this.isComponentMount = false
  }

  load = async () => {
    try {
      const { navigation } = this.props;
      const url = navigation.getParam('url', null);
        this.isComponentMount && this.setState({
          refreshing: true,
          error: null,
        });  
      const html = await AjaxModel.load(url).then((html)=>{
        this.isComponentMount && this.setState({
          refreshing: false,
          html: html,
        })
      }).catch((error)=>{
        this.isComponentMount && this.setState({
          refreshing: false,
          error: 'La información no fue cargada. Revise la conexión. Si persiste contacte con el administrador.',
        });
  
      });

    } catch (error) {
      throw error
    }
  };


  
  renderError(){
    return(
      <CustomError 
        error={this.state.error} 
        buttonText="Recargar" 
        onPress={()=>{this.load()}}
        icon = {{name: 'ios-refresh'}} 
        />
    )
  }
  
  renderBody(){
    if(!this.state.html){
      return null
    }

    return (
      <ScrollView style={{ flex: 1, padding: 20 }}>
                <HTML html={this.state.html} stylesConfiguration='ajaxConfiguration' />
            </ScrollView>    
    )
  }
  render() {
    const { navigation } = this.props;

    const title = navigation.getParam('title', null);
    //const loading = typeof this.props.newsItem.loading !== 'undefined'? this.props.newsItem.loading:false
    //const isError = this.props.newsItem.error !== null && typeof newsItem === 'undefined'
    //const showOffline = !this.state.isOnline && typeof newsItem !== 'undefined' && !loading
    const loading = this.state.refreshing
    const isError = !!this.state.error
    const showOffline = false
    const body = this.renderBody()
    

    return (
        <StyleProvider style={getTheme(unperiodicoVariables)}>
        <Container>



          <CustomHeader>
            <Left>
              <Bar navigation={this.props.navigation} />
            </Left>
            <Body>
            </Body>
            <Right>
          </Right>
          </CustomHeader>
          <Content 
            padder
            refreshControl={
              <CustomRefreshControl
                refreshing={loading}
                onRefresh={()=>{
                  this.load()
                }}
              />
            }
        >
            {showOffline && 
              <Message message='Sin conexión' />
            }
  
            {isError && this.renderError()}
            {!loading && !isError && body}
  
         
  
            
          </Content>
        </Container>
        </StyleProvider>
      );
  }

  
}

export default Detail
