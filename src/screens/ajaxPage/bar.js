import React, { Component } from "react";
import { Image } from "react-native";
import { Icon, Button } from "native-base";

import styles from "./styles";
import Assets from '../../../assets'
import avoidDoubleAction from "../../services/avoidDoubleAction";

class Bar extends Component<{}, State> {

  render() {
    const { state } = this;

    return (
      <Button
      bordered style={styles.headerButton}
      onPress={() => {
        if(!avoidDoubleAction.isActionActive()){
          return
        }
        this.props.navigation.openDrawer()
      }}
    >
      <Icon style={styles.headerIcon} name="ios-menu" />
      <Image
        style={{height: calculateResponsiveValue(30), width: calculateResponsiveValue(106)}}
        source={Assets.logo}>
        </Image>

    </Button>
    );
  }
}

export default Bar;
