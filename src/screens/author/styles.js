const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import config from '../../../config'
import { calculateResponsiveValue } from '../../helpers/Responsive';

export default {
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: "#801836",
    
  },
  title: {
    color: '#fff',
    fontSize: 18,
  },
  imageContainer: {
    flex: 1,
    width: null,
    height: null,
    marginRight: 20,
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 40 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 280,
    height: 100
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  },
  mb: {
    marginBottom: 15
  },

  modalContainer:{
    backgroundColor:'#ebeef0',
    flex:1,
    alignItems:'center'     //<-----
  },  
  modalButtonsContainer:{
    flex: 1,
    justifyContent: 'center',
    flexDirection: "row"
  },  
  buttonsContainer:{
    flex: 1,
    //justifyContent: 'center',
    flexDirection: "row",
    alignItems: "flex-start"
  }, 
  button: {
    marginLeft: 5,
    marginRight: 5
  } ,
  icon: {
    marginLeft: 0, 
    marginRight: 0
  },
  listItem: {
    borderBottomColor: 'rgba(126, 77, 119, 1)',

    borderBottomWidth: 0.5,
    paddingLeft: 2,
    marginLeft: 5,
  },
  listItemBody: {
    borderColor: 'rgba(256, 256, 256, 0)'
  },
  listItemRight: {
    borderColor: 'rgba(256, 256, 256, 0)'
  },

  modalContent:{
    backgroundColor:'#ef553a',
    /*width:300,*/
    paddingTop:10,
    paddingBottom:20,
    paddingLeft:20,
    paddingRight:20, 
    borderRadius:10
  }, 
  offline: {
    backgroundColor: '#000000',
    color: '#FFFFFF',
    textAlign: 'center',
    paddingTop: 0,
    /*height: 20,*/
  },
  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  headerIcon: {
    marginLeft: 1,
    marginRight: calculateResponsiveValue(10, 30),
    fontSize: calculateResponsiveValue(25, 30),
    
  },
  htmlImage: {
    resizeMode: "cover",
    width: '100%',
    height: calculateResponsiveValue(200),
    flex: 1
  },
  authorContainer:{
    marginBottom: calculateResponsiveValue(20),
    
    padding: 0,
  },
  authorImageContainer:{
    position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, justifyContent: 'center', alignItems: 'center',
  },
  authorHeader:{
    position: 'absolute', top: 0, left: calculateResponsiveValue(80), bottom: 0, justifyContent: 'center', alignItems: 'flex-start',
    paddingLeft: calculateResponsiveValue(10)
  },
  authorImage:{
    width: calculateResponsiveValue(100), height: calculateResponsiveValue(100), borderRadius: calculateResponsiveValue(100)/2,
    borderColor: 'white',
    borderWidth: 3,
    marginRight: 0,
    marginLeft: 0,
    
  },
  authorHeader1: {
    height: calculateResponsiveValue(60),
    backgroundColor: '#f2f2f2'
  },
  authorHeader2: {
    height: calculateResponsiveValue(60),
    backgroundColor: unperiodicoVariables.titleFontColor,
  },
  authorContentAnim: {
    backgroundColor: unperiodicoVariables.titleFontColor,
  },
  authorContent: {
    padding: calculateResponsiveValue(20),
    paddingTop:0,
  },
  authorName:{
    color: '#ffd261',
    fontWeight: 'bold',
    fontSize: calculateResponsiveValue(20),
  },
  headerAuthorName:{
    color: config.colors.config2.color,
    fontSize: calculateResponsiveValue(15),
    fontWeight: 'bold',
    lineHeight: calculateResponsiveValue(18),
    marginLeft: calculateResponsiveValue(20),
    marginBottom: calculateResponsiveValue(5),
    paddingRight: calculateResponsiveValue(40),
  },
  headerAuthorEmail:{
    color: '#767676',
    fontSize: calculateResponsiveValue(13),
    fontStyle: 'italic',
    lineHeight: calculateResponsiveValue(14),
    marginLeft: calculateResponsiveValue(20),
  },
  flechaRojaAbajoContainer:{
    position: 'absolute',
    right: calculateResponsiveValue(30),
    top: 0,
    bottom: 0,
    justifyContent: 'center', 
    alignItems: 'center',
  },
  flechaRojaAbajo:{
    width: calculateResponsiveValue(20),
    height: calculateResponsiveValue(20),
    resizeMode: 'contain'
  },


  flechaBlancaArribaContainer:{
    justifyContent: 'flex-end', alignItems: 'center',
    marginBottom: calculateResponsiveValue(10),
  },
  flechaBlancaArriba:{
    width: calculateResponsiveValue(20),
    height: calculateResponsiveValue(20),
    resizeMode: 'contain'
  },
  
  authorText: {
    color: 'white'
  },
  authorEmail: {
    fontStyle: 'italic',
  },
  authorLine: {
    height: 3,
    backgroundColor: unperiodicoVariables.topTabBarActiveTextColor,
    width: calculateResponsiveValue(50),
    marginBottom: calculateResponsiveValue(10),
    marginTop: calculateResponsiveValue(10)
  },
  
  wrapperAuthor: {
    padding: calculateResponsiveValue(20, 50),
    paddingTop: 0,
  },


};

