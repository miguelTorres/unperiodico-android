import React, { Component } from "react";
import { StyleSheet, ListView, ImageBackground, View, StatusBar, Image, UIManager, Alert, RefreshControl, Modal, TouchableHighlight, TouchableOpacity, Clipboard, ToastAndroid, AlertIOS, Platform, Dimensions } from "react-native";
import { Container, Content, Header, Body, Left, Right, Icon, H3, Text, List, ListItem, Thumbnail, Button, Card, CardItem, Title, Grid, Col,   Footer,
  FooterTab,
  StyleProvider,
  Fab,
  IconNB,
  Toast
 } from "native-base";
 import HTML from '../../components/HTML';
 import Share from 'react-native-share';
 import configurationHelper from '../../helpers/ConfigurationHelper'
 

 import moment from 'moment'
require('moment/locale/es');


import Bar from './bar'
import CustomError from '../../components/error/CustomError'

import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import NewsItemModel from '../../models/newsItem'
import Message from "../../components/Message";
import Multimedia from "../../components/multimedia/Multimedia";

import RelatedNews from '../../components/relatednews/RelatedNews'
import JsonApi, { wait } from "../../api/jsonApi";
import firebaseAnalytics from "../../models/firebaseAnalytics";
import avoidDoubleAction from "../../services/avoidDoubleAction";
import newsHelper from "../../helpers/NewsHelper";
import firebaseConstants from "../../models/firebaseConstants";
import { EventRegister } from "react-native-event-listeners";
import { timeout } from "../../helpers/Functions";
import FCM from "../../models/fcm";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";



class Detail extends Component<{}, State> {
  constructor(props) {
    super(props);
    
    this.onPressItem = this.onPressItem.bind(this)
    this.onPressRelatedNews = this.onPressRelatedNews.bind(this)

    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    this.state = {
      active: false,
      isOnline: !!this.props.newsItem.online[uid],
      showSave: typeof this.props.newsItem.news[uid] !== 'undefined',
      loading: true,
    }
  }

  getUid(){
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    return uid
  }

  loadNews(){
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    return newsHelper.loadNews(uid)
  }

  async componentDidMount() {

    //avoidDoubleAction.disableBusy()
    const uid = this.getUid()
    FCM.setCurrentUid(uid)

    this.isComponentMount = true
    this.listenerUpdateApp = EventRegister.addEventListener(firebaseConstants.UPDATE_APP, () => {
      this.makeRemoteRequest()
    })
    if(!!this.props.saveNews){
      this.setState({loading: true})
      var newsItem = await this.loadNews()
      this.setState({loading: false, newsItem})
    }else{

      await timeout(300)
      this.setState({loading: false})
      if(!!this.props.newsItem.online[uid]){
        this.loadRelatedNews()
        return;
      }
        this.makeRemoteRequest()
    }
    
  }
  componentWillUnmount() 
  {

    FCM.removeCurrentUid()
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerUpdateApp)
  }  

  isAbsolute(url){
    return url.indexOf('http') === 0 || url.indexOf('https') === 0
  }
  makeRemoteRequest = () => {
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    var url = this.isAbsolute(item.additional.url)? item.additional.url : appConfig.config.app.SITE_URL + item.additional.url
    //url = 'https://fake384925803459.com/fak'
    //this.props.loadNewsItem(item.uid, url)
    NewsItemModel.load(this.props, item.uid, url).then(()=>{
      const newsItem = this.props.newsItem.news[item.uid];

      this.isComponentMount && this.setState({
        ...this.state,
        isOnline: true,
        showSave: true,
      }, ()=>{
        this.loadRelatedNews()
      })
      try {
        firebaseAnalytics.logEvent('view_item', {
          'uid': newsItem.uid,
          'url': newsItem.additional.url,
          'title': newsItem.title,
        })
          
      } catch (error) {
      }

    }).catch((error)=>{
    })
    
  }
  async loadRelatedNews(){
    try {
      const { navigation } = this.props;
      const item = navigation.getParam('item', null);
      const uid = item.uid
      const newsItem = this.props.newsItem.news[uid];
      if(!!newsItem.additional.relatedNews && newsItem.additional.relatedNews.length >0){
        let relatedNews = await JsonApi.getAll(newsItem.additional.relatedNews)
        this.isComponentMount && this.setState({
          relatedNews: relatedNews.news
        })
      }
        
    } catch (error) {
    }

  }
  async toogleSavedNewsItem(){
    if(!avoidDoubleAction.isActionActive()){
      return
    }
    await wait(300)

    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const uid = item.uid
    const saveNews = (!!this.props.saveNews)
    const newsItem = saveNews? this.state.newsItem : this.props.newsItem.news[uid];
    const isSavedNewsItem = typeof this.props.savedNews.news[uid] !== 'undefined'
    if(isSavedNewsItem){
      this.props.savedNews_remove(uid)
      await newsHelper.removeNews(uid)
    }else{
      var savedItem = newsHelper.calculateSaveItem(newsItem)
      this.props.savedNews_save(uid, savedItem)
      await newsHelper.saveNews(newsItem)
    }
  }

  renderItem(newsItem){
    if(typeof newsItem === 'undefined'){
      return null
    }
    const { navigation } = this.props;
    const special = navigation.getParam('special', null);

    const item = newsItem
    const categories = item.additional.categories
    const NEWS_TYPE = configurationHelper.getConfiguration(this.props.NEWS_TYPE, categories)

    // Los tags se muestran en el componente Multimedia
    const tagNewsType = NEWS_TYPE && NEWS_TYPE.tag!=''? NEWS_TYPE.tag:null
    const CONTENT_TYPE = configurationHelper.getConfiguration(this.props.CONTENT_TYPE, categories)
    const tagContentType = CONTENT_TYPE && CONTENT_TYPE.tag!=''? CONTENT_TYPE.tag:null


    const title = '<h1>'+item.title+'</h1>'
    const teaser = '<div class="teaser-wraper"><p>'+item.additional.teaser+'</p></div>'
    const date = moment(item.datetime).format(appConfig.config.app.SONDEOS.formatDate).toUpperCase()
    const seat = (!!item.additional.seat)? ' | '+item.additional.seat.toUpperCase() : ''
    const dateSeat = '<div class="wraper-date"><p class="date">'+date + seat+'</p></div>'
    const mainCategory = (!!item.additional.mainCategory)? '<div class="wraper-category"><p class="category">'+item.additional.mainCategory.toUpperCase()+'</p></div>' : null

    const author = item.additional.author? {
      
      name: item.additional.author.name,
      title: item.additional.author.title,
      firstName: item.additional.author.firstName,
      lastName: item.additional.author.lastName,
    }:null

    const authorHtml = author? '<div class="main-author"><p class="author">'+author.name+'</p></div>':
    ''

    return (
      <View style={{backgroundColor: 'transparent'}}>
        <View>
          <Multimedia {...this.props} item={newsItem} NEWS_TYPE={appConfig.config.app.detail.NEWS_TYPE} CONTENT_TYPE={appConfig.config.app.detail.CONTENT_TYPE}  />
          {special && 
            <View style={styles.goToSpecial}>
              <TouchableOpacity onPress={()=>{
                      if(!avoidDoubleAction.isActionActive('detail')){
                        return
                      }
                  this.props.navigation.navigate ('Detail', {
                    item: special
                  });
              }}>
              <Text style={styles.goToSpecialText}>IR AL ESPECIAL</Text>
              </TouchableOpacity>
            </View>
          }
        </View>
        {this.renderRelated(newsItem)}
        <View style={styles.detailImnner}>

        {mainCategory && 
          <HTML stylesConfiguration='detailConfiguration' html={mainCategory} />
        }
          
          <HTML stylesConfiguration='detailConfiguration' html={title} />
        <HTML stylesConfiguration='detailConfiguration' html={dateSeat} />
        {author && 
            <HTML stylesConfiguration='detailConfiguration' html={authorHtml} />
          }
      
        <HTML stylesConfiguration='detailTeaserConfiguration'
                          html={teaser}/>
          <View style={styles.decoration} />                 
                        
        <HTML 
          stylesConfiguration='detailConfiguration'
          html={newsItem.additional.contentElements} 
        />
        </View>
      </View>
    )
  }
  onPressRelatedNews(item){
    if(!avoidDoubleAction.isActionActive('detail')){
      return
    }
    this.props.navigation.push ('Detail', {
      item,
    });

  }
  onPressItem(item){
    if(!avoidDoubleAction.isActionActive('detail')){
      return
    }


    const { navigation } = this.props;
    const itemNavigation = navigation.getParam('item', null);
    const uid = itemNavigation.uid
    const newsItem = this.props.newsItem.news[uid];

    const relatedNavigation = navigation.getParam('related', null)
    const related = (!relatedNavigation && newsItem.additional.related && newsItem.additional.related.length > 0)? 
    newsItem.additional.related : 
    relatedNavigation;

    const specialNavigation = navigation.getParam('special', null);

    const special = specialNavigation? specialNavigation : newsItem


    this.props.navigation.push ('Detail', {
      item,
      related,
      special
    });
  }
  onShare() {
    if(!avoidDoubleAction.isActionActive()){
      return
    }
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const newsItem = this.props.newsItem.news[item.uid];
    const url = newsItem.additional.originalUrl
    let shareOptions = {
      title: "UN Periódico: "+newsItem.title,
      message: url,
      //url: this.props.item.url,
      subject: "UN Periódico: "+newsItem.title, //  for email
    };

    setTimeout(() => {
      Share.open(shareOptions).catch((err) => {});
    },300);
  }
  renderError(){
    return(
      <CustomError 
        error={this.props.newsItem.error} 
        buttonText="Recargar" 
        onPress={()=>{this.makeRemoteRequest()}}
        icon = {{name: 'ios-refresh'}} 
        />
    )
  }
  renderRelated(newsItem){
    const { navigation } = this.props;


    const relatedNavigation = navigation.getParam('related', null)
    const related = (!relatedNavigation && newsItem.additional.related && newsItem.additional.related.length > 0)? 
    newsItem.additional.related : 
    relatedNavigation;

    if(related && related.length > 0){
      return (
        <RelatedNews title='Noticias en este especial' related={related} isDetail={true} navigation={this.props.navigation} onPressItem={this.onPressItem} />
      )  
    }
    return null
  }
  renderRelatedNews(){
    if(!!this.state.relatedNews && this.state.relatedNews.length >0){
      return (
        <RelatedNews title='Noticias relacionadas' related={this.state.relatedNews} navigation={this.props.navigation} onPressItem={this.onPressRelatedNews} />
      )  
    }
    return null
  }
  isLoading(){
    const saveNews = (!!this.props.saveNews)
    if(saveNews){
      return (!!this.state.loading)
    }
    const loading = typeof this.props.newsItem.loading !== 'undefined'? this.props.newsItem.loading:this.state.loading
    return loading
  }

  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item', null);
    const title = navigation.getParam('title', null);
    const uid = item.uid
    const saveNews = (!!this.props.saveNews)
    const newsItem = saveNews? this.state.newsItem : this.props.newsItem.news[uid];
    const loading = this.isLoading()
    const isSavedNewsItem = typeof this.props.savedNews.news[uid] !== 'undefined'
    const isError = this.props.newsItem.error !== null && typeof newsItem === 'undefined' && !this.state.loading

    const showOffline = saveNews? false : (!this.state.isOnline && typeof newsItem !== 'undefined' && !loading)
    const showSaveButton = !loading && !isError && (!!newsItem)
    

    return (
        <StyleProvider style={getTheme(unperiodicoVariables)}>
        <Container>



          <CustomHeader>
            <Left>
              <Bar navigation={this.props.navigation} />
            </Left>
            <Body>
            </Body>
            <Right>
              {showSaveButton && 
              <Button bordered style={styles.headerButton} onPress={()=>{this.toogleSavedNewsItem()}}>
              {!isSavedNewsItem && this.state.showSave &&
              <Icon type="FontAwesome" name="bookmark-o" style={{fontSize: calculateResponsiveValue(25, 30),}} />
              }
              {isSavedNewsItem && 
              <Icon type="FontAwesome" name="bookmark" style={{fontSize: calculateResponsiveValue(25, 30),}} />
              }
            
            </Button>
            }
          </Right>
          </CustomHeader>
          <Content 
            refreshControl={
              <CustomRefreshControl
                refreshing={loading}
                onRefresh={()=>{
                  this.makeRemoteRequest()
                }}
              />
            }
        >
            {showOffline && 
              <Message message='Sin conexión' />
            }
  
            {isError && this.renderError()}
            
            {!loading && !isError && this.renderItem(newsItem)}
  
            {this.renderRelatedNews()}
            
            
          </Content>
          {!loading && !isError &&
          <Fab
          active={true}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: unperiodicoVariables.titleFontColor }}
          position="bottomRight"
          onPress={() => this.onShare()}
        >
          <IconNB name="md-share" />
        </Fab>
        }
        </Container>
        </StyleProvider>
      );
  }

  
}

export default Detail
