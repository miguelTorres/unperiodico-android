import React, { Component } from "react";
import {TouchableHighlight, RefreshControl, Keyboard} from "react-native";
import {
  StyleProvider,
  Container,
  Header,
  Button,
  Icon,
  Item,
  Input,
  Content,
  Text,
  Left,
  Body
} from "native-base";
import _ from 'lodash'
import styles from "./styles";
import getTheme from '../../../native-base-theme/components'
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import appConfig from '../../models/appConfig'
import HTMLRender from "../../components/HTMLRender";
import jsonApi from "../../api/jsonApi";
import News from "../News/News"
import CustomError from "../../components/error/CustomError";
import Bar from '../bar/bar'
import avoidDoubleAction from "../../services/avoidDoubleAction";
import { EventRegister } from "react-native-event-listeners";
import firebaseConstants from "../../models/firebaseConstants";
import { calculateResponsiveValue } from "../../helpers/Responsive";
import CustomHeader from "../../components/CustomHeader";
import CustomRefreshControl from "../../components/CustomRefreshControl";

class Searchbar extends Component {
  constructor(props){
    super(props)
    this.onLoadForm = this.onLoadForm.bind(this)
    this.state = {
      elements: null,
      ready: false,
      loading: false,
    }
    this.subject = ''
    this.onChangeText = this.onChangeText.bind(this)
    this.onEndEditing = this.onEndEditing.bind(this)
  
  }
  componentDidMount(){
    this.listenerUpdateApp = EventRegister.addEventListener(firebaseConstants.UPDATE_APP, () => {
      this.isComponentMount && this.setState({
        news: null
      })
    })
    this.isComponentMount = true
  }
  componentWillUnmount(){
    this.isComponentMount = false
    EventRegister.removeEventListener(this.listenerUpdateApp)
  }

  search(subject){
    if(!this.state.elements){
      return
    }
    this.isComponentMount && this.setState({
      loading: true,
      news: null
    })
    var elements = this.state.elements
    const url = appConfig.config.app.SEARCH_RESULTS_URL
    var formdata = new FormData()


    _.forEach(elements, (element)=>{
      if(element.name == 'tx_news_pi1[search][subject]'){
        formdata.append(element.name, subject)
      }else{
        if(typeof element.value !== 'undefined'){
          formdata.append(element.name, element.value)
        }
      }
    })


    var options = {
      method: 'POST',
      cache: "no-cache",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: formdata,
    }

    jsonApi.getAll(url, options).then(news => {
      this.isComponentMount && this.setState({
        news: news,
        loading: false
      })
    }).catch(error => {
      this.isComponentMount && this.setState({
        loading: false,
        error: 'Error en la búsqueda. Intente más tarde.'
      })
      //throw(error);
    });
  }

  onLoadForm(elements){
    this.isComponentMount && this.setState({
      elements: elements,
      ready: true
    })
  }
  onChangeText(subject){
    this.subject = subject
  }
  onEndEditing(){
    if(!avoidDoubleAction.isActionActive('search-onEndEditing')){
      return
    }
    Keyboard.dismiss()
    this.search(this.subject)
  }
  renderError(){
    return(
      <CustomError 
        error={this.state.error} 
        buttonText="Recargar" 
        onPress={this.onEndEditing}
        icon = {{name: 'ios-refresh'}} 
        />
    )
  }

  render() {
    const url = appConfig.config.app.SEARCH_FORM_URL
    const searchView = 'Search'
    const news = {
      items: !!this.state.news?this.state.news.news:null
    }
    const isError = !!this.state.error
    
    return (
      <StyleProvider style={getTheme(unperiodicoVariables)}>
      <Container style={styles.container}>
        <CustomHeader searchBar rounded>
            <Left>
            <Bar showLogo={false} navigation={this.props.navigation} />
            </Left>
            <Body>
            <Item>
            <Input 
            editable={this.state.ready}
            onChangeText={this.onChangeText}
            onEndEditing={this.onEndEditing}
            placeholder="Buscar" />
            <Button bordered style={styles.headerButton} onPress={this.onEndEditing}>
            <Icon active name="search" style={styles.headerIcon} />
            </Button>
          </Item>
            </Body>
          
        </CustomHeader>

        <Content 
            refreshControl={
              <CustomRefreshControl
                refreshing={this.state.loading}
                onRefresh={this.onEndEditing}
              />
            }
        >
          <HTMLRender url={url} onLoadForm={this.onLoadForm} />
          {isError && this.renderError()}
          {this.state.news && !this.state.loading && 
            <News isStaticNews newsByCategory = {news} view={searchView} navigation={this.props.navigation} filtroFecha={0} />
          }
          
        </Content>
      </Container>
      </StyleProvider>
    );
  }
}

export default Searchbar;
