import React, { Component } from 'react'
import {
  View,
  ListView,
  Image,
  StyleSheet,
  ScrollView,
  FlatList,
  ActivityIndicator,
  RefreshControl
} from 'react-native'
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  H1,
  H2,
  H3,
  Right,
  Spinner
} from "native-base";

import DatePicker from 'react-native-datepicker'

import moment from 'moment'
require('moment/locale/es');

import _ from 'underscore'

import styles from "./styles"
import Assets from '../../../assets'
import config from '../../../config'
import HTML from '../../components/HTML'
import styleVariables from '../../../native-base-theme/variables/unperiodico-material'

import DefaultView from '../../components/list/DefaultView/DefaultView'
import CarouselView from '../../components/list/CarouselView/CarouselView'
import Carousel from '../../components/list/Carousel/Carousel'
import Default from '../../components/list/Default/Default'

import appConfig from '../../models/appConfig'
import CustomError from '../../components/error/CustomError'
import NewsModel from '../../models/news';
import Message from '../../components/Message';
import HomeConstants from '../home/HomeConstants';
import { EventRegister } from 'react-native-event-listeners';
import tabService from '../../services/tabService';
import FiltroFecha from '../../components/FiltroFecha';
import firebaseConstants from '../../models/firebaseConstants';

export default class News extends Component {
  constructor(props) {
    super(props)

    this.state = {
      initial: true,
      isOnline: false,
      tabReady: false,
      date: null,
      filteredNewsObject: {},
      isRefreshing: false
    }
    this._onEndReached = this._onEndReached.bind(this)
    this._onRefresh = this._onRefresh.bind(this)
    this.handleRefresh = this.handleRefresh.bind(this)
    this.isRefreshing = this.isRefreshing.bind(this)
    this.state.tabReady = true
    this.onDateChange=this.onDateChange.bind(this)
    this.resetDate=this.resetDate.bind(this)
    this.setContainerHeight=this.setContainerHeight.bind(this)
    this.setContentHeight = this.setContentHeight.bind(this)

    if(typeof this.props.isStaticNews !== 'undefined'){
      this.state.isOnline = true
    }


  }

  componentWillMount() {
    this.dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    
    
    this.willMount = false
    EventRegister.removeEventListener(this.listenerUpdateApp)


  }
  componentDidMount(){
    this.listenerUpdateApp = EventRegister.addEventListener(firebaseConstants.UPDATE_APP, () => {
      this.handleRefresh()      
    })

    this.isComponentMount = true
    if(!this.props.onlineLoaded){
      this.makeRemoteRequest(true)
    }
  }
  componentWillUnmount(){
    this.isComponentMount = false
    this.willMount = true
    if(this.listenerTabOnChange){
      EventRegister.removeEventListener(this.listenerTabOnChange)
    }
    EventRegister.removeEventListener(this.listenerUpdateApp)
  }
  calculateUrl(url){
    if(typeof url === 'undefined' || !this.state.date){
      return url
    }
    if(url.indexOf('?')>=0){
      url +=  '&no_cache=1&tx_news_pi1[overwriteDemand][timeRestrictionHigh]=- '+ this.state.timeRestrictionHigh + ' days'
    }else{
      url += '?no_cache=1&tx_news_pi1[overwriteDemand][timeRestrictionHigh]=- '+ this.state.timeRestrictionHigh + ' days'
    }
    return url
  }
  makeRemoteRequest = (initial, refresh) => {
    if(typeof this.props.isStaticNews !== 'undefined'){
      return
    }
    if(!!this.state.date){
      return this.makeRemoteRequestFiltered(initial, refresh)
    }

    let nextPage
    var isPid = typeof this.props.pid !== 'undefined'
    var isCategory = typeof this.props.category.item !== 'undefined'

    if(initial || refresh){
      if(isCategory){
        this.setState({
          isRefreshing: true
        })
  
        NewsModel.load(this.props, this.getCategory(), appConfig.config.app.SITE_URL + this.props.category.additional.url, true).then(()=>{
          if(!!this.willMount){
            return
          }
          this.isComponentMount && this.setState({
            ...this.state,
            isOnline: true,
            isRefreshing: false
          })
        }).catch((error)=>{
          this.setState({
            isRefreshing: false
          })
        })
        return
      }

      if(typeof this.props.url !== 'undefined'){
        this.setState({
          isRefreshing: true
        })
  
        NewsModel.load(this.props, this.getCategory(), this.props.url, initial || refresh, this.props.jsonkey).then(()=>{
          if(!!this.willMount){
            return
          }
          this.isComponentMount && this.setState({
            ...this.state,
            isOnline: true,
            isRefreshing: false
          })
        }).catch((error)=>{
          this.setState({
            isRefreshing: false
          })
        })
        return
      }  
    }

    nextPage = this.props.newsByCategory.nextPage


    if(!initial && !nextPage){
      return
    }

    var page = _.findWhere(this.props.newsByCategory.pagination.pages, {number: nextPage});   
    

    if(page){
      this.props.loadNews(this.getCategory(),appConfig.config.app.SITE_URL + page.url)
    }


  }

  makeRemoteRequestFiltered = (initial, refresh) => {
    if(typeof this.props.isStaticNews !== 'undefined'){
      return
    }

    let nextPage
    var isPid = typeof this.props.pid !== 'undefined'
    var isCategory = typeof this.props.category.item !== 'undefined'

    if(initial || refresh){
      if(isCategory){
        NewsModel.loadFiltered({}, 'filteredNews', appConfig.config.app.SITE_URL + this.calculateUrl(this.props.category.additional.url), true).then((newObject)=>{
          if(!!this.willMount){
            return
          }
          this.isComponentMount && this.setState({
            ...this.state,
            isOnline: true,
            filteredNewsObject: newObject
          })
        }).catch((error)=>{
          
        })
        return
      }
  
      if(typeof this.props.url !== 'undefined'){
        NewsModel.loadFiltered({}, 'filteredNews', this.calculateUrl(this.props.url), initial || refresh, this.props.jsonkey).then((newObject)=>{
          if(!!this.willMount){
            return
          }
          this.isComponentMount && this.setState({
            ...this.state,
            isOnline: true,
            filteredNewsObject: newObject
          })
        }).catch((error)=>{
          
        })
        return
      }  
    }

    nextPage = this.state.filteredNewsObject['filteredNews'].nextPage


    if(!initial && !nextPage){
      return
    }

    var page = _.findWhere(this.state.filteredNewsObject['filteredNews'].pagination.pages, {number: nextPage});   
    

    if(page){
      NewsModel.loadFiltered(this.state.filteredNewsObject, 'filteredNews', appConfig.config.app.SITE_URL + page.url).then((newObject)=>{
        if(!!this.willMount){
          return
        }
        this.isComponentMount && this.setState({
          ...this.state,
          isOnline: true,
          filteredNewsObject: newObject
        })
      }).catch((error)=>{
        
      })

    }


  } 

  isCategory = () => {
    return typeof this.props.category.item !== 'undefined'
  }
  isPid = () => {
    return typeof this.props.pid !== 'undefined'
  }
  getCategory = () => {
    if(this.isCategory()){
      return this.props.category.item.title
    }else{
      return this.props.category
    }
  }
  isLoading = () => {
    return this.getNewsByCategory().initialLoading && this.props.newsByCategory.loading
  }
  isLoadingMore = () => {
    return !this.getNewsByCategory().initialLoading && this.getNewsByCategory().loading
  }
  isRefreshing = () => {
    return (typeof this.getNewsByCategory() !== 'undefined' && typeof this.getNewsByCategory().refreshing !== 'undefined'? this.props.newsByCategory.refreshing:false) || this.state.isRefreshing
  }

  
  hasNextPage = () => {
    const hasNextPage = !!this.getNewsByCategory().nextPage
    return hasNextPage
  }


  handleRefresh = () => {
    this.makeRemoteRequest(true, true)
  }
  _onRefresh = () => {
    this.isComponentMount && this.setState({refreshing: true});

  }
  _onEndReached = (d) => {
    if(!this.isLoadingMore()){
      this.makeRemoteRequest()
    }
    
  }
  renderView(auxFooterHeight, contentHeight){
    let items
    items = this.getNewsByCategory().items
    var NEWS_TYPE = appConfig.config.app.list.NEWS_TYPE
    var CONTENT_TYPE = appConfig.config.app.list.CONTENT_TYPE

    if(this.props.view == 'Carousel'){
      return (
        <Carousel 
          data={items} 
          isRefreshing={this.isRefreshing} 
          handleRefresh={this.handleRefresh} 
          onEndReached={this._onEndReached}
          onRefreshControl={this._onRefresh}
          hasNextPage = {this.hasNextPage}
          NEWS_TYPE = {NEWS_TYPE}
          CONTENT_TYPE = {CONTENT_TYPE}
         />
      )  
    }
    if(this.props.view == 'CarouselView'){
      if(typeof contentHeight === 'undefined'){
        return null
      }
      return (
        <CarouselView 
          {...this.props}
          data={items} 
          isRefreshing={this.isRefreshing} 
          handleRefresh={this.handleRefresh} 
          onEndReached={this._onEndReached}
          onRefreshControl={this._onRefresh}
          hasNextPage = {this.hasNextPage}
          NEWS_TYPE = {NEWS_TYPE}
          CONTENT_TYPE = {CONTENT_TYPE}
          contentHeight = {contentHeight}
          indicator={Assets.indicatorBlack}
          inactiveIndicator={Assets.indicatorGray}
           />
      )  
    }


    return (
      <View>

      <Default 
        {...this.props}
        data={items} 
        isRefreshing={this.isRefreshing} 
        handleRefresh={this.handleRefresh} 
        onEndReached={this._onEndReached}
        onRefreshControl={this._onRefresh}
        hasNextPage = {this.hasNextPage}
        NEWS_TYPE = {NEWS_TYPE}
        CONTENT_TYPE = {CONTENT_TYPE}
        auxFooterHeight={auxFooterHeight}
        />
        </View>
    )  
    


  }


  onDateChange(date){
    //moment(date)
    var timeRestrictionHigh = moment().diff(moment(date, 'YYYY-MM-DD'), 'days') ;

    this.isComponentMount && this.setState({
      date: date,
      timeRestrictionHigh: timeRestrictionHigh
    }, ()=>{
      this.handleRefresh()
    })
    
  }
  resetDate(){
    this.isComponentMount && this.setState({
      date: null,
      timeRestrictionHigh: null
    })

  }
  isFiltroFecha(){

    return !(typeof this.props.filtroFecha !== 'undefined' && this.props.filtroFecha*1==0)
  }

  renderDatePicker(){
    if(!this.isFiltroFecha()){
      return null
    }
    return (
      <View>
      <FiltroFecha
        date={this.state.date}
        onDateChange={this.onDateChange}
        onResetDate={this.resetDate}
      />

      </View>
    )
  }  
  setContainerHeight(event){
    this.isComponentMount && this.setState({
      containerHeight: event.nativeEvent.layout.height
    })
  }

  getNewsByCategory(){
    return !!this.state.date? this.state.filteredNewsObject['filteredNews'] : this.props.newsByCategory
  }
  setContentHeight(event){
    this.isComponentMount && this.setState({
      contentHeight: event.nativeEvent.layout.height
    })
  }

  render() {

    const isRefreshing = this.isRefreshing()
    const showError = (typeof this.getNewsByCategory() !== 'undefined' && this.getNewsByCategory().error !== null && (!this.getNewsByCategory().items || this.getNewsByCategory().items.length === 0))

    const showSpinner = (typeof this.getNewsByCategory() === 'undefined' || isRefreshing || this.isLoading() || !this.state.tabReady)
    
    if(showError){
      return(
        <View>
          
          {this.renderDatePicker()}
          <CustomError error={this.getNewsByCategory().error} buttonText="Recargar" onPress={()=>{this.handleRefresh()}} icon = {{name: 'ios-refresh'}} />
        </View>
      )
    }    
    if(showSpinner){
      return(
        <View
        >
          <Spinner />
        </View>
      )
    }
    const reloading = false
    const refreshing = false
    const showOffline = !this.props.onlineLoaded && !this.state.isOnline

    const auxFooterHeight = this.isFiltroFecha()?
    config.filtroFecha.height + 50 + (showOffline? 25:0):
    0

    return (
      <View onLayout={this.setContentHeight} style={{flex: 1}}>
        {showOffline && 
          <Message message='Sin conexión' />
        }
        {this.renderDatePicker()}
        {this.getNewsByCategory().items.length === 0 && 
          <Text style={styles.message}>No se encontraron noticias</Text>
        }
        {this.getNewsByCategory().items.length > 0 && 
          this.renderView(auxFooterHeight, this.state.contentHeight)
        }
      </View>
    )

  


     
    
    
    
  }
}

