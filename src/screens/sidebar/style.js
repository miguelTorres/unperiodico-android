import { calculateResponsiveValue } from "../../helpers/Responsive";

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 4.2,
    width: null,
    //position: "relative",
    //marginBottom: 10,
    justifyContent: 'center', alignItems: 'center',
  },
  listItems: {
    marginTop: 10,
    flex: 1,
  },
  drawerImage: {
    //position: "absolute",
    //left: Platform.OS === "android" ? deviceWidth / calculateResponsiveValue(10) : deviceWidth / calculateResponsiveValue(9),
    //top: Platform.OS === "android" ? deviceHeight / calculateResponsiveValue(13) : deviceHeight / calculateResponsiveValue(12),
    width: calculateResponsiveValue(200),
    height: calculateResponsiveValue(57),
    resizeMode: "cover"
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20,
  
  },
  textTitle: {
    color: '#666',
    fontSize: 13,
    marginLeft: 20,
    fontWeight: 'bold'
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  }
};
