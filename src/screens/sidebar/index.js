import React, { Component } from "react";
import { Image, Linking, ImageBackground } from "react-native";
import {
  StyleProvider,
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,

} from "native-base";
import styles from "./style";
import appConfig from '../../models/appConfig'
import Assets from '../../../assets'
import { EventRegister } from "react-native-event-listeners";
import HomeConstants from "../home/HomeConstants";
import avoidDoubleAction from "../../services/avoidDoubleAction";
import FloatingViewConstants from "../../components/FloatingViewConstants";
import {timeout} from "../../helpers/Functions"
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import getTheme from '../../../native-base-theme/components'

const drawerCover = require("../../../assets/drawer-cover.png");
const drawerImage = require("../../../assets/logo-kitchen-sink.png");
const datas = [
  {
    title: "Home",
    route: "Home",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    title: "Calificar encuesta",
    route: "Poll",
    icon: "podium",
    bg: "#C5F442"
  },
  {
    title: "Noticias guardadas",
    route: "SavedNews",
    icon: "bookmarks",
    bg: "#C5F442"
  },
  {
    title: "Anatomy",
    route: "Anatomy",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    title: "Actionsheet",
    route: "Actionsheet",
    icon: "easel",
    bg: "#C5F442"
  },
  {
    title: "Header",
    route: "Header",
    icon: "phone-portrait",
    bg: "#477EEA",
    types: "8"
  },
  {
    title: "Footer",
    route: "Footer",
    icon: "phone-portrait",
    bg: "#DA4437",
    types: "4"
  },
  {
    title: "Badge",
    route: "NHBadge",
    icon: "notifications",
    bg: "#4DCAE0"
  },
  {
    title: "Button",
    route: "NHButton",
    icon: "radio-button-off",
    bg: "#1EBC7C",
    types: "9"
  },
  {
    title: "Card",
    route: "NHCard",
    icon: "keypad",
    bg: "#B89EF5",
    types: "5"
  },
  {
    title: "Check Box",
    route: "NHCheckbox",
    icon: "checkmark-circle",
    bg: "#EB6B23"
  },
  {
    title: "Deck Swiper",
    route: "NHDeckSwiper",
    icon: "swap",
    bg: "#3591FA",
    types: "2"
  },
  {
    title: "Fab",
    route: "NHFab",
    icon: "help-buoy",
    bg: "#EF6092",
    types: "2"
  },
  {
    title: "Form & Inputs",
    route: "NHForm",
    icon: "call",
    bg: "#EFB406",
    types: "12"
  },
  {
    title: "Icon",
    route: "NHIcon",
    icon: "information-circle",
    bg: "#EF6092"
  },
  {
    title: "Layout",
    route: "NHLayout",
    icon: "grid",
    bg: "#9F897C",
    types: "5"
  },
  {
    title: "List",
    route: "NHList",
    icon: "lock",
    bg: "#5DCEE2",
    types: "7"
  },
  {
    title: "ListSwipe",
    route: "ListSwipe",
    icon: "swap",
    bg: "#C5F442",
    types: "2"
  },
  {
    title: "Picker",
    route: "NHPicker",
    icon: "arrow-dropdown",
    bg: "#F50C75"
  },
  {
    title: "Radio",
    route: "NHRadio",
    icon: "radio-button-on",
    bg: "#6FEA90"
  },
  {
    title: "SearchBar",
    route: "NHSearchbar",
    icon: "search",
    bg: "#29783B"
  },
  {
    title: "Segment",
    route: "Segment",
    icon: "menu",
    bg: "#0A2C6B",
    types: "2"
  },
  {
    title: "Spinner",
    route: "NHSpinner",
    icon: "navigate",
    bg: "#BE6F50"
  },
  {
    title: "Tabs",
    route: "NHTab",
    icon: "home",
    bg: "#AB6AED",
    types: "3"
  },
  {
    title: "Thumbnail",
    route: "NHThumbnail",
    icon: "image",
    bg: "#cc0000",
    types: "2"
  },
  {
    title: "Toast",
    route: "Toast",
    icon: "albums",
    bg: "#C5F442"
  },
  {
    title: "Typography",
    route: "NHTypography",
    icon: "paper",
    bg: "#48525D"
  }
];
const datas2 = datas.map((item=>({...item, type: 'route'})))
class SideBar extends Component {
  constructor(props) {
    super(props);
    this.componentSideMenu = {}
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
    this.sideMenu = Object.keys(appConfig.config.app.sideMenu).map((key, index)=>{
      var val = appConfig.config.app.sideMenu[key]
      if(val.type === 'component'){
        var componentName = 'createComponent_'+val.view
        return {
          ...val,
          component: this.createComponent(componentName, val, index),
        }          
      }
      return val
    })
    this.renderRow = this.renderRow.bind(this)
  }
  createComponent(createComponentFunc, currentMenuItem, index){
    if(typeof this[createComponentFunc] === 'undefined' ){
      return <Text>Componente no registrado en sidebar {index}</Text>
    }

    if(typeof this.componentSideMenu[index] === 'undefined'){
      const component = this[createComponentFunc](currentMenuItem)
      this.componentSideMenu[index] = component
    }
    return this.componentSideMenu[index]
  }
  componentDidMount(){
    return
    this.listenerOnChangeFloatingView = EventRegister.addEventListener(FloatingViewConstants.layout.ONCHANGE, async () => {
      await timeout(5000)
      this.setState({
        change: !this.state.change
      })
    })
  }
  componentWillUnmount(){
    EventRegister.removeEventListener(this.listenerOnChangeFloatingView)
  }
  onPress(data){
    if(!avoidDoubleAction.isActionActive()){
      return
    }

    if(data.type == 'route' || typeof data.type === 'undefined'){
      this.props.navigation.push (data.route, data.params)
    }
    if(data.type == 'link'){
      Linking.openURL(data.url).catch(err => {});
    }

    if(data.type == 'footerTab'){
      this.props.navigation. closeDrawer()
      this.props.navigation.navigate ("Home")
      setTimeout(() => {
        EventRegister.emit(HomeConstants.GO_TO_TAB, {page: 2, category: 'footerTab', comparation: data.footerTabComparation})        
      }, 300);
    }
    if(data.type == 'headerTab'){
      this.props.navigation. closeDrawer()
      this.props.navigation.navigate ("Home")
      setTimeout(() => {
        EventRegister.emit(HomeConstants.GO_TO_TAB, {page: 1, category: 'footerTab', comparation: data.footerTabComparation})
        setTimeout(() => {
          EventRegister.emit(HomeConstants.GO_TO_TAB, {page: 1, category: 'headerTab', comparation: data.headerTabComparation})
        }, 300);
      }, 300);
    }
    //this.props.navigation. closeDrawer()


  }
  renderIcon(data){
    const icon = data.icon

    if(typeof icon === 'undefined'){
      return null
    }
    if(typeof icon === 'string'){
      return (
        <Icon
          active
          name={icon}
          style={{ color: "#777", fontSize: 26, width: 20 }}
        />
      )  
    }

    if(typeof icon.url === 'string'){
      return (
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain'}}
          source={{uri: icon.url}}
        />
  
      )
    }
    return null

  }
  renderRow(data){

    if(data.type == 'title'){
      return(
        <ListItem itemHeader style={{paddingBottom: 12,}}>
          <Left>
            {this.renderIcon(data)}

            <Text style={styles.textTitle}>
              {data.title}
            </Text>
          </Left>
        </ListItem>
      )
    }
    return(
        <ListItem
                button
                noBorder
                onPress={() => this.onPress(data)}
              >
                <Left>
                  {this.renderIcon(data)}

                  <Text style={styles.text}>
                    {data.title}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>
    )
  }

  render() {
    return (
      <StyleProvider style={getTheme(unperiodicoVariables)}>
      <Container>
        <Content
          bounces={false}
          style={{ backgroundColor: "#fff", flex: 1 }}
        >
          <ImageBackground source={drawerCover} style={styles.drawerCover}>
            <Image square style={styles.drawerImage} source={Assets.logoInvertido} />
          </ImageBackground>
          

          <List
            style={styles.listItems}
            dataArray={this.sideMenu}
            renderRow={this.renderRow}
            removeClippedSubviews={false}
          />
        </Content>
      </Container>
      </StyleProvider>
    );
  }
}

export default SideBar;
