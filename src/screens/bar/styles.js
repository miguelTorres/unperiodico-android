const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
import unperiodicoVariables from '../../../native-base-theme/variables/unperiodico-material'
import { calculateResponsiveValue } from '../../helpers/Responsive';

export default {
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: "#FFF"
  },
  title: {
    color: unperiodicoVariables.titleFontColor,
    fontSize: 18
  },
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 40 : 50,
    top: Platform.OS === "android" ? 35 : 60,
    width: 280,
    height: 100
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  },
  mb: {
    marginBottom: 15
  },

  modalContainer:{
    backgroundColor:'#ebeef0',
    flex:1,
    alignItems:'center'     //<-----
  },  
  modalButtonsContainer:{
    flex: 1,
    justifyContent: 'center',
    flexDirection: "row"
  },  
  buttonsContainer:{
    flex: 1,
    //justifyContent: 'center',
    flexDirection: "row",
    alignItems: "flex-start"
  }, 
  button: {
    marginLeft: 5,
    marginRight: 5
  } ,
  icon: {
    marginLeft: 0, 
    marginRight: 0
  },
  listItem: {
    borderBottomColor: 'rgba(126, 77, 119, 1)',

    borderBottomWidth: 0.5,
    paddingLeft: 2,
    marginLeft: 5,
  },
  listItemBody: {
    borderColor: 'rgba(256, 256, 256, 0)'
  },
  listItemRight: {
    borderColor: 'rgba(256, 256, 256, 0)'
  },

  modalContent:{
    backgroundColor:'#ef553a',
    /*width:300,*/
    paddingTop:10,
    paddingBottom:20,
    paddingLeft:20,
    paddingRight:20, 
    borderRadius:10
  }, 
  offline: {
    backgroundColor: '#000000',
    color: '#FFFFFF',
    textAlign: 'center',
    paddingTop: 0,
    /*height: 20,*/
  },
  headerButton: {
    borderWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  headerIcon: {
    marginLeft: 1,
    marginRight: calculateResponsiveValue(10, 30),
    fontSize: calculateResponsiveValue(25, 30),  
  },
  htmlImage: {
    resizeMode: "cover",
    width: '100%',
    height: 200,
    flex: 1
  },


};

