import ApiUtils from './ApiUtils'
import config from '../../config'
import base64 from 'base-64'
import utf8 from 'utf8'
import appConfig from '../models/appConfig';

class GenerateUrl {
  static generateUrl(params, generateUrl) {
    const params64 = GenerateUrl.object64(params)
    const paramsGenerateUrl = {
      type: 215653469,
      'tx_unalgenerateurl_unalgenerateurl[generateUrl]': generateUrl,
      'tx_unalgenerateurl_unalgenerateurl[action]': 'generateUrl',
      'tx_unalgenerateurl_unalgenerateurl[controller]': 'GenerateUrl',
      'tx_unalgenerateurl_unalgenerateurl[data]': params64
    }
    const queryString = GenerateUrl.objToQueryString(paramsGenerateUrl)
    const url = `${appConfig.config.app.GENERATE_URL}&${queryString}`
    return fetch(url)
    .then(ApiUtils.checkStatus)
    .then(response => {
      if(generateUrl){
        return response.text();
      }
      return response;
    }).catch(error => {
      return error;
    });
  }
  static object64(obj){
    const str = JSON.stringify(obj)
    return GenerateUrl.encode64RN(str);
  }
  static encode64(str){
    return btoa(str)
  }
  static encode64RN(str){
    var bytes = utf8.encode(str);
    var encoded = base64.encode(bytes);
    return encoded;
  }  
  static objToQueryString(obj) {
    const keyValuePairs = [];
    for (const key in obj) {
      keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return keyValuePairs.join('&');
  }  
}

export default GenerateUrl;