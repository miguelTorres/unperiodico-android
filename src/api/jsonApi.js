import ApiUtils from './ApiUtils'
import GenerateUrl from './generateUrl'

export async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

class JsonApi {
  static getAll(url,options={}, attempts) {
    attempts = (!!attempts)? attempts : 4
    return fetch(url, options)
    .then(ApiUtils.checkStatus)
    .then(response => {
      return response.json();
    }).catch(async (error)=>{
      if ((!!error.response) && error.response.status == 503) {
        if(attempts === 1){
          throw error;
          return
        }
        await wait(3000)
        return JsonApi.getAll(url, options, attempts - 1)
      }else{
        throw error
      }
    });
  }

  static getAllFromGenerateUrl(id, params) {
    return GenerateUrl.generateUrl({
      id: id,
      parameters: params,
    }, 0)
    .then(response => response.json())
    .catch(error => {
      throw error;
    });
  }  
}

export default JsonApi;