import ApiUtils from './ApiUtils'
import appConfig from '../models/appConfig';
class Ping {
  static ping() {
    return fetch(appConfig.config.app.PING_URL, {method: 'HEAD'})
    .then(ApiUtils.checkStatus)
    .then(response => {
      return true;
    })
    .catch(error => {
      throw error;
    });
  }

}

export default Ping;
