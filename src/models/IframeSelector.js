import DefaultSelector from './DefaultSelector'
import FloatingViewConstants from '../components/FloatingViewConstants';
export default class IframeSelector extends DefaultSelector {
  constructor(item) {
    super(item)
    this.type = 'Iframe'
  }
  isSelected(){
    return this.isCE() && typeof this.item.additional.contentElementShowInListIdList.iframe !== 'undefined'
  }
  getMedias(){
    return [this.item.additional.contentElementShowInListIdList.iframe]
  }
  getIcon(){
    return {
      name: 'wallet',
      type: 'MaterialCommunityIcons'
    }
  }
  getInitialFloatingState(){
    return FloatingViewConstants.layout.MINIMIZED
  }
  getLockToLandscapeOnMaximize(){
    return true
  }
  getBlock(){
    return false
  }


}