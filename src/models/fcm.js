import { Platform } from "react-native";
import firebase from 'react-native-firebase';
import config from '../../config'
import Ping from "../api/ping";
import appConfig from "./appConfig";


var navigation = null
var handled = false
var currentUid = []
class Fcm {
    init(){
        
    }
    setCurrentUid(uid){
        currentUid.push(uid)
    }
    getCurrentUid(){
        return currentUid.length > 0 ? currentUid[currentUid.length-1]:null
    }
    removeCurrentUid(){
        currentUid.pop()
    }
    async manageNotification(navigationin){
        navigation = navigationin
        if(!handled){
            this.listenNotification()
            handled = true
        }
        
    }
    manageSubscription(subscribed){
        return firebase.messaging().requestPermission()
        .then(() => {
            // User has authorised  
            return Ping.ping().then(()=>{
                if(config.PRODUCTION){
                    if(subscribed){
                        firebase.messaging().unsubscribeFromTopic(appConfig.config.app.subscribeToTopic)
                    }else{
                        firebase.messaging().subscribeToTopic(appConfig.config.app.subscribeToTopic)
                    }
    
                }else{
                    if(subscribed){
                        firebase.messaging().unsubscribeFromTopic('testtopic3')
                    }else{
                        firebase.messaging().subscribeToTopic('testtopic3')
                    }

                }

                return true
            })
        })
        .catch(error => {
            // User has rejected permissions 
        })


    }
    onNotificationOpened(notificationOpen){
        if(!notificationOpen){
            return
        }
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification = notificationOpen.notification;
            const item = {
                uid: notification._data.uid,
                title: notification._data.title,
                additional: {
                    url: notification._data.url
                }
            }
            if(this.getCurrentUid()==item.uid){
                return
            }

            navigation.push ('Detail', {
                item
              });
    }
    
    listenNotification(){
        // Build a channel
        const channel = new firebase.notifications.Android.Channel('my_default_channel', 'Canal de UN Periódico', firebase.notifications.Android.Importance.Max)
        .setDescription('Canal de UN Periódico')
        .setSound('unnotification.mp3')
        ;

        // Create the channel
        firebase.notifications().android.createChannel(channel);  
        
        

        firebase.notifications().getInitialNotification().then((notificationOpen)=>{
            this.onNotificationOpened(notificationOpen)
        })
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            this.onNotificationOpened(notificationOpen)
        });
        /*
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        });*/
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            //https://github.com/invertase/react-native-firebase/issues/988#issuecomment-383175789
            const localNotification = new firebase.notifications.Notification({
                //sound: 'default',
                show_in_foreground: true,
              })
              .setSound('unnotification.mp3')
              .setNotificationId(notification.notificationId)
              .setTitle(notification.title)
              .setSubtitle(notification.subtitle)
              .setBody(notification.body)
              .setData(notification.data)
              .android.setChannelId('my_default_channel') // e.g. the id you chose above
              .android.setSmallIcon('@drawable/ic_notification') // create this icon in Android Studio
              .android.setColor('#801836') // you can set a color here
              .android.setAutoCancel(true)
              .android.setPriority(firebase.notifications.Android.Priority.High);
    
            firebase.notifications()
              .displayNotification(localNotification)
              .catch(err => {});
            return
            notification.android.setChannelId("my_default_channel");
            firebase.notifications().displayNotification(notification)
            return


            // Process your notification as required
            const newNotification = new firebase.notifications.Notification()
                .setNotificationId(notification._notificationId + 'local')
                .setTitle(notification._title)
                .setBody(notification._body)
                .setData(notification._data);
            if(Platform.OS === "android"){
                newNotification
                .android.setChannelId('my_default_channel')
            }
            firebase.notifications().displayNotification(newNotification)
        });
    }

    finalize(){
        if(this.notificationOpenedListener){
            this.notificationOpenedListener();
        }
        /*
        if(this.notificationDisplayedListener){
            this.notificationDisplayedListener();
        }*/
        if(this.notificationListener){
            this.notificationListener();
        }
       
    }

}
let FCM = new Fcm();
export default FCM;
