import _ from "lodash";
import appConfig from './appConfig'
import FloatingViewConstants from '../components/FloatingViewConstants'
export default class DefaultSelector {
    constructor(item){
        this.item = item
        this.type = 'Default'
    }
    isSelected(){
        return true;
    }
    isCE(){
        return (!!this.item.additional.contentElementShowInListIdList)
    }
    isCEImage(){
        const isImage = (!!this.item.additional.contentElementShowInListIdList) && typeof this.item.additional.contentElementShowInListIdList.images !== 'undefined'
        return isImage;
    }
    get imageUrl(){
        const isImage = this.isCEImage();
        const img = isImage? appConfig.config.app.SITE_URL + this.item.additional.contentElementShowInListIdList.images[0].src
            : (this.item.additional.falMediaPreviews? appConfig.config.app.SITE_URL + this.item.additional.falMediaPreviews.src:null)
        return img
    }
    getImageUrl(index){
        if(this.isCEImage()
        && this.item.additional.contentElementShowInListIdList.images.length > index){
            return appConfig.config.app.SITE_URL + this.item.additional.contentElementShowInListIdList.images[index].src
        }
        
        return (this.item.additional.falMediaPreviews? appConfig.config.app.SITE_URL + this.item.additional.falMediaPreviews.src:null)
    }
    getFigcaption(index){
        var image
        if(this.isCEImage()
        && this.item.additional.contentElementShowInListIdList.images.length > index){
            image =  this.item.additional.contentElementShowInListIdList.images[index]
        }else{
            image = this.item.additional.falMediaPreviews
        }
        return (!!image.figCaption) ? image.figCaption : ''
    }
    getMedias(){
        return []
    }
    getMediaObjects(){
        var medias = this.getMedias()
        var mediaObjects = medias.length === 0? [{
                preview: this.getImageUrl(0),
                media: null
            }]
            : _.map(medias, (media, index)=>{
                if(typeof this['getImageUrlByMedia']!=='undefined'){
                    return {
                        preview: this.getImageUrlByMedia(media),
                        media: medias[index]
                    }    
                }
                return {
                    preview: this.getImageUrl(index),
                    media: medias[index]
                }
            })
        return mediaObjects;
    }
    getUrl(media){
        if(media && typeof media.sources !=='undefined'){
            return appConfig.config.app.SITE_URL + '/' + media.sources[0].src
        }
        return null
    }
    getInitialFloatingState(){
        return FloatingViewConstants.layout.MAXIMIZED
    }
    getLockToLandscapeOnMaximize(){
        return false
    }
    getIcon(){
        return null
    }
    getBlock(){
        return true
    }
}
