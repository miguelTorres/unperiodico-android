import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from '../reducers'
import syncOffline from './syncOffline'
import syncSavedNews from './syncSavedNews'
import syncCategories from './syncCategories'
import syncNews from './syncNews'
import syncNewsItem from './syncNewsItem'
import syncConfiguration from './syncConfiguration'
import { syncFirebase } from '../firebase'

function configureStore(initialState) {
  const store = createStore(
    reducer,
    applyMiddleware(thunk)
  )
  syncOffline(store)
  //syncFirebase(store)
  syncSavedNews(store)
  syncCategories(store)
  syncNews(store)
  syncNewsItem(store)
  syncConfiguration(store)
  
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('../reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store
}
const store = configureStore()
export default store