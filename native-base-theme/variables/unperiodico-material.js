import color from "color";

import { Platform, Dimensions, PixelRatio } from "react-native";
import { responsiveFontSize } from "../../src/helpers/Responsive";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = "material";
const isIphoneX =
  platform === "ios" && deviceHeight === 812 && deviceWidth === 375;

  export default {
  platformStyle,
  platform,

  // Android
  androidRipple: true,
  androidRippleColor: "rgba(256, 256, 256, 0.3)",
  androidRippleColorDark: "rgba(0, 0, 0, 0.15)",
  btnUppercaseAndroidText: true,

  // Badge
  badgeBg: "#ED1727",
  badgeColor: "#fff",
  badgePadding: 0,

  // Button
  btnFontFamily: "opensans",
  btnDisabledBg: "#b5b5b5",
  buttonPadding: 6,
  get btnPrimaryBg() {
    return this.brandPrimary;
  },
  get btnPrimaryColor() {
    return this.inverseTextColor;
  },
  get btnInfoBg() {
    return this.brandInfo;
  },
  get btnInfoColor() {
    return this.inverseTextColor;
  },
  get btnSuccessBg() {
    return this.brandSuccess;
  },
  get btnSuccessColor() {
    return this.inverseTextColor;
  },
  get btnDangerBg() {
    return this.brandDanger;
  },
  get btnDangerColor() {
    return this.inverseTextColor;
  },
  get btnWarningBg() {
    return this.brandWarning;
  },
  get btnWarningColor() {
    return this.inverseTextColor;
  },
  get btnTextSize() {
    return this.fontSizeBase - 1;
  },
  get btnTextSizeLarge() {
    return this.fontSizeBase * 1.5;
  },
  get btnTextSizeSmall() {
    return this.fontSizeBase * 0.8;
  },
  get borderRadiusLarge() {
    return this.fontSizeBase * 3.8;
  },
  get iconSizeLarge() {
    return this.iconFontSize * 1.5;
  },
  get iconSizeSmall() {
    return this.iconFontSize * 0.6;
  },

  // Card
  cardDefaultBg: "#fff",
  cardBorderColor: "#ccc",

  // CheckBox
  CheckboxRadius: 0,
  CheckboxBorderWidth: 2,
  CheckboxPaddingLeft: 2,
  CheckboxPaddingBottom: 5,
  CheckboxIconSize: 16,
  CheckboxIconMarginTop: 1,
  CheckboxFontSize: 17,
  DefaultFontSize: 17,
  checkboxBgColor: "#039BE5",
  checkboxSize: 20,
  checkboxTickColor: "#fff",

  // Color
  brandPrimary: "rgba(128,24,54,1)",
  brandInfo: "#3F57D3",
  brandSuccess: "#5cb85c",
  brandDanger: "#d9534f",
  brandWarning: "#f0ad4e",
  brandDark: "#000",
  brandLight: "#f4f4f4",

  // Font
  fontFamily: "opensans",
  fontSizeBase: responsiveFontSize(15),
  get fontSizeH1() {
    return this.fontSizeBase * 1.8;
  },
  get fontSizeH2() {
    return this.fontSizeBase * 1.6;
  },
  get fontSizeH3() {
    return this.fontSizeBase * 1.4;
  },

  // Footer
  footerHeight: responsiveFontSize(isIphoneX ? 89 : 55),
  footerDefaultBg: "#fff",
  footerPaddingBottom: responsiveFontSize(isIphoneX ? 34 : 0),

  // FooterTab
  tabBarTextColor: "#4d4d4d",
  tabBarTextSize: 11,
  activeTab: "#fff",
  sTabBarActiveTextColor: "rgba(128,24,54,1)",
  tabBarActiveTextColor: "#fff",
  tabActiveBgColor: "rgba(128,24,54,1)",

  // Header
  toolbarBtnColor: "rgba(128,24,54,1)",
  toolbarDefaultBg: "rgba(255,255,255,1)",
  toolbarHeight: responsiveFontSize(56),
  toolbarSearchIconSize: 23,
  toolbarInputColor: "#fff",
  searchBarHeight: responsiveFontSize(platform === "ios" ? 30 : 40),
  searchBarInputHeight: responsiveFontSize(platform === "ios" ? 40 : 50),
  toolbarBtnTextColor: "#fff",
  toolbarDefaultBorder: "rgba(128,24,54,1)",
  iosStatusbar: "light-content",
  get statusBarColor() {
    return color(this.toolbarDefaultBg)
      .darken(0.2)
      .hex();
  },
  get darkenHeader() {
    return color(this.tabBgColor)
      .darken(0.03)
      .hex();
  },

  // Icon
  iconFamily: "Ionicons",
  iconFontSize: responsiveFontSize(28),
  iconHeaderSize: responsiveFontSize(24),

  // InputGroup
  inputFontSize: responsiveFontSize(17),
  inputBorderColor: "#D9D5DC",
  inputSuccessBorderColor: "#2b8339",
  inputErrorBorderColor: "#ed2f2f",
  inputHeightBase: responsiveFontSize(50),
  get inputColor() {
    return this.textColor;
  },
  get inputColorPlaceholder() {
    return "#575757";
  },

  // Line Height
  btnLineHeight: responsiveFontSize(19),
  lineHeightH1: responsiveFontSize(32),
  lineHeightH2: responsiveFontSize(27),
  lineHeightH3: responsiveFontSize(22),
  lineHeight: responsiveFontSize(24),

  // List
  listBg: "#FFF",
  listBorderColor: "#c9c9c9",
  listDividerBg: "#f4f4f4",
  listBtnUnderlayColor: "#DDD",
  listItemPadding: responsiveFontSize(12),
  listNoteColor: "rgba(70,70,70,1)",
  listNoteSize: responsiveFontSize(13),

  // Progress Bar
  defaultProgressColor: "#E4202D",
  inverseProgressColor: "#1A191B",

  // Radio Button
  radioBtnSize: 23,
  radioSelectedColorAndroid: "rgba(128,24,54,1)",
  radioBtnLineHeight: 24,
  get radioColor() {
    return this.brandPrimary;
  },

  // Segment
  segmentBackgroundColor: "rgba(255,255,255,1)",
  segmentActiveBackgroundColor: "rgba(128,24,54,1)",
  segmentTextColor: "rgba(70,70,70,1)",
  segmentActiveTextColor: "rgba(255,255,255,1)",
  segmentBorderColor: "rgba(128,24,54,1)",
  segmentBorderColorMain: "#3F51B5",

  // Spinner
  defaultSpinnerColor: "rgba(128,24,54,1)",
  inverseSpinnerColor: "#1A191B",

  // Tab
  //tabDefaultBg: "rgba(77,77,77,1)",
 // topTabBarTextColor: "rgba(255,255,255,1)",
  //topTabBarActiveTextColor: "rgba(248,231,28,1)",
  //topTabBarBorderColor: "#fff",
  //topTabBarActiveBorderColor: "rgba(248,231,28,1)",

  // Tabs
  tabBgColor: "#F8F8F8",
  tabFontSize: responsiveFontSize(15),

  // Text
  textColor: "rgba(70,70,70,1)",
  inverseTextColor: "#fff",
  noteFontSize: responsiveFontSize(14),
  get defaultTextColor() {
    return this.textColor;
  },

  // Title
  titleFontfamily: "OpenSans",
  titleFontSize: responsiveFontSize(19),
  subTitleFontSize: responsiveFontSize(14),
  subtitleColor: "#FFF",
  titleFontColor: "rgba(128,24,54,1)",

  // Other
  borderRadiusBase: 2,
  borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
  contentPadding: responsiveFontSize(10),
  dropdownLinkColor: "rgba(128,24,54,1)",
  inputLineHeight: responsiveFontSize(24),
  deviceWidth,
  deviceHeight,
  isIphoneX,
  inputGroupRoundedBorderRadius: 30
};
