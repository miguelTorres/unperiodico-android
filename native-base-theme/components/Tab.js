import variable from "./../variables/unperiodico-material";

export default (variables = variable) => {
  const tabTheme = {
    flex: 1,
    backgroundColor: "#FFF"
  };

  return tabTheme;
};
