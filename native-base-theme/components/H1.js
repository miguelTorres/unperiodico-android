import variable from "./../variables/unperiodico-material";

export default (variables = variable) => {
  const h1Theme = {
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    fontSize: variables.fontSizeH1,
    lineHeight: variables.lineHeightH1,
  };

  return h1Theme;
};
