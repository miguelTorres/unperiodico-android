import variable from "./../variables/unperiodico-material";

export default (variables = variable) => {
  const switchTheme = {
    marginVertical: -5,
  };

  return switchTheme;
};
