plugin.tx_unalunperiodicoapp_unalappconfiguration {
  settings{
    app >
    app {
      sideMenu {
        10 {
          type = headerTab
          footerTabComparation{
            category = staticprominent
          }
          title = Inicio
            icon{
              url = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconinicio175.png
            }

        }
  
        30 {
            type = route
            title = Sondeos
            route = Poll
            icon{
                url = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconsondeos175.png
            }
            bg = C5F442
        }



        40 {
          type = headerTab
          footerTabComparation{
            view = EveryNews
          }
          headerTabComparation{
            category = staticmostseen
          }
          title = Lo más visto
            icon{
                url = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconmas-vistos175.png
            }

        }
      
        60 {
            type = route
            title = Noticias guardadas
            route = SavedNews
            params{
                title = Noticias guardadas
            }
            icon{
                url = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconmarcadores175.png
            }
            bg = #C5F442
        }


        90 {
          type = route
          title = Columna
          route = NewsContainer
            icon{
                url = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconcolumna175.png
            }
          bg = #C5F442
          params{
              title = Columna
            url {
              generateUrl = 1
              pageId = 186
              params{
                type=102
              }
            }
            view = Authors
            category = staticcolumna
          }
        }        

        100 {
          type = route
          title = Sobre nosotros
          route = AjaxPage
          icon{
              url = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconcreditos175.png
          }
          bg = #C5F442
          params{
            title = Sobre nosotros
            url {
              generateUrl = 1
              pageId = 193
              params{
                type=101
              }
            }
          }
        }     

        110 {
          type = link
          url {
            generateUrl = 1
            pageId = 75
          }
          title = Histórico UN Periódico
          icon{
            url = https://dev3.unal.edu.co/fileadmin/user_upload/images/transparent.png
          }
        }        


        120 {
          type = title
          title = NUESTROS PRODUCTOS
          style{
              fontSize = 13
          }
          icon{
            url = https://dev3.unal.edu.co/fileadmin/user_upload/images/transparent.png
          }
        }     

        130 {
          type = link
          url = http://agenciadenoticias.unal.edu.co/
          title = Agencia de noticias
          icon{
            url = https://dev3.unal.edu.co/fileadmin/user_upload/images/transparent.png
          }
        }        

        140 {
          type = link
          url = http://circular.unal.edu.co/
          title = Eventos
          icon{
            url = https://dev3.unal.edu.co/fileadmin/user_upload/images/transparent.png
          }
        }     

        150 {
          type = link
          url = http://unradio.unal.edu.co/
          title = UN Radio
          icon{
            url = https://dev3.unal.edu.co/fileadmin/user_upload/images/transparent.png
          }
        }        
        160 {
          type = link
          url = http://untelevision.unal.edu.co/
          title = UN Televisión
          icon{
            url = https://dev3.unal.edu.co/fileadmin/user_upload/images/transparent.png
          }
        }        
      
      }
      footerTabs {

        10 {
          url {
            generateUrl = 1
            pageId = 183
            params{
              type=102
            }
          }
          title {
              default {
                  title = DESTACADO
              }
                categories{
                    49 {
                        title = ESPECIAL
                    }
                }
          }
          view = CarouselView
          itemView = ProminentCarouselViewItem
          category = staticprominent
          filtroFecha = 0
        }
        20 {
          title = NOTICIAS
          view = EveryNews
        }

        30 {
          url {
            generateUrl = 1
            pageId = 185
            params{
              type=102
              tx_news_pi1{
                  overwriteDemand{
                      categories=67
                  }
              }
              #tx_news_pi1[controller]=news
              #tx_news_pi1[action]=list
              #tx_news_pi1[overwriteDemand][categories]=67
            }
          }
          title = RECOMENDADOS
          category = staticrecommended
          view = DefaultView
          filtroFecha = 0
        }
      
      }
      
      initialPages {
        10 {
          url {
            generateUrl = 1
            pageId = 198
            params{
              type=102
            }
          }
          jsonkey = Noticias
          title = Recientes
          category = staticrecent
          view = CarouselView
          itemView = RecientesCarouselViewItem
          filtroFecha = 0
        }
        20 {
          url {
            generateUrl = 1
            pageId = 197
            params{
              type=102
            }
          }
          title = Lo más visto
          category = staticmostseen
          #view = Carousel
          view = default
          showMainCategory = 1
          filtroFecha = 0
        }

      
      }
      list{
        NEWS_TYPE{
          default{
            //view = ProminentItem
            //icon = https://dev3.unal.edu.co/fileadmin/user_upload/images/play_Text.svg.png
            tag{
              name = 
              color = #7f1836
              bgcolor = #ffd161
            }
          }
          categories{
            49 {
              tag {
                name = ESPECIAL
              }
            }
            22 {
              tag {
                name = ABC
              }
            }
            20 {
              tag {
                name = UN TELEVISIÓN
              }
            }
            21 {
              tag {
                name = UN RADIO
              }
            }
          }
        }
        CONTENT_TYPE{
          default{
            //view = ProminentItem
            //icon = https://dev3.unal.edu.co/fileadmin/user_upload/images/play_Text.svg.png
            tag{
              name = 
              color = #ffffff
              bgcolor = #7f1836
            }
          }
          categories{
            68 {
              tag {
                name = COLUMNA
              }
            }
            4 {
              tag {
                name = VIDEO
              }
              icon = http://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconvideo_1175.png
            }
            5 {
              tag {
                name = AUDIO
              }
              icon = http://dev3.unal.edu.co/fileadmin/user_upload/images/app-iconaudio175.png
            }            
            3 {
              tag {
                name = GALERÍA
              }
              icon {
                name = md-photos
                type = Ionicons
                style{
                  color = white
                }
              }
            }  
            26 {
              tag {
                name = TRANSMISIÓN
              }
              icon = https://dev3.unal.edu.co/fileadmin/user_upload/images/app-icontransmision175.png
            }
            17 {
              tag {
                name = ARTÍCULO
              }
            }            
          
          }
        }        
      }
      detail < .list
      detail.CONTENT_TYPE.categories.3 >
      POLL_URL{
        generateUrl = 1
        pageId = 192
        params{
        }
      }
      POLL_LIST_URL{
            generateUrl = 1
            pageId = 189
            params{
                type=102
            }
        }
      SEARCH_VIEW = DefaultView
      SEARCH_FORM_URL{
            generateUrl = 1
            pageId = 191
            params{
                type=101
            }
        }
      SEARCH_RESULTS_URL{
            generateUrl = 1
            pageId = 195
            params{
                type=102
            }
        }
      CONFIGURATION_URL{
        generateUrl = 1
        pageId = 194
        params{
            type=101
        }
      }
    SONDEOS {
        formatDate = MMMM DD [de] YYYY
      }
      subscribeToTopic=test
      SITE_URL=https://dev3.unal.edu.co/
      PING_URL=https://www.google.com/
      THUMBNAILS_URL = https://dev3.unal.edu.co/thumbnails/
      AUDIO_ARTWORK=http://dev3.unal.edu.co/fileadmin/user_upload/images/ic_launcher.png
      NEWS_URL=https://dev3.unal.edu.co/index.php?id=185&type=102
      REVIEW_IDS_SERVICE=http://dev3.unal.edu.co/index.php?id=205&type=102&no_cache=1&tx_news_pi1[overwriteDemand][reviewIds]=
      CATEGORIES_URL{
        generateUrl = 1
        pageId = 188
        params{
            type=102
        }
      }

      GENERATE_URL=https://dev3.unal.edu.co/index.php?id=74
    }
  }
}
